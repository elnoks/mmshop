<?
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->ShowAjaxHead();
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:iblock.element.add.form",
    "call",
    array(
        "BUTTON_TEXT"=> "Заказать обратный звонок",
        "SEF_MODE" => "N",
        "AJAX_MODE" => "Y",
        "IBLOCK_ID" => "6",
        "PROPERTY_CODES" => array(
            0 => "130",
            1 => "131",
            2 => "NAME",
        ),
        "PROPERTY_CODES_REQUIRED" => array(
            0 => "130",
        ),
        "GROUPS" => array(
            0 => "1",
            1 => "2",
            2 => "3",
            3 => "4",
            4 => "5",
            5 => "6",
            6 => "7",
        ),
        "STATUS" => "ANY",
        "STATUS_NEW" => "N",
        "ALLOW_EDIT" => "N",
        "ALLOW_DELETE" => "N",
        "ELEMENT_ASSOC" => "CREATED_BY",
        "NAV_ON_PAGE" => "10",
        "MAX_USER_ENTRIES" => "100000",
        "MAX_LEVELS" => "100000",
        "LEVEL_LAST" => "N",
        "USE_CAPTCHA" => "N",
        "USER_MESSAGE_ADD" => "",
        "USER_MESSAGE_EDIT" => "",
        "DEFAULT_INPUT_SIZE" => "30",
        "RESIZE_IMAGES" => "N",
        "MAX_FILE_SIZE" => "1",
        "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
        "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
        "AJAX_OPTION_SHADOW" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "COMPONENT_TEMPLATE" => "call",
        "IBLOCK_TYPE" => "communications",
        "LIST_URL" => "",
        "CUSTOM_TITLE_NAME" => "",
        "CUSTOM_TITLE_TAGS" => "",
        "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
        "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
        "CUSTOM_TITLE_IBLOCK_SECTION" => "",
        "CUSTOM_TITLE_PREVIEW_TEXT" => "",
        "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
        "CUSTOM_TITLE_DETAIL_TEXT" => "",
        "CUSTOM_TITLE_DETAIL_PICTURE" => ""
    ),
    false
);?>