<?
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

Loader::includeModule("catalog");

if ($_REQUEST['id'] > 0) {

    $fields = [
        'PRODUCT_ID' => $_REQUEST['id'],
        'QUANTITY' => 1,
        "DELAY" => "N",
    ];

    $r = \Bitrix\Catalog\Product\Basket::addProduct($fields);
    if (!$r->isSuccess()) {
        var_dump($r->getErrorMessages());
    }
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>