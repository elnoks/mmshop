<?
define('STOP_STATISTICS', true);
define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

Loader::includeModule("catalog");

if ($_REQUEST['id'] > 0) {

    $filter["ACTIVE"] = "Y";
    if($_REQUEST["TSVET"] != "undefined") $filter["PROPERTY_TSVET"] = $_REQUEST["TSVET"];
    if($_REQUEST["PROPERTY_RAZMERY"] != "undefined") $filter["PROPERTY_RAZMERY"] = $_REQUEST["RAZMERY"];

    $res = CCatalogSKU::getOffersList(
        $_REQUEST['id'],
        0,
        $filter,
        array('NAME'),
        array("CODE" => array('RAZMERY', 'TSVET'))
    );

    if(!empty($res)){
        $idel = key($res[$_REQUEST['id']]);
    }else {
        $idel = $_REQUEST['id'];
    }

    $fields = [
        'PRODUCT_ID' => $idel,
        'QUANTITY' => 1,
        "DELAY" => "Y",
    ];

    $r = Bitrix\Catalog\Product\Basket::addProduct($fields);
    if (!$r->isSuccess()) {
        var_dump($r->getErrorMessages());
    }
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>