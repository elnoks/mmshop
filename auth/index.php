<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");

$userName = CUser::GetFullName();
if (!$userName)
	$userName = CUser::GetLogin();
?>
<script>
	<?if ($userName):?>
	BX.localStorage.set("mmshop", "<?=CUtil::JSEscape($userName)?>", 604800);
	<?else:?>
	BX.localStorage.remove("mmshop");
	<?endif?>

	<?if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0 && preg_match('#^/\w#', $_REQUEST["backurl"])):?>
	document.location.href = "<?=CUtil::JSEscape($_REQUEST["backurl"])?>";
	<?endif?>
</script>

<?
$APPLICATION->SetTitle("Авторизация");
?>
    <div class="content_front">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center profile-created">
                    <h1>
                    <?
                    if($_REQUEST["registered"] == "yes"){
                        echo "Ваш профиль успешно создан";
                    }else{
                        echo "Вы успешно авторизовались";
                    }
                    ?>
                    </h1>
                    <p>Если у вас есть какие-то вопросы, свяжитесь с нами:</p>
                    <p style="font-size: 16px;"><strong>+7 (951) 824-29-25</strong></p>
                    <p>Ежедневно с 09:00 до 18:00  (по Мск)</p>
                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>