<table class="discont-table-for-desk">
    <tbody><tr>
        <td style="width: 140px;">10 000 - 3%</td>
        <td style="width: 140px;">30 000 - 6%</td>
        <td style="width: 140px;">75 000 - 9%</td>
    </tr>
    <tr>
        <td>15 000 - 4%</td>
        <td>40 000 - 7%</td>
        <td>100 000 - 10%</td>
    </tr>
    <tr>
        <td>20 000 - 5%</td>
        <td>50 000 - 8%</td>
        <td></td>
    </tr>
    </tbody></table>
<table class="discont-table-for-mobile">
    <tbody><tr>
        <td style="width: 140px;">10 000 - 3%</td>
        <td style="width: 140px;">40 000 - 7%</td>
    </tr>
    <tr>
        <td>15 000 - 4%</td>
        <td>50 000 - 8%</td>
    </tr>
    <tr>
        <td>20 000 - 5%</td>
        <td>75 000 - 9%</td>
    </tr>
    <tr>
        <td>30 000 - 6%</td>
        <td>100 000 - 10%</td>
    </tr>
    </tbody></table>

<hr>