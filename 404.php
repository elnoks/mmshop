<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->AddChainItem("Страница не найдена", "");

$APPLICATION->SetTitle("Страница не найдена"); ?>

    <div class="content_front">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center find-nothing">
                    <h1>СТРАНИЦА НЕ НАЙДЕНА</h1>
                    <p>Такой страницы не существует. Введите поисковый запрос</p>
                    <p>или перейдите по актуальной категории</p>
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:search.form",
                        "def404", Array(),
                        false,
                        array(
                            "ACTIVE_COMPONENT" => "Y"
                        )
                    );
                    ?>
                </div>
            </div>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "search_menu",
                array(
                    "ROOT_MENU_TYPE" => "top",
                    "MAX_LEVEL" => "2",
                    "CHILD_MENU_TYPE" => "top",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "COMPONENT_TEMPLATE" => "search_menu",
                    "MENU_THEME" => "site",
                    "LINK_NEW" => "/2",
                    "LINK_SALE" => "/3"
                ),
                false
            ); ?>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>