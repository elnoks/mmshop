<?
define ("COLOR_IB", 2);
define ("SIZE_IB", 3);
define ("BRAND_IB", 1);

function op($mass)
{
    echo "<pre>";
    print_r($mass);
    echo "</pre>";
}

use Bitrix\Main\EventManager,
    Bitrix\Main\Loader,
    Bitrix\Sale;

function getDiscountByUser(){
    Loader::includeModule('sale');
    $filter = array(
        'filter' => array('USER_ID' =>  $GLOBALS["USER"]->GetID(), 'LID' => SITE_ID, "PAYED" => "Y"),
        'select' => array('*'),
        'order' => array('ID' => 'DESC')
    );
    $price = 0;
    $orderList = Sale\Order::getList($filter);
    while ($orderResult = $orderList->fetch()) {
        $price = $price + $orderResult["PRICE"];
    }

    $discount = "0%";
    if ($price > 10000 && $price < 15000)
        $discount = "3%";
    elseif($price >= 15000 && $price < 20000)
        $discount = "4%";
    elseif($price > 20000 && $price < 30000)
        $discount = "5%";
    elseif($price > 30000 && $price < 40000)
        $discount = "6%";
    elseif($price > 40000 && $price < 50000)
        $discount = "7%";
    elseif($price > 50000 && $price < 75000)
        $discount = "8%";
    elseif($price > 75000 && $price < 100000)
        $discount = "9%";
    elseif($price > 100000)
        $discount = "10%";

    return $discount;
}

function getFromHighloadbyXmlId($id, $xml_id)
{
    Loader::includeModule('highloadblock');
    $hldata = Bitrix\Highloadblock\HighloadBlockTable::getById($id)->fetch();
    $hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
    $hlDataClass = $hldata["NAME"] . "Table";

    $result = $hlDataClass::getList(array(
        "select" => array("ID", "UF_NAME", "UF_XML_ID"),
        "order" => array("UF_SORT" => "ASC"),
        "filter" => array("UF_XML_ID" => $xml_id),
    ));

    if ($res = $result->fetch()) {
        return($res["UF_NAME"]);
    }
}

function fLetter($str, $lowerEnd = false, $encoding = 'UTF-8')
{
    $firstLetter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
    if ($lowerEnd) {
        $strEnd = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
    } else {
        $strEnd = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
    }
    return $firstLetter.$strEnd;
}

function checkFav()
{
    $favor = array();
    $basketRes = Bitrix\Sale\Internals\BasketTable::getList(array(
        'filter' => array(
            'FUSER_ID' => Bitrix\Sale\Fuser::getId(),
            'ORDER_ID' => null,
            'LID' => SITE_ID,
            'CAN_BUY' => 'Y',
            'DELAY' => 'Y'
        )
    ));
    while ($item = $basketRes->fetch()){
        $mxResult = CCatalogSku::GetProductInfo($item["PRODUCT_ID"]);
        if (is_array($mxResult))
            $favor[] = $mxResult['ID'];

        $favor[] = $item["PRODUCT_ID"];
    }
    return $favor;
}

EventManager::getInstance()->addEventHandler('catalog', 'OnSuccessCatalogImport1C', 'deactivateZeroOffers');
function deactivateZeroOffers()
{

}
/*
EventManager::getInstance()->addEventHandler('sale', 'OnSaleOrderSaved', 'paymentRecalc');
function paymentRecalc(Bitrix\Main\Event $event)
{
    $order = $event->getParameter("ENTITY");

    $paymentCollection = $order->getPaymentCollection();

    foreach ($paymentCollection as $payment) {
        $psID = $payment->getPaymentSystemId();
        $payment->delete();
    }

    $paymentAll = $paymentCollection->createItem(
            Bitrix\Sale\PaySystem\Manager::getObjectById($psID)
        );
    $paymentAll->setField("SUM", $order->getPrice() - $order->getDeliveryPrice());

    $paymentDelivery = $paymentCollection->createItem(
        Bitrix\Sale\PaySystem\Manager::getObjectById(10)
    );
    $paymentDelivery->setField("SUM", $order->getDeliveryPrice());

    $order->save();
}
*/
EventManager::getInstance()->addEventHandler('iblock', 'OnAfterIBlockElementAdd', 'OnAfterIBlockElementAddHandler');
function OnAfterIBlockElementAddHandler(&$arFields)
{
    Loader::includeModule("iblock");
    $arrEvents = array();
    $rsET = CEventType::GetList(array("LID" => "ru"));
    while ($arET = $rsET->Fetch())
        $arrEvents[] = $arET["EVENT_NAME"];

    $res = CIBlock::GetByID($arFields["IBLOCK_ID"]);
    if ($ar_res = $res->GetNext())
        if (in_array($ar_res["CODE"], $arrEvents)) {
            $arEventFields["ACTIVE_FROM"] = $arFields["ACTIVE_FROM"];
            foreach ($arFields["PROPERTY_VALUES"] as $id => $val) {
                $db_props = CIBlockElement::GetProperty($arFields["IBLOCK_ID"], $arFields["ID"], array("sort" => "asc"), Array("ID" => $id));
                if ($ar_props = $db_props->Fetch()) {
                    if ($ar_props["PROPERTY_TYPE"] == "L") {
                        $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "ID" => $ar_props["VALUE"], "CODE" => $ar_props["CODE"]));
                        while ($enum_fields = $property_enums->GetNext())
                            $arEventFields[$ar_props["CODE"]] = $enum_fields["VALUE"];
                    } else
                        $arEventFields[$ar_props["CODE"]] = $ar_props["VALUE"];
                }
            }
            //file_put_contents($_SERVER['DOCUMENT_ROOT']."/1.txt", date("Y-m-d H:i:s")." - ".print_r($arEventFields, true)."\n----------\n", FILE_APPEND);
            Bitrix\Main\Mail\Event::send(array(
                "EVENT_NAME" => $ar_res["CODE"],
                "LID" => SITE_ID,
                "C_FIELDS" => $arEventFields
            ));
        }
}

?>