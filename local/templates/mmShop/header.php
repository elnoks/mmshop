<!DOCTYPE html>
<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

Loc::loadMessages(__FILE__);
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><? $APPLICATION->ShowTitle() ?></title>
    <?
    //css
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/libs/bootstrap/css/bootstrap.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/libs/bootstrap/css/bootstrap-theme.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/libs/font-awesome-4.7.0/css/font-awesome.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/cabinet-style.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/media.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/custom.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/libs/slickslider/slick/slick.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/libs/slickslider/slick/slick-theme.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/fonts/flaticon/flaticon.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.mCustomScrollbar.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "http://fonts.googleapis.com/css?family=Varela+Round");

    $APPLICATION->ShowHead();
    //js
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/libs/jquery/jquery-3.3.1.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/libs/bootstrap/js/bootstrap.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/libs/slickslider/slick/slick.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.mCustomScrollbar.concat.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/app.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/script.js");
    ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= SITE_TEMPLATE_PATH ?>/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= SITE_TEMPLATE_PATH ?>/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= SITE_TEMPLATE_PATH ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= SITE_TEMPLATE_PATH ?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= SITE_TEMPLATE_PATH ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= SITE_TEMPLATE_PATH ?>/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= SITE_TEMPLATE_PATH ?>/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>
<body class="fullwidth">
<? $APPLICATION->ShowPanel(); ?>

<div class="menu_mobile" style="display: none">
    <div class="menu_mobile_close"></div>
    <? $APPLICATION->IncludeComponent(
        "bitrix:search.form",
        ".default",
        array(
            "COMPONENT_TEMPLATE" => ".default",
            "PAGE" => "#SITE_DIR#search/index.php",
            "USE_SUGGEST" => "N"
        ),
        false
    ); ?>
    <? $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "menu_mobile",
        array(
            "ROOT_MENU_TYPE" => "top",
            "MAX_LEVEL" => "4",
            "CHILD_MENU_TYPE" => "top",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "Y",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(),
            "COMPONENT_TEMPLATE" => "top_menu",
            "MENU_THEME" => "site",
            "LINK_NEW" => "/2",
            "LINK_SALE" => "/3"
        ),
        false
    ); ?>
</div>
<div class="top_line">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/info.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                ); ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/phone.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                ); ?>
                <a href="javascript:void(0)"
                   data-toggle="modal"
                   onclick="openCallForm('/', 'callback')"
                   class="oln tl_link tl_link_6"
                   data-target="#callback"
                >
                    <i class="tl_ico flaticon-technology-1"></i><?= Loc::getMessage("CALL_BACK"); ?>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="logo">
                    <a href="/">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/include/logo.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        ); ?>
                    </a>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top_menu",
                    array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "4",
                        "CHILD_MENU_TYPE" => "top",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "Y",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => array(),
                        "COMPONENT_TEMPLATE" => "top_menu",
                        "MENU_THEME" => "site",
                        "LINK_NEW" => "/2",
                        "LINK_SALE" => "/3"
                    ),
                    false
                ); ?>
            </div>
            <div class="col-lg-3">
                <div class="head_controls">
                    <div class="head_control_item menu_key" style="display: none">
                        <a href="javascript:void(0)" id="mobile_menu_link">
                            <div class="head_control_item_ico head_control_item_ico_menu"></div>
                            <div class="head_control_item_label">
                                Меню
                            </div>
                        </a>
                    </div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:system.auth.authorize",
                        "",
                        array(
                            "REGISTER_URL" => "/auth/?register=yes",
                            "FORGOT_URL" => "/auth/?forgot_password=yes",
                            "LOGIN_URL" => "/auth/?login=yes"
                        ),
                        false
                    ); ?>

                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket.line",
                        "def",
                        array(
                            "COMPONENT_TEMPLATE" => "fav",
                            "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                            "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_TOTAL_PRICE" => "N",
                            "SHOW_EMPTY_VALUES" => "N",
                            "SHOW_PERSONAL_LINK" => "N",
                            "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                            "SHOW_AUTHOR" => "N",
                            "PATH_TO_AUTHORIZE" => "",
                            "SHOW_REGISTRATION" => "N",
                            "PATH_TO_REGISTER" => SITE_DIR . "auth/?register=yes",
                            "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                            "SHOW_PRODUCTS" => "Y",
                            "SHOW_DELAY" => "Y",
                            "SHOW_NOTAVAIL" => "N",
                            "SHOW_IMAGE" => "Y",
                            "SHOW_PRICE" => "Y",
                            "SHOW_SUMMARY" => "Y",
                            "POSITION_FIXED" => "Y",
                            "HIDE_ON_BASKET_PAGES" => "N",
                            "POSITION_HORIZONTAL" => "right",
                            "POSITION_VERTICAL" => "top"
                        ),
                        false
                    ); ?>
                    <?php
                    $APPLICATION->IncludeComponent(
                        "bitrix:sale.basket.basket.line",
                        "def",
                        array(
                            "COMPONENT_TEMPLATE" => "def",
                            "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                            "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_TOTAL_PRICE" => "Y",
                            "SHOW_EMPTY_VALUES" => "N",
                            "SHOW_PERSONAL_LINK" => "N",
                            "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                            "SHOW_AUTHOR" => "N",
                            "PATH_TO_AUTHORIZE" => "",
                            "SHOW_REGISTRATION" => "N",
                            "PATH_TO_REGISTER" => SITE_DIR . "auth/?register=yes",
                            "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                            "SHOW_PRODUCTS" => "Y",
                            "SHOW_DELAY" => "N",
                            "SHOW_NOTAVAIL" => "N",
                            "SHOW_IMAGE" => "Y",
                            "SHOW_PRICE" => "Y",
                            "SHOW_SUMMARY" => "Y",
                            "POSITION_FIXED" => "Y",
                            "HIDE_ON_BASKET_PAGES" => "N",
                            "POSITION_HORIZONTAL" => "right",
                            "POSITION_VERTICAL" => "top"
                        ),
                        false
                    ); ?>
                    <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:search.form",
                        "flat", Array(),
                        false,
                        array(
                            "ACTIVE_COMPONENT" => "Y"
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//    $pos = strripos($APPLICATION->GetCurPage(false), '/catalog/');
//    $breadcrumbCat = $pos === false ? false : true;
if ($APPLICATION->GetCurPage(false) !== '/') {
    if ($_REQUEST["register"] != "yes") {
        ?>
        <div class="container">
            <?
            $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "breadcrumb",
                array(),
                false
            );
            ?>
        </div>
        <?php
    }
}
?>