function openOrderForm(site_dir, call, id = null){
    BX.showWait(BX(call));
    BX.ajax.post(
        site_dir + 'ajax/'+ call +'.php?id=' + id,
        {
        },
        BX.delegate(function(result)
            {
                BX.adjust(BX(call), {html: result});

                var popWindow = BX(call);
                var easing = new BX.easing({
                        duration: 300,
                        start: {opacity: 0},
                        finish: {opacity: 100},
                        step: function (state) {
                            popWindow.style.opacity = state.opacity / 100;
                        },
                    }
                );
                easing.animate();

                BX.closeWait(BX(call));
            },
            this)
    );
}

function openCallForm(site_dir, call, id = null, condition = false) {
    BX.showWait(BX(call));
    var arr = new Object();
    var errNames = ['fav', 'favToCart', 'orderCard'];

    var fields = BX.findChild(BX(id + 'skuFav'), {class: 'selected'}, true, true);
    if(call != "favToCart" && condition == true){
        if (fields.length > 0) {
            fields.forEach(function (element) {
                arr[element.getAttribute("data-code")] = element.getAttribute("data-xml");
            });
        }
    }
    BX.ajax.post(
        site_dir + 'ajax/'+ call +'.php?id=' + id + '&RAZMERY=' + arr.RAZMERY + "&TSVET=" + arr.TSVET,
        {
        },
        BX.delegate(function(result)
        {
            if (id != null && condition == true) {
                BX.onCustomEvent('OnBasketChange');
                if (call == "favToCart"){
                    BX.removeClass(id + 'favorite', 'active');
                }
            }else if(id != null){
                call = 'modal_quick' + '_' + id;
            }

            if (condition == false)
                BX.adjust(BX(call), {html: result});

            if (!BX.util.in_array(call, errNames)) {
                var popWindow = BX(call);
                var easing = new BX.easing({
                        duration: 300,
                        start: {opacity: 0},
                        finish: {opacity: 100},
                        step: function (state) {
                            popWindow.style.opacity = state.opacity / 100;
                        },
                    }
                );
                easing.animate();
            }
            BX.closeWait(BX(call));
        },
        this)
    );
}

BX.ready(function(){
    if(BX('showMe')) {
        var easing = new BX.easing({
                duration: 1000,
                start: {opacity: 0},
                finish: {opacity: 100},
                step: function (state) {
                    BX('showMe').style.opacity = state.opacity / 100;
                },
            }
        );
        easing.animate();
    }
})
