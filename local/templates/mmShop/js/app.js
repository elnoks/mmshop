BX.ready(function () {
    var window_width;
    window_width = document.body.clientWidth;

    $(window).resize(function () {
        window_width = document.body.clientWidth;

        if (window_width > 1199) {
            main_menu_hide();
            filter_hide();
        } else {
            filter_show();
        }
        $('.slider2').on('setPosition', function (slick) {
            var slider2_height = $('.slider2').height();
            $('.banners_small').height(slider2_height);
        });
    });

    function filter_hide() {
        $('.sb_filter_container').removeAttr('style');
        $('.sb_filter_container').removeClass('mobile_filter');
    }

    function filter_show() {
        $('.sb_filter_container').addClass('mobile_filter');
    }

    $('.detail_img').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.m_variations'
    });

    $('.m_variations').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.detail_img',
        dots: false,
        arrows: false,
        centerMode: false,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false,
                    vertical: true,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false,
                    vertical: false,
                }
            }
        ]
    });

    if (window_width < 1200) {
        $('.spec_products').slick({
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                    }
                }
            ]
        });
    }

    $('.slider1').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,

    });

    $('.slider2').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('.slider3').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('.manufact_slider ul').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 8,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    var timer,
        delay = 150,
        e;

    $(window).scroll(function () {
        if ($(this).scrollTop() > $('.top_line').outerHeight() && $(this).scrollTop() != 0 ) {
            $('.main_menu .dropdown .sub_menu').css('top', $('.header').outerHeight() - 2);
        } else {
            $('.main_menu .dropdown .sub_menu').css('top', $('.header').outerHeight() + $('.top_line').outerHeight() );
        }
    });

    $('.main_menu .dropdown').mouseenter(function () {
        e = $(this);
        timer = setTimeout(function () {
            close_all_access();
            $('.mms_overlay').removeClass('hidden');
            e.find('.sub_menu').slideDown('fast');
        }, delay);
    }).mouseleave(function () {
        close_all_access();
        $('.mms_overlay').addClass('hidden');
        $(this).find('.sub_menu').hide();
        clearTimeout(timer);
    });

    function main_menu_hide() {
        if ($('.menu_mobile').css('display', 'none')) {
            $(".menu_mobile").animate({
                width: "hide",
                opacity: "hide"
            });
            $('.mms_overlay2').addClass('hidden');
        }
    }

    function close_all_access() {
        $(".cart_key, .favorite_key").removeClass("bx-opener");
        $(".cart_key, .favorite_key").addClass("bx-closed");
        $('.access_menu').hide();
    }

    $('#profile_link, #search_link').click(function () {
        if ($(this).parent().find('.access_menu').is(':visible')) {
            $('.access_menu').hide();
            $('.mms_overlay').addClass('hidden');
        } else {
            close_all_access();
            $(this).parent().find('.access_menu').toggle();
            $('.mms_overlay').removeClass('hidden');
        }
    });

    $('.mms_overlay').click(function () {
        $('.access_menu').hide();
        $('.menu_mobile').hide();
        close_all_access();
        $(this).addClass('hidden');
    });

    $('.mms_overlay2').click(function () {
        $('.access_menu').hide();
        $('.menu_mobile').hide();
        $('.sb_filter_container ').hide();
        $(this).addClass('hidden');
    });

    $('.to_up').click(function () {
        $('html, body').animate({scrollTop: 0}, 900);
    });

    $('.menu_mobile').on('click', '.menu_mobile_close', function () {
        $(".menu_mobile").animate({
            width: "toggle",
            opacity: "toggle"
        });
        $('.mms_overlay2').addClass('hidden');
    });

    $('.menu_key').click(function () {
        $('.mms_overlay2').removeClass('hidden');
        $(".menu_mobile").animate({
            width: "toggle",
            opacity: "toggle"
        });
    });

    $('.sb_filter_mobile_close').click(function () {
        $(".sb_filter_container").animate({
            width: "toggle",
            opacity: "toggle"
        });
        $('.mms_overlay2').addClass('hidden');
    });

    menu_mobile_prev_1 = $('.menu_mobile').html();
    menu_mobile_prev = '';

    $('.menu_mobile').on('click', 'li.dropdown', function () {
        menu_mobile_prev = $('.menu_mobile').html();
        var submenu = $(this).children('ul').css('display', 'block');
        var submenu = $(this).children('ul').css('height', '100%');
        var current_name = $(this).find('a').html();
        if ($('.menu_backlink').attr('data-index') == 1) {
            var backlink = '<div class="menu_backlink" data-index="2">' + current_name + '</div>';
        } else {
            var backlink = '<div class="menu_backlink" data-index="1">' + current_name + '</div>';
        }
        $('.menu_backlink').detach();
        $('.menu_mobile > ul').detach();
        $('.menu_mobile .menu_mobile_close').after(submenu);
        $('.menu_mobile .menu_mobile_close').after(backlink);
    });

    $('.menu_mobile').on('click', '.menu_backlink', function (e) {
        if ($(this).attr('data-index') == 1) {
            $('.menu_mobile').html(menu_mobile_prev_1);
        }
        if ($(this).attr('data-index') > 1) {
            $('.menu_mobile').html(menu_mobile_prev);
        }
    });

    var s2_h = Math.floor($('.slider2 .slick-slide img').height()) - 1;
    $('.banners_small').css('height', s2_h);
    $(window).resize(function () {
        var s2_h = Math.floor($('.slider2 .slick-slide img').height()) - 1;
        //var s2_h = Math.floor($('.slider2').height()) - 1;

        $('.banners_small').css('height', s2_h);
    });

    $('.more_link_front').click(function () {
        $('.more_link_front_container').slideToggle();
        if ($(this).html() == 'Свернуть описание') {
            $(this).html('Развернуть описание');
        } else {
            $(this).html('Свернуть описание');
        }
    });

    $('.ft_name span').click(function () {
        $(this).parent().toggleClass('ft_name_open');
        $(this).parent().find('.ft_body').slideToggle('fast');
    });

    $('.ft_body input[type=checkbox]').on("change", function () {
        $(this).parent().parent().find('.chb').toggleClass('cnb_active');
        $(this).parent().parent().toggleClass('cnb_active_li');

        if (parseInt($(this).parent().parent().parent().find('.cnb_active').length) > 0) {
            $(this).parent().parent().parent().parent().parent().addClass('had_select');
        } else {
            $(this).parent().parent().parent().parent().parent().removeClass('had_select');
        }
    });

    $('.clear_current_filter').click(function () {
        $(this).parent().find('.cnb_active_li').removeClass('cnb_active_li');
        $(this).parent().find('.chb').removeClass('cnb_active');
        $(this).parent().find('input:checked').prop('checked', false);
        $(this).parent().parent().removeClass('had_select');
    });

    $('.filter_clear').click(function () {
        $('.had_select').removeClass('had_select');
        $('.cnb_active').removeClass('cnb_active');
        $('.ft_body').find('input:checked').prop('checked', false);
    });

    if ($('#slider-range').length) {
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 500,
            values: [75, 300],
            slide: function (event, ui) {
                $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
                $('#price-min').val(ui.values[0]);
                $('#price-max').val(ui.values[1]);
            }
        });
        $("#amount").val("$" + $("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1));
    }

    if (window_width < 1201) {
        $('.sb_filter_container').addClass('mobile_filter');
    }

    $('.mobile_filter .ft_name').click(function () {
        $('.sb_filter').css('left', '-300px');
        $(this).find('.ft_body').css('left', '300px');
    });

    $('.mobile_filter_trigger').click(function () {
        $('.sb_filter_container').animate({width: "toggle", opacity: "toggle"});
        $('.mms_overlay2').removeClass('hidden');
        $('.mms_overlay2, .mms_overlay').css('display', 'block');
    });

    $('.sort_filters').click(function () {
        $(this).toggleClass('sort_filters_open');
    });

    if (window_width < 769) {
        cur_item = $('.sort_filters').find('li.active a').html();
        $('.sort_filters span').html(cur_item);
    }

    $('.sort_filters ul li').click(function () {
        sort_item = $(this).find('a').html();
        if (window_width < 769) { // изменение
            $('.sort_filters span').html(sort_item);
        }
    });

    $('.dropdown_mms').click(function(){
        $(this).toggleClass('dropdown_mms_opened');
    });

    $('.dropdown_mms span').html($('.dropdown_mms ul li.active a').html());
    $('.dropdown_mms a').click(function(r){
       r.preventDefault();
    });

    $('.dropdown_mms ul li').click(function(){
        var selected_val = $(this).find('a').html();
        $('.dropdown_mms span').html(selected_val);
    });

    $('.m_kinds li a').click(function(e){
        e.preventDefault();
    });

    $('.catalog_subtext_collapse').click(function(e){

        if ($('.catalog_subtext_collapse_content').hasClass('collapsed_c')){
            $('.catalog_subtext_collapse_content').slideDown('fast');
            $('.catalog_subtext_collapse').html('Свернуть описание');
            $('.catalog_subtext_collapse_content').removeClass('collapsed_c');
        } else{
            $('.catalog_subtext_collapse_content').slideUp('fast');
            $('.catalog_subtext_collapse').html('Развернуть описание');

            $('.catalog_subtext_collapse_content').addClass('collapsed_c');
        }
    })

    $('.product_item_list_mobile').slick({
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        }]
    });

    $('.actual_unslick_mobile').slick({
        responsive: [{
            breakpoint: 768,
            settings: {
                dots: false,
                arrows: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
/*
    $(window).on("load", function () {
        if ($('.content').length) {
            $(".content").mCustomScrollbar();
        }
    });
    */
});