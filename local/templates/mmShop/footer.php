        </div>
        </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="row flex_height">
                    <div class="col-lg-4 f_right col-md-12 col-lg-push-8">
                        <div class="to_up"></div>
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:sender.subscribe",
                            "dispatch",
                            array(
                                "COMPONENT_TEMPLATE" => "dispatch",
                                "USE_PERSONALIZATION" => "Y",
                                "CONFIRMATION" => "N",
                                "SHOW_HIDDEN" => "Y",
                                "AJAX_MODE" => "Y",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "AJAX_OPTION_HISTORY" => "N",
                                "CACHE_TYPE" => "A",
                                "CACHE_TIME" => "3600",
                                "SET_TITLE" => "N",
                                "HIDE_MAILINGS" => "N",
                                "USER_CONSENT" => "N",
                                "USER_CONSENT_ID" => "1",
                                "USER_CONSENT_IS_CHECKED" => "Y",
                                "USER_CONSENT_IS_LOADED" => "Y",
                                "AJAX_OPTION_ADDITIONAL" => ""
                            ),
                            false
                        );?>

                        <p class="f_pay_info">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                ".default",
                                array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include/pay_info.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            ); ?>
                        </p>
                        <div class="f_pay_methods">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                ".default",
                                array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include/pay_methods.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            ); ?>
                        </div>
                        <div class="created_by">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                ".default",
                                array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/include/creator.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            ); ?>
                        </div>
                    </div>
                    <div class="col-lg-8 f_left col-md-12 col-sm-12 col-lg-pull-4">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                                        "BLOCK_NAME" => "Информация",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                                        "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                        "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                        "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
                                        "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                        "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                    ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                                    "BLOCK_NAME" => "Центр поддержки",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "ROOT_MENU_TYPE" => "bottomSecond",	// Тип меню для первого уровня
                                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                    "MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
                                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                                    "MAX_LEVEL" => "1",	// Уровень вложенности меню
                                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                                ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <div class="f_phone">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => "/include/footerPhone.php",
                                            "EDIT_TEMPLATE" => ""
                                        ),
                                        false
                                    ); ?>
                                    <span>
                                        <? $APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            ".default",
                                            array(
                                                "COMPONENT_TEMPLATE" => ".default",
                                                "AREA_FILE_SHOW" => "file",
                                                "PATH" => "/include/schedule.php",
                                                "EDIT_TEMPLATE" => ""
                                            ),
                                            false
                                        ); ?>
                                    </span>
                                </div>
                                <div class="f_mail">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => "/include/email.php",
                                            "EDIT_TEMPLATE" => ""
                                        ),
                                        false
                                    ); ?>
                                </div>
                                <div class="f_addr">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => "/include/address.php",
                                            "EDIT_TEMPLATE" => ""
                                        ),
                                        false
                                    ); ?>
                                </div>
                                <div class="f_question_btn">
                                    <a href="javascript:void(0)" onclick="openCallForm('/', 'get_question')" data-toggle="modal" data-target="#get_question">
                                        <i class="f_qb_ico"></i>
                                        <span>Задать вопрос</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    ".default",
                                    array(
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "AREA_FILE_SHOW" => "file",
                                        "PATH" => "/include/social_list.php",
                                        "EDIT_TEMPLATE" => ""
                                    ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-lg-12 col-md-12 f_bt">
                                <div class="f_sub_links">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => "/include/sub_links.php",
                                            "EDIT_TEMPLATE" => ""
                                        ),
                                        false
                                    ); ?>
                                </div>
                                <div class="copyright">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        array(
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "AREA_FILE_SHOW" => "file",
                                            "PATH" => "/include/copyright.php",
                                            "EDIT_TEMPLATE" => ""
                                        ),
                                        false
                                    ); ?>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div></div>
        <div class="modal fade" style="opacity: 0" id="callback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>
        <div class="modal fade" style="opacity: 0" id="get_question" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>

        <div id="overlay" class="mms_overlay hidden"></div>
        <div class="mms_overlay2 hidden"></div>
        </body>
</html>
