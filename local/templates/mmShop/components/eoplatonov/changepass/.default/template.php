<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
?>
<span id="changePass" data-idForm="<?="changePass".$this->randString();?>">
<? if ($_POST[changepass] == "Y")  $APPLICATION->RestartBuffer(); ?>
<?
if ($arResult["IS_AUTHORIZED"]) { ?>
    <form method="POST">
        <h2><?= Loc::getMessage('PROFILE_CHANGE_PASSWORD') ?></h2>
        <p><?= Loc::getMessage('NEW_PASSWORD_S') ?></p>
        <?= bitrix_sessid_post(); ?>
        <input type="hidden" name="changepass" value="Y"/>
        <? if ($arParams[LAST_PASS] == "Y") { ?>
            <input type="password" autocomplete="off" name="last_pass"
                   placeholder="<?= Loc::getMessage('CURRENT_PASSWORD_REQ') ?>"/>
        <? } ?>
        <input type="password" autocomplete="off" name="new_pass"
               placeholder="<?= Loc::getMessage('CHPASS_NEW_PASS') ?>"/>

        <input type="password" autocomplete="off" name="new_pass2"
               placeholder="<?= Loc::getMessage('CHPASS_NEW_PASS2') ?>"/>
        <div class="row">
            <div class="col-lg-3 col-sm-4 col-md-4">
                <button type="submit" class="button btn-orange">
                    <?= Loc::getMessage("MAIN_SAVE") ?>
                </button>
            </div>
            <div class="col-lg-8 col-sm-6 col-md-6">
                <?
                if (isset($_REQUEST["last_pass"]))
                    echo $arResult["ANSWER"]
                ?>
            </div>
        </div>
        <input type="hidden" value="<?= Loc::getMessage("MAIN_SAVE") ?>">
    </form>
<?
}
    if($_POST[changepass]=="Y") die();
?>
</span>
