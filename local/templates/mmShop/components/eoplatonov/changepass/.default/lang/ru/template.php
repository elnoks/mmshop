<?
$MESS["CHPASS_NEED_AUTH"] = "Для смены пароля необходимо авторизоваться";
$MESS["CHPASS_CAPTION"] = "Сменить пароль";
$MESS["CHPASS_LAST_PASS"] = "Cтарый пароль";
$MESS["CHPASS_NEW_PASS"] = "Новый пароль";
$MESS["CHPASS_NEW_PASS2"] = "Подтвердите новый пароль";
$MESS["CHPASS_SUBMIT"] = "Сменить";
$MESS['CURRENT_PASSWORD_REQ'] = "Текущий пароль";
$MESS['CURRENT_PASSWORD_REQ_ERROR'] = "Текущий пароль не совпадает";
$MESS['PROFILE_CHANGE_PASSWORD'] = "Смена пароля";
$MESS['NEW_PASSWORD_S'] = "Если Вы хотите сменить пароль, то укажите старый пароль и желаемый новый. После этого нажмите кнопку «Сохранить»";
$MESS['SAVE'] = "Сохранить изменения";
?>