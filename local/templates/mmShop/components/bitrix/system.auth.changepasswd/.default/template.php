<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<div class="auth-dialog head_control_item">
<h4><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h4>
<?
if(!empty($arParams["~AUTH_RESULT"])):
	$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
?>
	<div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
<?endif?>

	<form class="login_form" method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
<?if (strlen($arResult["BACKURL"]) > 0): ?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<? endif ?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="CHANGE_PWD">
		
		<label for='USER_LOGIN'><?echo GetMessage("AUTH_LOGIN")?></label>
        <i class="ico_man"></i>
		<input id='USER_LOGIN' type="text" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
		
		
		<label for='USER_CHECKWORD'><?echo GetMessage("AUTH_CHECKWORD")?></label>
        <i class="ico_man"></i>
		<input id='USER_CHECKWORD' type="text" name="USER_CHECKWORD" maxlength="255" value="<?=$arResult["USER_CHECKWORD"]?>" />
		
		<label for='USER_PASSWORD'><?echo GetMessage("AUTH_NEW_PASSWORD_REQ")?></label>
        <i class="ico_pw"></i>
		<input id="USER_PASSWORD" type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" />
		
		<label for='USER_CONFIRM_PASSWORD'><?echo GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label>
        <i class="ico_pw"></i>
		<input id="USER_CONFIRM_PASSWORD" type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" />
		<?if ($arResult["USE_CAPTCHA"]):?>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="100%" height="40" alt="CAPTCHA" />
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
				</div>
			</div>
		<?endif?>
		<input type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" />
		<div class="bx-authform-description-container">
			<?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?>
		</div>
		<div class="registration_p">
			<a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a>
		</div>
	</form>
</div>

<script type="text/javascript">
document.bform.USER_LOGIN.focus();
</script>
