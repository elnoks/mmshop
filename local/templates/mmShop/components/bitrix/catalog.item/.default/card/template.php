<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="main_part">
    <div class="product_marks">
        <?
        if ($item['LABEL']) {
            if (!empty($item['LABEL_ARRAY_VALUE'])) {
                foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value) {
                    ?>
                    <span class="label-new label-orange" id="<?= $itemIds['STICKER_ID'] ?>">
                        <?
                        if ($code == "NOVINKA") {
                            echo "NEW";
                        } elseif ($code == "SPETSPREDLOZHENIE") {
                            echo "<i class=\"flaticon-signs spec\"></i>";
                        }else{
                            echo $value;
                        }
                        ?>
                    </span>
                    <?
                }
            }
        }
        if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && $price['PERCENT'] > 0) {
            ?>
            <span class="label-sale label-red" id="<?= $itemIds['DSC_PERC'] ?>">
                    <?= -$price['PERCENT'] ?>%
                </span>
            <?
        }
        ?>
    </div>
    <div class="product_action hidden-xs">
        <a href="javascript:void(0)" onclick="openCallForm('/', 'preview', '<?= $item["ID"] ?>')"
           data-toggle="modal" data-target="#modal_quick_<?= $item["ID"] ?>">
            <div class="preview_btn"></div>
        </a>
        <?php
        $favArr = checkFav();
        ?>
        <a id="<?=$item["ID"]?>favorite"
           href="javascript:void(0)"
            <?= in_array($item["ID"], $favArr) ? "class='active'" :""; echo "onclick = \"BX.addClass(BX(this), 'active');openCallForm('/', 'fav', '" . $item["ID"] . "', true)\""; ?>
           data-treevalue-fav=""
           data-onevalue-fav=""
        >
            <div class="favorite_btn"></div>
        </a>
    </div>
    <?php
    if ($item['PREVIEW_PICTURE']['SRC']) {
        $img = $item['PREVIEW_PICTURE']['SRC'];
    } elseif ($item['DETAIL_PICTURE']['SRC']) {
        $img = $item['DETAIL_PICTURE']['SRC'];
    } else {
        $img = $templateFolder . "/images/no_photo.png";
    }
    ?>
    <div class="product_item_image" title="<?= $imgTitle ?>" data-entity="image-wrapper">
        <span id="<?= $itemIds['PICT'] ?>"
              style="background-image: url('<?= $img ?>'); <?= ($showSlider ? 'display: none;' : '') ?>"></span>
    </div>
    <?php
    if (!empty($item['DISPLAY_PROPERTIES']["BREND"]["DISPLAY_VALUE"])) {
        ?>
        <div class="product_item_title">
            <?= $item['DISPLAY_PROPERTIES']["BREND"]["DISPLAY_VALUE"]; ?>
        </div>
        <?php
    }

    if (!empty($item['DISPLAY_PROPERTIES']["TIP_MODELI"]["VALUE"])) {
        ?>
        <div class="product_item_subtitle">
            <?= $item['DISPLAY_PROPERTIES']["TIP_MODELI"]["VALUE"]; ?>
        </div>
        <?php
    }
    ?>
    <div class="product_item_description">
        <a href="<?= $item['DETAIL_PAGE_URL'] ?>" title="<?= $productTitle ?>"><?= $productTitle ?></a>
    </div>

    <div class="product_item_price" data-entity="price-block">
        <?
        if ($arParams['SHOW_OLD_PRICE'] === 'Y') {
            ?>
            <span class="old_price" id="<?= $itemIds['PRICE_OLD'] ?>"
                <?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
								<?= $price['PRINT_RATIO_BASE_PRICE'] ?>
							</span>&nbsp;
            <?
        }
        ?>
        <span id="<?= $itemIds['PRICE'] ?>">
            <?
            if (!empty($price)) {
                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                    echo Loc::getMessage(
                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                        array(
                            '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                            '#VALUE#' => $measureRatio,
                            '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                        )
                    );
                } else {
                    echo $price['PRINT_RATIO_PRICE'];
                }
            }
            ?>
        </span>
    </div>
</div>

<div class="product_item_variations hidden-xs" id="<?=$item["ID"]?>skuFav">
    <?
    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP'])) {
        ?>
        <span id="<?= $itemIds['PROP_DIV'] ?>">
                <?

                foreach ($arParams['SKU_PROPS'] as $skuProperty) {
                    $propertyId = $skuProperty['ID'];
                    $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                    if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                        continue;
                    ?>
                    <div data-entity="sku-block">
                        <div data-entity="sku-line-block">
                            <ul class="variation_<?= $skuProperty["CODE"] == "RAZMERY" ? "size" : "color"; ?>">
                                <?
                                foreach ($skuProperty['VALUES'] as $value) {
                                    if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]) || $value["ID"] == 0)
                                        continue;

                                    $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                                    if ($skuProperty['SHOW_MODE'] === 'PICT' && $skuProperty["CODE"] != "RAZMERY") {
                                        ?>
                                        <li class="" title="<?= $value['NAME'] ?>"
                                            data-code="<?=$skuProperty["CODE"]?>"
                                            data-xml="<?=$value["XML_ID"]?>"
                                            data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                            data-onevalue="<?= $value['ID'] ?>">
                                            <a href="javascript:void(0)">
                                                <img src="<?= $value['PICT']['SRC'] ?>">
                                            </a>
                                        </li>
                                        <?
                                    } else {
                                        ?>
                                        <li class="" title="<?= $value['NAME'] ?>"
                                            data-code="<?=$skuProperty["CODE"]?>"
                                            data-xml="<?=$value["XML_ID"]?>"
                                            data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
                                            data-onevalue="<?= $value['ID'] ?>">
                                            <a href="javascript:void(0)"><?= $value['NAME'] ?></a>
                                        </li>
                                        <?
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <?
                }
                ?>
            </span>
        <?
        foreach ($arParams['SKU_PROPS'] as $skuProperty) {
            if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
                continue;

            $skuProps[] = array(
                'ID' => $skuProperty['ID'],
                'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                'VALUES' => $skuProperty['VALUES'],
                'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
            );
        }

        unset($skuProperty, $value);

        if ($item['OFFERS_PROPS_DISPLAY']) {
            foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer) {
                $strProps = '';

                if (!empty($jsOffer['DISPLAY_PROPERTIES'])) {
                    foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty) {
                        $strProps .= '<dt>' . $displayProperty['NAME'] . '</dt><dd>'
                            . (is_array($displayProperty['VALUE'])
                                ? implode(' / ', $displayProperty['VALUE'])
                                : $displayProperty['VALUE'])
                            . '</dd>';
                    }
                }

                $item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
            }
            unset($jsOffer, $strProps);
        }
    }
    ?>
    <span data-entity="buttons-block">
            <?
            if (!$haveOffers) {
                if ($actualItem['CAN_BUY']) {
                    ?>
                    <span id="<?= $itemIds['BASKET_ACTIONS'] ?>">
                        <a class="btn_orange mt20" id="<?= $itemIds['BUY_LINK'] ?>" href="javascript:void(0)"
                           rel="nofollow">
                            <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
                        </a>
                    </span>
                    <?
                } else {
                    ?>
                    <span>
                        <?
                        if ($showSubscribe) {
                            $APPLICATION->IncludeComponent(
                                'bitrix:catalog.product.subscribe',
                                '',
                                array(
                                    'PRODUCT_ID' => $actualItem['ID'],
                                    'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                    'BUTTON_CLASS' => 'btn btn-primary ' . $buttonSizeClass,
                                    'DEFAULT_DISPLAY' => true,
                                    'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                ),
                                $component,
                                array('HIDE_ICONS' => 'Y')
                            );
                        }
                        ?>
                        <a class="btn_orange mt20" id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" href="javascript:void(0)"
                           rel="nofollow">
                            <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                        </a>
                    </span>
                    <?
                }
            } else {
                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y') {
                    ?>

                    <?
                    if ($showSubscribe) {
                        $APPLICATION->IncludeComponent(
                            'bitrix:catalog.product.subscribe',
                            '',
                            array(
                                'PRODUCT_ID' => $item['ID'],
                                'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                'BUTTON_CLASS' => 'btn btn-primary ' . $buttonSizeClass,
                                'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                    }
                    ?>
                    <span>
                        <a class="btn_orange mt20" id="<?= $itemIds['NOT_AVAILABLE_MESS'] ?>" href="javascript:void(0)"
                           rel="nofollow"
                            <?= ($actualItem['CAN_BUY'] ? 'style="display: none;"' : '') ?>>
                            <?= $arParams['MESS_NOT_AVAILABLE'] ?>
                        </a>
                    </span>
                    <span id="<?= $itemIds['BASKET_ACTIONS'] ?>" <?= ($actualItem['CAN_BUY'] ? '' : 'style="display: none;"') ?>>
                        <a class="btn_orange mt20" id="<?= $itemIds['BUY_LINK'] ?>" href="javascript:void(0)"
                           rel="nofollow">
                            <?= ($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET']) ?>
                        </a>
                    </span>
                    <?
                } else {
                    ?>
                    <span>
                        <a class="btn_orange mt20" href="<?= $item['DETAIL_PAGE_URL'] ?>">
                            <?= $arParams['MESS_BTN_DETAIL'] ?>
                        </a>
                    </span>
                    <?
                }
            }
            ?>
        </span>
</div>

<?php
/*
	if (
		$arParams['DISPLAY_COMPARE']
		&& (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
	)
	{
		?>
		<div class="product-item-compare-container">
			<div class="product-item-compare">
				<div class="checkbox">
					<label id="<?=$itemIds['COMPARE_LINK']?>">
						<input type="checkbox" data-entity="compare-checkbox">
						<span data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></span>
					</label>
				</div>
			</div>
		</div>
		<?
	}
*/
?>
