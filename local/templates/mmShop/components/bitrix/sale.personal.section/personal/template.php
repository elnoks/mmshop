<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale;

Loader::includeModule('sale');
Loader::includeModule('catalog');

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$phone ='';
$rsUser = CUser::GetByID($USER->GetID());
if($arUserw = $rsUser->Fetch()){
    $phone = $arUserw["PERSONAL_PHONE"];
}
?>
    <div class="col-md-12 margin-b-15">
        <h1><?=CUser::GetFullName()?></h1>
        <p class="cab-sm-contacts">
            <span>
                <img src="<?=$this->__folder?>/images/icon-sm-email.png"><?=CUser::GetEmail()?>
            </span>
            <?php
            if(strlen($phone) > 0){
            ?>
            <span>
                <img src="<?=$this->__folder?>/images/icon-sm-tel.png"><?=$phone?>
            </span>
            <?
            }
            ?>
        </p>
        <p class="cab-edit-btn"><a href="<?=$arResult['PATH_TO_PRIVATE']?>" class="button btn-grey text-inherit"><?=Loc::getMessage("SPS_PERSONAL_PAGE_NAME_EDIT")?></a></p>
    </div>

<div class="row">
    <div class="col-md-12 lk-main flex-display">
        <?
        if ($arParams['SHOW_PROFILE_PAGE'] === 'Y')
        {
            $dbUserProps = CSaleOrderUserProps::GetList(
                array('DATE_UPDATE' => 'DESC'),
                array("USER_ID" => (int)($GLOBALS["USER"]->GetID())),
                false,
                false,
                array("ID")
            );
            $newProfList = $profileList = array();
            while($arUserProps = $dbUserProps->GetNext())
            {
                $profileProperties = Sale\OrderUserProperties::getProfileValues($arUserProps["ID"]);
                $profileList[$arUserProps["ID"]] = $profileProperties;
            }

            foreach ($profileList as $key => $profile){
                $arVal = Bitrix\Sale\Location\LocationTable::getList(array(
                        'filter' => array('=CODE' => $profile[6], 'NAME.LANGUAGE_ID' => 'ru'),
                        'limit' => 1,
                        'select' => array('LNAME' => 'NAME.NAME')
                    )
                )->fetch();
                if ( $arVal && strlen( $arVal[ 'LNAME' ] ) > 0 ){
                    if ($arVal && strlen($arVal['LNAME']) > 0) {
                        $newProfList[$key] .= $profile[4] . ' ' . Loc::getMessage("PRE_CITY", array("#CITY#" => $arVal['LNAME']));
                        if (!empty($profile[7]))
                            $newProfList[$key] .= Loc::getMessage("NUM", array("#NUM_STREET#" => $profile[7]));

                        if (!empty($profile[20]))
                            $newProfList[$key] .= Loc::getMessage("HOUSE", array("#NUM_HOUSE#" => $profile[20]));

                        if (!empty($profile[22]))
                            $newProfList[$key] .= Loc::getMessage("KV", array("#NUM_KV#" => $profile[22]));
                    }
                }
            }
            ?>
            <div class="cab-cover cab-adress-block">
                <h2><?=Loc::getMessage("SPS_MAIN_PROFILE")?></h2><a href="<?=$arResult['PATH_TO_ACCOUNT']?>" class="grey-link row"><?=Loc::getMessage("SPS_ADD_PROFILE")?></a>
                <div style="clear: both;"></div>
                <div class="lk-main flex-display row">
                    <?php
                    if(empty($newProfList)){
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            array(
                                "COMPONENT_TEMPLATE" => ".default",
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/include/emptyList.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );
                    }else {
                        ?>
                        <div class="col-xs-12 row">
                            <div class="select-cover col-sm-11 col-xs-10">
                                <div class="dropdown_mms select-cover">
                                    <span id="getParametr" data-profile="<?=key($newProfList)?>"><?= current($newProfList) ?></span>
                                    <ul>
                                        <?php
                                        foreach ($newProfList as $key => $prof) {
                                            ?>
                                            <li title="<?= $prof ?>" data-profile="<?= $key ?>">
                                                <a onclick="BX('getParametr').setAttribute('data-profile', '<?=$key?>')" href="javascript:void(0)"><?= $prof ?></a>
                                            </li>
                                            <?
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="cab-eit-btn-big col-sm-1 col-xs-2">
                                <a id="edit-button" onclick="choseProfile();" data-code="<?=current($newProfList)?>" href="javascript:void(0)"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
                if(!empty($newProfList)){
                ?>
                <p><a href="<?=$arResult['PATH_TO_PROFILE']?>" class="button btn-orange"><?=Loc::getMessage("SPS_SHOW_ALL_PROFILES")?></a></p>
                <?}
                ?>
            </div>
            <?
        }
        if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
        {
            $APPLICATION->IncludeComponent(
                "bitrix:sale.personal.order.list",
                "",
                array(
                    "PATH_TO_DETAIL" => $arResult["PATH_TO_ORDER_DETAIL"],
                    "PATH_TO_CANCEL" => $arResult["PATH_TO_ORDER_CANCEL"],
                    "PATH_TO_CATALOG" => $arParams["PATH_TO_CATALOG"],
                    "PATH_TO_COPY" => $arResult["PATH_TO_ORDER_COPY"],
                    "PATH_TO_BASKET" => $arParams["PATH_TO_BASKET"],
                    "PATH_TO_PAYMENT" => $arParams["PATH_TO_PAYMENT"],
                    "SAVE_IN_SESSION" => $arParams["SAVE_IN_SESSION"],
                    "ORDERS_PER_PAGE" => $arParams["ORDERS_PER_PAGE"],
                    "SET_TITLE" =>$arParams["SET_TITLE"],
                    "ID" => $arResult["VARIABLES"]["ID"],
                    "NAV_TEMPLATE" => $arParams["NAV_TEMPLATE"],
                    "ACTIVE_DATE_FORMAT" => $arParams["ACTIVE_DATE_FORMAT"],
                    "HISTORIC_STATUSES" => $arParams["ORDER_HISTORIC_STATUSES"],
                    "ALLOW_INNER" => $arParams["ALLOW_INNER"],
                    "ONLY_INNER_FULL" => $arParams["ONLY_INNER_FULL"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "DEFAULT_SORT" => $arParams["ORDER_DEFAULT_SORT"],
                    "RESTRICT_CHANGE_PAYSYSTEM" => $arParams["ORDER_RESTRICT_CHANGE_PAYSYSTEM"],
                    "REFRESH_PRICES" => $arParams["ORDER_REFRESH_PRICES"],
                ),
                $component
            );

            $filter = array(
                'filter' => array('USER_ID' => $USER->GetID(), 'LID' => SITE_ID),
                'select' => array('*'),
                'order' => array('ID' => 'DESC')
            );
            $arResult[] = array();
            $orderList = Sale\Order::getList($filter);
            if ($orderResult = $orderList->fetch()) {
                $arResultOrder[$orderResult["ID"]] = $orderResult;
                $basket = Sale\Order::load($orderResult["ID"])->getBasket();
                $arResultOrder[$orderResult["ID"]]["BASKET_COUNT"] = (count($basket->getQuantityList()));
            }
            ?>
            <div class="cab-cover cab-last-order">
                <h2><?=Loc::getMessage("SPOL_TPL_LAST")?></h2>
                <div class="flex-display">
                    <ul>
                        <li><strong><?=Loc::getMessage("SPOL_TPL_ID")?></strong>
                            <span>
                                <?=$arResultOrder[key($arResultOrder)]["ID"]?>
                            </span>
                        </li>
                        <li><strong><?=Loc::getMessage("SPOL_TPL_DATE")?></strong>
                            <span>
                                <?=FormatDate("j.m.Y", MakeTimeStamp($arResultOrder[key($arResultOrder)]["DATE_INSERT"]))?>
                            </span>
                        </li>
                    </ul>
                    <ul>
                        <li><strong><?=Loc::getMessage("SPOL_TPL_ITEM_SUMM")?></strong>
                            <span>
                                <?=$arResultOrder[key($arResultOrder)]["BASKET_COUNT"]?>
                            </span>
                        </li>
                        <li><strong><?=Loc::getMessage("SPOL_TPL_SUMM")?></strong>
                            <span>
                                <?=CurrencyFormat($arResultOrder[key($arResultOrder)]["PRICE"], $arResultOrder[key($arResultOrder)]["CURRENCY"])?>
                            </span>
                        </li>
                    </ul>
                </div>
                <p><a href="/personal/orders/" class="button btn-orange"><?=Loc::getMessage("SPOL_TPL_SHOW_ALL")?></a></p>
            </div>
        <?
        }
        ?>
    </div>
</div>
<div class="row lk-main flex-display">
    <div class="col-md-6 cab-discont-block">
        <div class="cab-cover">
            <h2><?=Loc::getMessage("SPS_DISCOUNT")?></h2>
            <p><?=Loc::getMessage("SPS_DISCOUNT_TEXT")?></p>
            <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/discountInfo.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                ); ?>
            <?php

            ?>
            <p class="cab-your-discont">
                <strong><?=Loc::getMessage("SPS_SHOW_ALL_DISCOUND")?><span style="color:#cb2026;"><?$APPLICATION->ShowViewContent("globalDiscount");?></span></strong>
                <strong><?=Loc::getMessage("SPS_SHOW_ALL_BUY")?><span><?$APPLICATION->ShowViewContent("globalBuy");?></span></strong>
            </p>
        </div>
        <div class="cab-cover cab-change-pass">
        <?$APPLICATION->IncludeComponent(
            "eoplatonov:changepass",
            ".default",
            Array(
                "AJAX_MODE" => "Y",
                "CHECK_RIGHTS" => "N",
                "COMPONENT_TEMPLATE" => ".default",
                "INCLUDE_JQUERY" => "N",
                "LAST_PASS" => "Y",
                "SEND_INFO" => "N",
                "SET_TITLE" => "N"
            ),
            $component
        );?>
        </div>
    </div>
    <div class="col-md-6 cab-cover_corr">
        <?php
        $APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket.line",
            "def_personal",
            array(
                "COMPONENT_TEMPLATE" => "fav",
                "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                "SHOW_NUM_PRODUCTS" => "Y",
                "SHOW_TOTAL_PRICE" => "N",
                "SHOW_EMPTY_VALUES" => "N",
                "SHOW_PERSONAL_LINK" => "N",
                "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                "SHOW_AUTHOR" => "N",
                "PATH_TO_AUTHORIZE" => "",
                "SHOW_REGISTRATION" => "N",
                "PATH_TO_REGISTER" => SITE_DIR . "auth/?register=yes",
                "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                "SHOW_PRODUCTS" => "Y",
                "SHOW_DELAY" => "Y",
                "SHOW_NOTAVAIL" => "N",
                "SHOW_IMAGE" => "Y",
                "SHOW_PRICE" => "Y",
                "SHOW_SUMMARY" => "Y",
                "POSITION_FIXED" => "Y",
                "HIDE_ON_BASKET_PAGES" => "N",
                "POSITION_HORIZONTAL" => "right",
                "POSITION_VERTICAL" => "top"
            ),
            false
        ); ?>
    </div>
    <div class="col-md-12 cab-change-pass-mobile">
        <?php
        $APPLICATION->ShowViewContent("changePassword");
        ?>
    </div>
</div>

<?

/*
if ($arParams['SHOW_ACCOUNT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ACCOUNT'],
		"name" => Loc::getMessage("SPS_ACCOUNT_PAGE_NAME"),
		"icon" => '<i class="fa fa-credit-card"></i>'
	);
}

if ($arParams['SHOW_PRIVATE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_PRIVATE'],
		"name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
		"icon" => '<i class="fa fa-user-secret"></i>'
	);
}

if ($arParams['SHOW_ORDER_PAGE'] === 'Y')
{

	$delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_ORDERS'].$delimeter."filter_history=Y",
		"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
		"icon" => '<i class="fa fa-list-alt"></i>'
	);
}



if ($arParams['SHOW_BASKET_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_BASKET'],
		"name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
		"icon" => '<i class="fa fa-shopping-cart"></i>'
	);
}

if ($arParams['SHOW_SUBSCRIBE_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arResult['PATH_TO_SUBSCRIBE'],
		"name" => Loc::getMessage("SPS_SUBSCRIBE_PAGE_NAME"),
		"icon" => '<i class="fa fa-envelope"></i>'
	);
}

if ($arParams['SHOW_CONTACT_PAGE'] === 'Y')
{
	$availablePages[] = array(
		"path" => $arParams['PATH_TO_CONTACT'],
		"name" => Loc::getMessage("SPS_CONTACT_PAGE_NAME"),
		"icon" => '<i class="fa fa-info-circle"></i>'
	);
}

$customPagesList = CUtil::JsObjectToPhp($arParams['~CUSTOM_PAGES']);
if ($customPagesList)
{
	foreach ($customPagesList as $page)
	{
		$availablePages[] = array(
			"path" => $page[0],
			"name" => $page[1],
			"icon" => (strlen($page[2])) ? '<i class="fa '.htmlspecialcharsbx($page[2]).'"></i>' : ""
		);
	}
}
//op($availablePages);
if (empty($availablePages))
{
	ShowError(Loc::getMessage("SPS_ERROR_NOT_CHOSEN_ELEMENT"));
}
else
{
	?>
	<div class="row">
		<div class="col-md-12 sale-personal-section-index">
			<div class="row sale-personal-section-row-flex">
				<?
				foreach ($availablePages as $blockElement)
				{
					?>
					<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
						<div class="sale-personal-section-index-block bx-theme-<?=$theme?>">
							<a class="sale-personal-section-index-block-link" href="<?=htmlspecialcharsbx($blockElement['path'])?>">
								<span class="sale-personal-section-index-block-ico">
									<?=$blockElement['icon']?>
								</span>
								<h2 class="sale-personal-section-index-block-name">
									<?=htmlspecialcharsbx($blockElement['name'])?>
								</h2>
							</a>
						</div>
					</div>
					<?
				}
				?>
			</div>
		</div>
	</div>
	<?
}
*/
?>
