<?php
$MESS["SPS_CHAIN_MAIN"] = "Мой кабинет";
$MESS["SPS_CHAIN_ORDERS"] = "Мои заказы";
$MESS["SPS_ORDER_NUM"] = '№ заказа';
$MESS["SPS_ORDER_DATE"] ='Дата заказа';
$MESS["SPS_ORDER_COUNT_BASKET"] ='Количество товаров';
$MESS["SPS_ORDER_SUM"] = 'Сумма заказа';
$MESS["SPS_ORDER_DELIVERY"] = 'Способ доставки';
$MESS["SPS_ORDER_STATUS"] = 'Статус';
$MESS["SPS_ORDER_STATUS_F"] = '	Выполнен';
$MESS["SPS_ORDER_STATUS_N"] = '	Принят, ожидается оплата';
$MESS["SPS_ORDER_STATUS_P"] = '	Оплачен, формируется к отправке';
$MESS["SPS_ORDER_STATUS_DF"] = 'Отгружен';
$MESS["SPS_ORDER_STATUS_DN"] = 'Ожидает обработки';
$MESS["SPS_ORDER_BACK"] = 'Назад';