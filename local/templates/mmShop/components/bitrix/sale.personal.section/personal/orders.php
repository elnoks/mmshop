<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Sale;

if ($arParams['SHOW_ORDER_PAGE'] !== 'Y') {
    LocalRedirect($arParams['SEF_FOLDER']);
}

if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0) {
    $APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ORDERS"), $arResult['PATH_TO_ORDERS']);

$filter = array(
    'filter' => array('USER_ID' => $USER->GetID(), 'LID' => SITE_ID),
    'select' => array('*'),
    'order' => array('ID' => 'DESC')
);
$arResultOrder[] = array();
$orderList = Sale\Order::getList($filter);
while ($orderResult = $orderList->fetch()) {
    $arResultOrder[$orderResult["ID"]] = $orderResult;
    $basket = Sale\Order::load($orderResult["ID"])->getBasket();
    $arResultOrder[$orderResult["ID"]]["BASKET_COUNT"] = (count($basket->getQuantityList()));
}
?>
<div class="row">
    <div class="col-md-12">
        <table class="order-table">
            <tr>
                <th><?= Loc::getMessage("SPS_ORDER_NUM") ?></th>
                <th><?= Loc::getMessage("SPS_ORDER_DATE") ?></th>
                <th><?= Loc::getMessage("SPS_ORDER_COUNT_BASKET") ?></th>
                <th><?= Loc::getMessage("SPS_ORDER_SUM") ?></th>
                <th><?= Loc::getMessage("SPS_ORDER_DELIVERY") ?></th>
                <th><?= Loc::getMessage("SPS_ORDER_STATUS") ?></th>
                <th></th>
            </tr>
            <?php
            foreach ($arResultOrder as $order) {
                ?>
                <tr>
                    <td><a href="" data-toggle="modal" data-target=".my-order-popup"
                           onclick="openOrderForm('/', 'orderCard', '<?= $order["ID"] ?>')"><?= $order["ID"] ?></a></td>
                    <td><?= FormatDate("j.m.Y", MakeTimeStamp($order["DATE_INSERT"])) ?></td>
                    <td><?= $order["BASKET_COUNT"] ?></td>
                    <td><?= CurrencyFormat($order["PRICE"], $order["CURRENCY"]); ?></td>
                    <td><?= CSaleDelivery::GetByID($order["DELIVERY_ID"])["NAME"] ?></td>
                    <td><?= Loc::getMessage("SPS_ORDER_STATUS_" . $order["STATUS_ID"]) ?><?= CSaleOrder::StatusOrder($order["ID"], $order["STATUS_ID"]) ?></td>
                    <td></td>
                </tr>
                <?
            }
            ?>
        </table>
        <div class="cab-mobile-orders">
            <div>
                <?php
                foreach ($arResultOrder as $order) {
                    ?>
                    <ul>
                        <li><strong><?= Loc::getMessage("SPS_ORDER_NUM") ?></strong><span><a href="javascript:void(0)" class="grey-link" data-toggle="modal" data-target=".my-order-popup"
                               onclick="openOrderForm('/', 'orderCard', '<?= $order["ID"] ?>')"><?= $order["ID"] ?></a></span>
                        </li>
                        <li><strong><?= Loc::getMessage("SPS_ORDER_DATE") ?></strong><span><?= FormatDate("j.m.Y", MakeTimeStamp($order["DATE_INSERT"])) ?></span></li>
                        <li><strong><?= Loc::getMessage("SPS_ORDER_COUNT_BASKET") ?></strong><span><?= $order["BASKET_COUNT"] ?></span></li>
                        <li><strong><?= Loc::getMessage("SPS_ORDER_SUM") ?></strong><span><?= CurrencyFormat($order["PRICE"], $order["CURRENCY"]); ?></span></li>
                        <li><strong><?= Loc::getMessage("SPS_ORDER_DELIVERY") ?></strong><span><?= CSaleDelivery::GetByID($order["DELIVERY_ID"])["NAME"] ?></span></li>
                        <li><strong><?= Loc::getMessage("SPS_ORDER_STATUS") ?></strong><span><?= Loc::getMessage("SPS_ORDER_STATUS_" . $order["STATUS_ID"]) ?><?= CSaleOrder::StatusOrder($order["ID"], $order["STATUS_ID"]) ?></span></li>
                    </ul>
                    <?
                }
                ?>
            </div>
        </div>
        <p><a href="/personal/" class="button btn-grey text-inherit"><?= Loc::getMessage("SPS_ORDER_BACK") ?></a></p>
    </div>
</div>
<div class="modal fade my-order-popup" id="orderCard" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true"></div>
