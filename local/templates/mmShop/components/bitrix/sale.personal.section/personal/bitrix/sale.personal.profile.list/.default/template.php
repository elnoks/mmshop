<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

//if (strlen($arResult["ERROR_MESSAGE"]) > 0) {
//    ShowError($arResult["ERROR_MESSAGE"]);
//}
if (strlen($arResult["NAV_STRING"]) > 0) {
    ?>
    <div class="row mb-3">
        <div class="col"><?= $arResult["NAV_STRING"] ?></div>
    </div>
    <?
}

if (count($arResult["PROFILES"])) {
    ?>

    <div class="row">
        <div class="col-md-12">
            <h1><?= Loc::getMessage("MAIN_HEAD") ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="order-table adress-table">
                <thead>
                <tr>
                    <?
                    $dataColumns = array(
                        "ADDRESS", "EDIT", "DELETE"
                    );
                    foreach ($dataColumns as $column) {
                        ?>
                        <th>
                            <?= Loc::getMessage("P_" . $column) ?>
                        </th>
                        <?
                    }
                    ?>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?
                foreach ($arResult["PROFILES"] as $val) {
                    ?>
                    <tr>
                        <td scope="row"><?= $val["ADDRESS"] ?></td>

                        <td>
                            <a class="cab-edit-icon"
                               title="<?= Loc::getMessage("SALE_DETAIL_DESCR") ?>"
                               href="<?= $val["URL_TO_DETAIL"] ?>">

                            </a>
                        </td>
                        <td>
                            <a class="cab-del-icon"
                               title="<?= Loc::getMessage("SALE_DELETE_DESCR") ?>"
                               href="javascript:if(confirm('<?= Loc::getMessage("STPPL_DELETE_CONFIRM") ?>')) window.location='<?= $val["URL_TO_DETELE"] ?>'">
                            </a>
                        </td>
                        <td></td>
                    </tr>
                    <?
                } ?>
                </tbody>
            </table>
        </div>
    </div>
    <?
    if (strlen($arResult["NAV_STRING"]) > 0) {
        ?>
        <div class="row">
            <div class="col"><?= $arResult["NAV_STRING"] ?></div>
        </div>
        <?
    }
} else {
    ?>
    <h3><?= Loc::getMessage("STPPL_EMPTY_PROFILE_LIST") ?></h3>
    <?
}
?>
<p class="cab-buttons-wrap"><a style="vertical-align: top; margin-right: 20px;" href="/personal/" class="button btn-grey text-inherit"><?= Loc::getMessage("SALE_BACK") ?></a>
<a href="/personal/account/" class="button btn-orange"><?= Loc::getMessage("SALE_NEW") ?></a>