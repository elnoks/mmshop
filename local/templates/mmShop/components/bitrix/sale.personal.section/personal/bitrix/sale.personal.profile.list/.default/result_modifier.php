<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Sale;

$dbUserProps = CSaleOrderUserProps::GetList(
    array('DATE_UPDATE' => 'DESC'),
    array("USER_ID" => (int)($GLOBALS["USER"]->GetID())),
    false,
    false,
    array("ID")
);
$newProfList = $profileList = array();
while ($arUserProps = $dbUserProps->GetNext()) {
    $profileProperties = Sale\OrderUserProperties::getProfileValues($arUserProps["ID"]);
    $profileList[$arUserProps["ID"]] = $profileProperties;
}

foreach ($profileList as $key => $profile) {
    $arVal = Bitrix\Sale\Location\LocationTable::getList(array(
            'filter' => array('=CODE' => $profile[6], 'NAME.LANGUAGE_ID' => 'ru'),
            'limit' => 1,
            'select' => array('LNAME' => 'NAME.NAME')
        )
    )->fetch();

    if ($arVal && strlen($arVal['LNAME']) > 0) {
        $newProfList[$key] .= $profile[4] . ' ' . Loc::getMessage("PRE_CITY", array("#CITY#" => $arVal['LNAME']));
        if (!empty($profile[7]))
            $newProfList[$key] .= Loc::getMessage("NUM", array("#NUM_STREET#" => $profile[7]));

        if (!empty($profile[20]))
            $newProfList[$key] .= Loc::getMessage("HOUSE", array("#NUM_HOUSE#" => $profile[20]));

        if (!empty($profile[22]))
            $newProfList[$key] .= Loc::getMessage("KV", array("#NUM_KV#" => $profile[22]));
    }
}

$arrRes = array();
foreach ($arResult["PROFILES"] as $key => $profile) {
    $arrRes[$key] = $profile;
    $arrRes[$key]["ADDRESS"] = $newProfList[$profile["ID"]];
}
$arResult["PROFILES"] = $arrRes;