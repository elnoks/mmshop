<?
$MESS['PROFILE_DATA_SAVED'] = "Изменения сохранены";
$MESS['LAST_UPDATE'] = "Дата обновления:";
$MESS['ACTIVE'] = "Активен:";
$MESS['NAME'] = "Имя:";
$MESS['LAST_NAME'] = "Фамилия:";
$MESS['SECOND_NAME'] = "Отчество:";
$MESS['EMAIL'] = "E-Mail:";
$MESS['MAIN_RESET'] = "Отмена";
$MESS['LOGIN'] = "Логин (мин. 3 символа):";
$MESS['NEW_PASSWORD'] = "Новый пароль (мин. 6 символов):";
$MESS['NEW_PASSWORD_CONFIRM'] = "Подтверждение нового пароля:";
$MESS['SAVE'] = "Сохранить изменения";
$MESS['RESET'] = "Сбросить";
$MESS['LAST_LOGIN'] = "Последняя авторизация:";
$MESS['NEW_PASSWORD_REQ'] = "Новый пароль:";
$MESS['PERSONAL_PHONE'] = "Телефон";
$MESS['PERSONAL_SEX'] = "Пол";
$MESS['USER_MALE'] = "М";
$MESS['USER_FEMALE'] = "Ж";
$MESS['MAIN_BACK'] = "Назад";
$MESS['AUTH_SECURE_PRIVACY'] = "Я согласен на <a href='/privacy/'>обработку персональных данных</a>";
$MESS['EDIT_PROFILE'] = "Редактирование учётной записи";
