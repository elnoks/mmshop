<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (strlen($arResult["ID"]) > 0) {
    $rsUser = CUser::GetByID($USER->GetID());
    if ($arUserw = $rsUser->Fetch()) {
        $phone = $arUserw["PERSONAL_PHONE"];
        $email = $arUserw["EMAIL"];
    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <h1><?= Loc::getMessage('SPPD_EDIT_ADDRESS'); ?></h1>
        </div>
    </div>
    <?php
    ShowError($arResult["ERROR_MESSAGE"]);
    ?>
    <hr>
    <form method="post" action="<?= POST_FORM_ACTION_URI ?>" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="sm-input-wrap">
                    <?= bitrix_sessid_post() ?>
                    <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>">
                    <input type="hidden" name="NAME" id="sale-personal-profile-detail-name"
                           value="<?= CUser::GetFullName() ?>"/>
                    <?
                    foreach ($arResult["ORDER_PROPS"] as $keyB => $block) {
                    if (!empty($block["PROPS"])) {
                    if ($keyB == 1) {
                        foreach ($block["PROPS"] as $key => $property) {
                            $name = "ORDER_PROP_" . (int)$property["ID"];
                            if ($property["CODE"] == "EMAIL")
                                $currentValue = $email;
                            elseif ($property["CODE"] == "PHONE")
                                $currentValue = $phone;
                            elseif ($property["CODE"] == "FIO")
                                $currentValue = CUser::GetFullName();
                            ?>
                            <input
                                    type="hidden" name="<?= $name ?>"
                                    maxlength="50"
                                    id="sppd-property-<?= $key ?>"
                                    value="<?= $currentValue ?>"/>
                            <?
                        }
                    } else {

                    foreach ($block["PROPS"] as $key => $property) {

                    $pos = strpos($property["CODE"], "BEGIN");
                    if ($pos !== false)
                        echo "<div class='flex-for-input'>";


                    $name = "ORDER_PROP_" . (int)$property["ID"];
                    $currentValue = $arResult["ORDER_PROPS_VALUES"][$name];

                    if ($property["CODE"] != "DEFAULT_BUTTON") {
                    ?>
                    <label for="sppd-property-<?= $key ?>">
                        <?= $property["NAME"] ?>
                        <?php
                        $pos = strpos($property["CODE"], "CITY");
                        if ($pos === false)
                            echo "</label>";
                        }
                        if ($property["TYPE"] == "TEXT") {
                            ?>
                            <input
                                    type="<?= $property["CODE"] == "DEFAULT_BUTTON" ? "hidden":"text"?>"
                                    name="<?= $name ?>"
                                    maxlength="50"
                                    id="sppd-property-<?= $key ?>"
                                    value="<?= $currentValue ?>"/>
                        <?
                        $pos = strpos($property["CODE"], "CITY");
                        if ($pos !== false)
                            echo "</label>";

                        $pos = strpos($property["CODE"], "END");
                        if ($pos !== false)
                            echo "</div>";

                        if ($property["CODE"] == "DEFAULT_BUTTON") {
                        ?>
                            <input
                                    id="checkbox"
                                    type="checkbox"
                                    class="checkbox"
                                    name="checkbox"
                                    value=""
                                <?
                                if ($currentValue == "Y" || !isset($currentValue) && $property["DEFAULT_VALUE"] == "Y") echo " checked"; ?>
                            />
                            <label for="checkbox"><?= $property["NAME"] ?></label>
                            <script>
                                BX.ready(function(){


                                $('#checkbox').click(function () {
                                    if ($('#checkbox').is(':checked')) {
                                        BX('sppd-property-<?=$key?>').setAttribute("value", "Y");
                                    } else {
                                        BX('sppd-property-<?=$key?>').setAttribute("value", "N");
                                    }
                                });
                                });
                            </script>
                            <?

                        }

                        } elseif ($property["TYPE"] == "LOCATION") {
                            $locationTemplate = ($arParams['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
                            $locationClassName = 'location-block-wrapper';
                            $locationValue = (int)($currentValue) ? (int)$currentValue : $property["DEFAULT_VALUE"];

                            CSaleLocation::proxySaleAjaxLocationsComponent(
                                array(
                                    "AJAX_CALL" => "N",
                                    'CITY_OUT_LOCATION' => 'Y',
                                    'COUNTRY_INPUT_NAME' => $name . '_COUNTRY',
                                    'CITY_INPUT_NAME' => $name,
                                    'LOCATION_VALUE' => $locationValue,
                                ),
                                array(),
                                $locationTemplate,
                                true,
                                'location-block-wrapper'
                            );

                        }
                        }
                        }
                        }
                        }
                        ?>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12 cab-buttons-wrap">
                <a href="javascript:void(0)" onclick="javascript:history.back(); return false;" style="margin-right: 20px; vertical-align: bottom;"
                   class="mw_btn button btn-grey text-inherit"><?= Loc::getMessage('SPPD_BACK'); ?></a>
                <input type="submit" class="mw_btn button btn-orange" name="apply" onclick="javascript:history.back();"
                       value="<?= GetMessage("SALE_APPLY") ?>">
            </div>
        </div>
    </form>
    <?
    $javascriptParams = array(
        "ajaxUrl" => CUtil::JSEscape($this->__component->GetPath() . '/ajax.php'),
    );
    $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
    ?>
    <script>
        BX.message({
            SPPD_FILE_COUNT: '<?=Loc::getMessage('SPPD_FILE_COUNT')?>',
            SPPD_FILE_NOT_SELECTED: '<?=Loc::getMessage('SPPD_FILE_NOT_SELECTED')?>'
        });
    </script>
    <?
} else {
    ShowError($arResult["ERROR_MESSAGE"]);
}
?>

