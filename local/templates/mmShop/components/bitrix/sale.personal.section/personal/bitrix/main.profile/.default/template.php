<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;

?>
<div class="row">
    <div class="col-md-12">
        <h1><?= Loc::getMessage('EDIT_PROFILE') ?></h1>
    </div>
</div>
<hr>
<?
//ShowError($arResult["strProfileError"]);
//
//if ($arResult['DATA_SAVED'] == 'Y') {
//    ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
//}

?>
<form method="post" name="form1" action="<?= $APPLICATION->GetCurUri() ?>" enctype="multipart/form-data" role="form">
    <?= $arResult["BX_SESSION_CHECK"] ?>
    <input type="hidden" name="lang" value="<?= LANG ?>"/>
    <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>"/>
    <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>"/>
    <div class="row">
        <div class="col-md-6">
            <div class="sm-input-wrap md-input-wrap">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="main-profile-name"><?= Loc::getMessage('NAME') ?></label>

                        <input type="text" name="NAME" maxlength="50" id="main-profile-name"
                               value="<?= $arResult["arUser"]["NAME"] ?>"/>
                    </div>
                    <div class="col-sm-6">
                        <label for="main-profile-last-name"><?= Loc::getMessage('LAST_NAME') ?></label>
                        <input type="text" name="LAST_NAME" maxlength="50"
                               id="main-profile-last-name" value="<?= $arResult["arUser"]["LAST_NAME"] ?>"/>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="main-profile-email"><?= Loc::getMessage('PERSONAL_PHONE') ?></label>

                        <input type="text" name="PERSONAL_PHONE" maxlength="50" id="main-profile-email"
                               value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>"/>
                    </div>
                    <div class="col-sm-6">
                        <label for="main-profile-email"><?= Loc::getMessage('EMAIL') ?></label>

                        <input type="text" name="EMAIL" maxlength="50" id="main-profile-email"
                               value="<?= $arResult["arUser"]["EMAIL"] ?>"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="PERSONAL_GENDER" id="gender" value="<?=$arResult["arUser"]["PERSONAL_GENDER"]?>">
                        <label for=""><?= Loc::getMessage('PERSONAL_SEX') ?></label><br>
                        <a href="javascript:void(0)"
                           id="gender_m"
                           onclick="BX.addClass(BX(this), ' active-sex'); BX.removeClass(BX('gender_f'), ' active-sex'); BX('gender').setAttribute('value', 'M')"
                           class="sex <?=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? "active-sex" : ""?>"><?= Loc::getMessage('USER_MALE') ?>
                        </a>
                        <a href="javascript:void(0)"
                           id="gender_f"
                           onclick="BX.addClass(BX(this), ' active-sex'); BX.removeClass(BX('gender_m'), ' active-sex'); BX('gender').setAttribute('value', 'F')"
                           class="sex <?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? "active-sex" : ""?>"><?= Loc::getMessage('USER_FEMALE') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12 cab-buttons-wrap">
            <a href="/personal/" style="margin-right: 20px; vertical-align: bottom;" class="button btn-grey text-inherit"><?= Loc::getMessage("MAIN_BACK")?></a>

            <input id="submitForm" type="submit" name="save" class="button btn-orange"
               value="<?= Loc::getMessage("MAIN_SAVE")?>">

            <p class="cab-personal-data-wrap">
                <input type="checkbox" checked class="checkbox" id="checkbox" />
                <label class="personal-data" for="checkbox"><?= Loc::getMessage("AUTH_SECURE_PRIVACY")?></label>
            </p>
        </div>
    </div>
</form>
<script type="text/javascript">
    $('#checkbox').change(function(){
        if(this.checked != true)
        {
            BX.addClass('submitForm', 'unclick');
            return false;
        }else{
            BX.removeClass('submitForm', 'unclick');
            return true;
        }
    });
</script>