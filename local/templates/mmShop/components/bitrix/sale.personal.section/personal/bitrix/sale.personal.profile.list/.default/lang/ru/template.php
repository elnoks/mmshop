<?
$MESS ['MAIN_HEAD'] = "Мои адреса";
$MESS ['SALE_BACK'] = "Назад";
$MESS ['SALE_NEW'] = "Новый адрес";
$MESS ['P_ADDRESS'] = "Адрес";
$MESS ['P_EDIT'] = "Редактировать";
$MESS ['P_DELETE'] = "Удалить";
$MESS ['P_ID'] = "Код";
$MESS ['P_NAME'] = "Название";
$MESS ['P_PERSON_TYPE_ID'] = "Тип плательщика";
$MESS ['P_DATE_UPDATE'] = "Дата обновления";
$MESS ['SALE_ACTION'] = "Действия";
$MESS ['SALE_DETAIL'] = "Изменить";
$MESS ['SALE_DELETE'] = "Удалить";
$MESS ['SALE_DETAIL_DESCR'] = "Изменить профиль";
$MESS ['SALE_DELETE_DESCR'] = "Удалить профиль";
$MESS["STPPL_DELETE_CONFIRM"] = "Вы уверены, что хотите удалить этот профиль?";
$MESS["STPPL_EMPTY_PROFILE_LIST"] = "Список профилей пуст";
$MESS["PRE_CITY"] = "г. #CITY#";
$MESS["NUM"] = ", ул. #NUM_STREET#";
$MESS["HOUSE"] = ", #NUM_HOUSE#";
$MESS["KV"] = ", кв. #NUM_KV#";
?>
