<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Localization\Loc;

$templateFolder = $this->GetFolder();

$APPLICATION->AddChainItem(Loc::getMessage("SPPD_ADD"), '/');

$rsUser = CUser::GetByID($USER->GetID());
if ($arUserw = $rsUser->Fetch()) {
    $phone = $arUserw["PERSONAL_PHONE"];
    $email = $arUserw["EMAIL"];
}
?>

<div class="col-md-12">
    <h1><?= Loc::getMessage('SPPD_ADD'); ?></h1>
</div>

<? if (!empty($_SESSION["MSG_PROFILE"])): ?>
    <div class="col-md-12">
        <p  class="<?= $_SESSION["MSG_PROFILE_TYPE"] ?>"><?= $_SESSION["MSG_PROFILE"] ?></p>
    </div>
    <? unset($_SESSION["MSG_PROFILE"]); ?>
<? endif ?>
<div class="f_dev"></div>
<form action="<?= $APPLICATION->GetCurPage() ?>" method="post">
        <?= bitrix_sessid_post() ?>
    <?foreach($arResult["PERSON_TYPE"] as $key=>$ptype):?>

        <input type="hidden" class="change-p-type" <?if($ptype["CHECKED"]):?>checked="checked" <?endif?> id="ptype-<?=$ptype["ID"]?>" name="PERSON_TYPE_ID" data-url="<?=$APPLICATION->GetCurPageParam('PERSON_TYPE_ID='.$ptype["ID"],array("PERSON_TYPE_ID"))?>" value="<?=$ptype["ID"]?>">
    <?endforeach?>
        <input type="hidden" name="PROFILE_NAME" id="sale-personal-profile-detail-name"
               value="<?= CUser::GetFullName().time() ?>"/>
    <div class="col-md-12">
        <div class="sm-input-wrap">
        <? foreach ($arResult["PROFILE_PROPS"] as $key => $grpoup) {
            if ($key == 1) {
                foreach ($grpoup as $item) {
                    $name = "ORDER_PROP_" . (int)$item["ID"];
                    if ($item["CODE"] == "EMAIL")
                        $currentValue = $email;
                    elseif ($item["CODE"] == "PHONE")
                        $currentValue = $phone;
                    elseif ($item["CODE"] == "FIO")
                        $currentValue = CUser::GetFullName();
                    ?>
                <input type="hidden"
                       name="PROP_<?= $item["ID"] ?>_<?= $item["TYPE"] ?>"
                       id="profile-input-<?= $item["ID"] ?>"
                       value="<?= (isset($currentValue)) ? $currentValue : $item["DEFAULT_VALUE"]; ?>">
                <?
                }
            } else {
                foreach ($grpoup as $property) {

                    $pos = strpos($property["CODE"], "BEGIN");
                    if ($pos !== false)
                        echo "<div class='flex-for-input'>";

                    $name = "ORDER_PROP_" . (int)$property["ID"];
                    $currentValue = $arResult["ORDER_PROPS_VALUES"][$name];

                    if ($property["CODE"] != "DEFAULT_BUTTON") {
                    ?>
                            <label for="PROP_<?= $item["ID"] ?>_<?= $item["TYPE"] ?>">
                        <?= $property["NAME"] ?>
                        <?php
                        $pos = strpos($property["CODE"], "CITY");
                        if ($pos === false)
                            echo "</label>";
                    }
                    if ($property["TYPE"] == "TEXT") {
                    ?>
                        <input type="<?= $property["CODE"] == "DEFAULT_BUTTON" ? "hidden" : "text" ?>"
                               name="PROP_<?= $property["ID"] ?>_<?= $item["TYPE"] ?>"
                               class="grey-input <?
                               if (in_array($item["ID"], $_SESSION["PROFILE"]["VALIDATE"])):?>error<? endif ?>"
                               id="profile-input-<?= $property["ID"] ?>"
                               value="<?= (isset($currentValue)) ? $currentValue : $property["DEFAULT_VALUE"]; ?>">
                        <?
                        $pos = strpos($property["CODE"], "CITY");
                        if ($pos !== false)
                            echo "</label>";

                        $pos = strpos($property["CODE"], "END");
                        if ($pos !== false)
                            echo "</div>";

                        if ($property["CODE"] == "DEFAULT_BUTTON") {
                        ?>
                        <input
                                id="checkbox"
                                type="checkbox"
                                class="checkbox"
                                name="checkbox"
                                value=""
                            <?
                            if ($currentValue == "Y" || !isset($currentValue) && $property["DEFAULT_VALUE"] == "Y") echo " checked"; ?>
                        />
                            <label for="checkbox"><?= $property["NAME"] ?></label>
                            <script>
                                BX.ready(function () {
                                    $('#checkbox').click(function () {
                                        if ($('#checkbox').is(':checked')) {
                                            BX('profile-input-<?= $property["ID"] ?>').setAttribute("value", "Y");
                                        } else {
                                            BX('profile-input-<?= $property["ID"] ?>').setAttribute("value", "N");
                                        }
                                    });
                                });
                            </script>
                            <?

                        }

                    } elseif ($property["TYPE"] == "LOCATION") {
                        $locationTemplate = ($arParams['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
                        $locationClassName = 'location-block-wrapper';
                        $locationValue = intval($currentValue) ? $currentValue : $property["DEFAULT_VALUE"];

                        CSaleLocation::proxySaleAjaxLocationsComponent(
                            array(
                                "AJAX_CALL" => "Y",
                                'CITY_OUT_LOCATION' => 'Y',
                                'COUNTRY_INPUT_NAME' => 'PROP_' . $item["ID"],
                                'CITY_INPUT_NAME' => 'PROP_' . $item["ID"],
                                'LOCATION_VALUE' => $property["DEFAULT_VALUE"],
                            ),
                            array(),
                            $locationTemplate,
                            true,
                            'location-block-wrapper'
                        );
                    }
                }
            }
        }
        ?>
                    </div>
                    </div>
        <div class="f_dev mt10"></div>
        <div class="row">
            <div class="col-md-12 cab-buttons-wrap">
                <a href="/personal/profiles/" style="margin-right: 20px; vertical-align: bottom;"
                   class="mw_btn button btn-grey text-inherit"><?= Loc::getMessage('SALE_BACK') ?></a>
                <button type="submit" name="save" value="Y"
                        class="mw_btn button btn-orange"><?= Loc::getMessage('SALE_SAVE') ?></button>
            </div>
        </div>
</form>


<? unset($_SESSION["PROFILE"]) ?>
