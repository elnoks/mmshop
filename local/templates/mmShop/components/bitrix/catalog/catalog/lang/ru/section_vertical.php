<?
$MESS["CT_GIFTS_SECTION_LIST_BLOCK_TITLE_DEFAULT"] = "Подарки к товарам этого раздела";
$MESS["CLOSE_DESCR"] = "Свернуть описание";
$MESS["CT_BCS_CATALOG_MESS_SHOW_BY"] = "Показать с начала: ";
$MESS["CT_BCS_CATALOG_MESS_SHOW_BY_NEW"] = "новые";
$MESS["CT_BCS_CATALOG_MESS_SHOW_BY_USER"] = "по умолчанию";
$MESS["CT_BCS_CATALOG_MESS_SHOW_BY_COST"] = "дороже";
$MESS["CT_BCS_CATALOG_MESS_SHOW_BY_CHEAP"] = "дешевле";
$MESS["CT_BCS_CATALOG_MESS_SHOW_BY_SALE"] = "со скидкой";