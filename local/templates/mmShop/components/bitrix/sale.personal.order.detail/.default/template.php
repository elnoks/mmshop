<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$APPLICATION->SetTitle("");

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }

    $component = $this->__component;

    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }
} else {
    if (!empty($arResult['ERRORS']['NONFATAL'])) {
        foreach ($arResult['ERRORS']['NONFATAL'] as $error) {
            ShowError($error);
        }
    }
    ?>
    <div class="modal-dialog modal-lg modal-for-order">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                            src="<?= $this->__folder ?>/images/modal-close.png"></button>
                <h1 class="modal-title">
                    <?= Loc::getMessage('SPOD_LIST_MY_ORDER', array('#ACCOUNT_NUMBER#' => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]))) ?>
                </h1>
                <div class="modal-body mCustomScrollbar">
                    <table class="table-order-popup">
                        <tr>
                            <th></th>
                            <th><?= Loc::getMessage('SPOD_ORDER_BASKET_NAME') ?></th>
                            <th><?= Loc::getMessage('SPOD_ORDER_BASKET_SIZE') ?></th>
                            <th><?= Loc::getMessage('SPOD_ORDER_BASKET_QUANTITY') ?></th>
                            <th><?= Loc::getMessage('SPOD_ORDER_BASKET_PRICE') ?></th>
                        </tr>
                        <?
                        $disPrise = 0;
                        foreach ($arResult['BASKET'] as $basketItem) {
                            $disPrise = $disPrise + $basketItem["DISCOUNT_PRICE"];
                            ?>
                            <tr>
                                <td width="160px;">
                                    <?
                                    if (strlen($basketItem['PICTURE']['SRC']))
                                        $imageSrc = $basketItem['PICTURE']['SRC'];
                                    else
                                        $imageSrc = $this->GetFolder().'/images/no_photo.png';
                                    ?>
                                    <div class="order-sm-image-wrap">
                                        <img src="<?=$imageSrc?>">
                                    </div>
                                </td>
                                <td>
                                    <?php
                                    $db_props = CIBlockElement::GetProperty($basketItem["PARENT"]["IBLOCK_ID"], $basketItem["PARENT"]["ID"], array("sort" => "asc"), Array("CODE" => "BREND"));
                                    if ($ar_props = $db_props->Fetch()) {
                                        echo "<p><strong>".getFromHighloadbyXmlId(1, $ar_props["VALUE"])."</strong></p>";
                                    }
                                    $db_props = CIBlockElement::GetProperty($basketItem["PARENT"]["IBLOCK_ID"], $basketItem["PARENT"]["ID"], array("sort" => "asc"), Array("CODE" => "TIP_MODELI"));
                                    if ($ar_props = $db_props->Fetch())
                                        echo "<p>" . $ar_props["VALUE_ENUM"] . "</p>";
                                    ?>
                                    <p><?=$basketItem["NAME"]?></p>
                                </td>
                                <td>
                                    <?php
                                    $db_propsR = CIBlockElement::GetProperty($basketItem["PARENT"]["OFFER_IBLOCK_ID"], $basketItem["PRODUCT_ID"], array("sort" => "asc"), Array("CODE" => "RAZMERY"));
                                    if($ar_props = $db_propsR->Fetch()){
                                        echo getFromHighloadbyXmlId(3, $ar_props["VALUE"]);
                                    }
                                    ?>
                                </td>
                                <td><?=$basketItem["QUANTITY"]?></td>
                                <td>
                                    <?php
                                    if($basketItem["BASE_PRICE"] >  $basketItem["PRICE"]){
                                        echo "<strike>".$basketItem["BASE_PRICE_FORMATED"]."</strike>";
                                    }
                                    echo $basketItem["PRICE_FORMATED"];
                                    ?>
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </table>
                </div>
                <?php
                $addres = array();
                foreach ($arResult["ORDER_PROPS"] as $prop){
                    if($prop["CODE"] == "LOCATION"){
                        $arr = explode(",", $prop["VALUE"]);
                        $last = $arr[count($arr) - 1];
                        $addres[1] = Loc::getMessage('SPOD_ADDRES_CITY', array("#CITY#" => $last));
                    }
                    if($prop["CODE"] == "STREET") {
                        $addres[2] = Loc::getMessage('SPOD_ADDRES_STREET', array("#STREET#" => $prop["VALUE"]));
                    }
                    if($prop["CODE"] == "CITY_ZIP_END") {
                        $addres[0] = $prop["VALUE"].", ";
                    }
                    if($prop["CODE"] == "CITY_HOUSE_BEGIN") {
                        $addres[3] = ", ".$prop["VALUE"];
                    }
                    if($prop["CODE"] == "CITY_FLAT") {
                        $addres[4] = Loc::getMessage('SPOD_ADDRES_FLAT', array("#FLAT#" => $prop["VALUE"]));
                    }
                }
                ksort($addres);
                ?>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="modal-adress-table">
                                <li><strong><?= Loc::getMessage('SPOD_ORDER_GETTER') ?></strong><span><?=$arResult["FIO"]?></span></li>
                                <li><strong><?= Loc::getMessage('SPOD_ORDER_ADDRESS') ?></strong>
                                    <span><?=implode($addres)?></span>
                                </li>
                                <li><strong><?= Loc::getMessage('SPOD_ORDER_DELIVERY') ?></strong><span><?=$arResult["DELIVERY"]["NAME"]?></span></li>
                                <?php
                                foreach ($arResult["PAYMENT"] as $item){
                                    ?>
                                    <li><strong><?= Loc::getMessage('SPOD_ORDER_PAY') ?></strong><span><?=$item["PAY_SYSTEM_NAME"]?></span></li>
                                    <?
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="modal-summ-table">
                                <li><?= Loc::getMessage('SPOD_ORDER_SUM') ?><span><?=$arResult["PRODUCT_SUM_FORMATED"]?></span></li>
                                <li><?= Loc::getMessage('SPOD_ORDER_DISCOUNT') ?><b>(<?=$APPLICATION->ShowViewContent("globalDiscount");?>)</b>: <span><b>-<?=CurrencyFormat($disPrise,"RUB")?></b></span></li>
                                <li><?= Loc::getMessage('SPOD_ORDER_DELIVERY_SUM') ?><span><?=$arResult["PRICE_DELIVERY_FORMATED"]?></span></li>
                                <li><?= Loc::getMessage('SPOD_ORDER_ALL_SUM') ?><span><?=$arResult["PRICE_FORMATED"]?></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?
}

?>