<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$result = array();
foreach ($arResult["ITEMS"] as $arItem){
    switch ($arItem["DISPLAY_PROPERTIES"]["POSITION"]["VALUE_XML_ID"]){
        case "top-left";
            $result[0][] = $arItem;
        break;
        case "top-right";
            $result[1][] = $arItem;
        break;
        case "bottom-left";
            $result[2][] = $arItem;
        break;
        case "bottom-right";
            $result[3][] = $arItem;
        break;
    }
}
$arResult["ITEMS"] = $result;
?>