<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="row mb12">
    <div class="col-lg-7 col-md-12 col-sm-12">
        <ul class="slider1">
            <?
            foreach ($arResult["ITEMS"][0] as $arItem) {
                ?>
                <li>
                    <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">
                        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"/>
                    </a>
                </li>
                <?
            }
            ?>
        </ul>
    </div>
    <div class="col-lg-5 col-sm-6">
        <ul class="slider2">
            <?
            foreach ($arResult["ITEMS"][1] as $arItem) {
                ?>
                <li>
                    <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">
                        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"/>
                    </a>
                </li>
                <?
            }
            ?>
        </ul>
    </div>
    <div class="col-lg-5 col-md-6  hidden-xs col-sm-6">
        <div class="banners_small">
            <?
            foreach ($arResult["ITEMS"][2] as $arItem) {
                ?>
                <div class="banner_small mb12">
                    <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>" style="background-image: url('<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>')"></a>
                </div>
                <?
            }
            ?>
        </div>
    </div>
    <div class="col-lg-7 col-md-12 col-sm-12">
        <ul class="slider3">
            <?
            foreach ($arResult["ITEMS"][3] as $arItem) {
                ?>
                <li>
                    <a href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">
                        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"/>
                    </a>
                </li>
                <?
            }
            ?>
        </ul>
    </div>
</div>
