<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="row section_actual">
    <div class="col-lg-12">
        <h3><?= $arParams["BLOCK_NAME"] ?></h3>
    </div>

    <?
    $this->SetViewTarget("cat_section");
    foreach ($arResult["ITEMS"] as $arItem) {
        $curr = "";
        foreach ($arItem["DISPLAY_PROPERTIES"]["LINK_DECTION"]["LINK_SECTION_VALUE"] as $lastSection) {

            $curr = $lastSection["SECTION_PAGE_URL"];
        }
        ?>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="front_cat_item">
                <a href="<?= $curr ?>">
                    <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="" class="w100">
                </a>
                <div class="front_cat_item_title"><a href="<?= $curr ?>"><?= $arItem["NAME"] ?></a></div>
                <div class="front_cat_item_subtitle"><?= $arItem["PREVIEW_TEXT"] ?></div>
            </div>
        </div>
        <?
    }
    $this->EndViewTarget("cat_section");
    ?>
    <div class="actual_unslick">
        <?
        $APPLICATION->ShowViewContent("cat_section");
        ?>
    </div>
    <div class="actual_unslick_mobile hidden-lg">
        <?
        $APPLICATION->ShowViewContent("cat_section");
        ?>
    </div>

</div>



