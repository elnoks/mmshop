<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */

/** @var array $arHeaders */

use Bitrix\Main\Localization\Loc;

if (!empty($arResult["ERROR_MESSAGE"]))
    ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0){
    foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {
        $arHeaders[] = $arHeader["id"];
    }
    ?>
    <div class="col-lg-12 mp0">
        <table class="cart_table" id="basket_items">
            <thead>
            <tr>
                <th class="col-tac"></th>
                <th class="product_col col-large"><?= Loc::getMessage("SALE_ORDER_H_NAME") ?></th>
                <th class="col-tac"><?= Loc::getMessage("SALE_ORDER_H_SIZE") ?></th>
                <th class="col-tac"><?= Loc::getMessage("SALE_ORDER_H_COLOR") ?></th>
                <th class="col-tac"><?= Loc::getMessage("SALE_ORDER_H_QUANTITY") ?></th>
                <th class="col-tac"><?= Loc::getMessage("SALE_ORDER_H_PRICE") ?></th>
                <th class="col-mini col-action"></th>
            </tr>
            </thead>
            <tbody>
            <?
            foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
                if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
//                    op($arItem);
                    ?>
                    <tr id="<?= $arItem["ID"] ?>"
                        data-item-name="<?= $arItem["NAME"] ?>"
                        data-item-price="<?= $arItem["PRICE"] ?>"
                        data-item-currency="<?= $arItem["CURRENCY"] ?>"
                    >
                        <td class="col-tac">
                            <?
                            if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
                                $url = $arItem["PREVIEW_PICTURE_SRC"];
                            elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
                                $url = $arItem["DETAIL_PICTURE_SRC"];
                            else:
                                $url = $templateFolder . "/images/no_photo.png";
                            endif;
                            ?>
                            <a href="<?= strlen($arItem["DETAIL_PAGE_URL"]) > 0 ? $arItem["DETAIL_PAGE_URL"] : "javascript:void(0)" ?>">
                                <img src="<?= $url ?>">
                            </a>
                        </td>

                        <td>
                            <div class="cart_title"><?= getFromHighloadbyXmlId(1, $arItem["PROPERTY_BREND_VALUE"]) ?></div>
                            <div class="cart_category"><?= $arItem["PROPERTY_TIP_MODELI_VALUE"] ?></div>
                            <div class="cart_category">
                                <?= $arItem["NAME"] ?>
                            </div>
                            <div class="cart_info_mobile">
                                <?php
                                echo Loc::getMessage("SALE_ORDER_SIZE", array("#SIZE#" => getFromHighloadbyXmlId(SIZE_IB, $arItem["PROPERTY_RAZMERY_VALUE"])));
                                echo Loc::getMessage("SALE_ORDER_COLOR", array("#COLOR#" => getFromHighloadbyXmlId(COLOR_IB, $arItem["PROPERTY_TSVET_VALUE"])));
                                ?>
                            </div>
                        </td>

                        <td class="col-tac">
                            <?= getFromHighloadbyXmlId(SIZE_IB, $arItem["PROPERTY_RAZMERY_VALUE"]) ?>
                        </td>

                        <td class="col-tac">
                            <?= getFromHighloadbyXmlId(COLOR_IB, $arItem["PROPERTY_TSVET_VALUE"]) ?>
                        </td>

                        <td class="col-tac">
                            <div class="quantity_cart">
                                <?
                                $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                                $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                                ?>
                                <input
                                        type="text"
                                        size="3"
                                        id="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                        name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                        maxlength="18"
                                        value="<?= $arItem["QUANTITY"] ?>"
                                        onchange="updateQuantity('QUANTITY_INPUT_<?= $arItem["ID"] ?>', '<?= $arItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
                                >
                                <?
                                if (!isset($arItem["MEASURE_RATIO"])) {
                                    $arItem["MEASURE_RATIO"] = 1;
                                }
                                if (
                                    floatval($arItem["MEASURE_RATIO"]) != 0
                                ):
                                    ?>
                                    <a href="javascript:void(0);" class="quantity-more btn-grey"
                                       onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'up', <?= $useFloatQuantityJS ?>);">+</a>
                                    <a href="javascript:void(0);" class="quantity-less btn-grey"
                                       onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'down', <?= $useFloatQuantityJS ?>);">-</a>
                                <?
                                endif;
                                ?>
                            </div>
                            <input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>"
                                   name="QUANTITY_<?= $arItem['ID'] ?>" value="<?= $arItem["QUANTITY"] ?>"/>
                        </td>

                        <td class="col-tac col-price">
                            <div class="old-price" id="old_price_<?= $arItem["ID"] ?>">
                                <?
                                if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
                                    <?= $arItem["FULL_PRICE_FORMATED"] ?>
                                <? endif; ?>
                            </div>
                            <div class="item_price" id="current_price_<?= $arItem["ID"] ?>">
                                <?= $arItem["PRICE_FORMATED"] ?>
                            </div>
                        </td>

                        <td>
                            <div class="cart_remove">
                                <a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>">
                                    <i class="flaticon-delete"></i>
                                </a>
                            </div>
                        </td>

                        <td class="cart_mobile_td quantity_cart">
                            <div class="cart_remove">
                                <a href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>">
                                    <i class="flaticon-delete"></i>
                                </a>
                            </div>
                            <div class="col-price">
                                <div class="old-price" id="mob_old_price_<?= $arItem["ID"] ?>">
                                    <?
                                    if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
                                        <?= $arItem["FULL_PRICE_FORMATED"] ?>
                                    <? endif; ?>
                                </div>
                                <div class="item_price"
                                     id="mob_current_price_<?= $arItem["ID"] ?>"><?= $arItem["PRICE_FORMATED"] ?></div>
                            </div>
                            <div class="m_quantity">
                                <input
                                        type="text"
                                        size="3"
                                        id="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                        name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                        maxlength="18"
                                        value="<?= $arItem["QUANTITY"] ?>"
                                        onchange="updateQuantity('QUANTITY_INPUT_<?= $arItem["ID"] ?>', '<?= $arItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
                                >
                                <a href="javascript:void(0);" class="quantity-more btn-grey"
                                   onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'up', <?= $useFloatQuantityJS ?>);">+</a>
                                <a href="javascript:void(0);" class="quantity-less btn-grey"
                                   onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'down', <?= $useFloatQuantityJS ?>);">-</a>
                            </div>
                        </td>
                    </tr>
                <?
                endif;
                if ($arHeader["id"] == "PROPERTY_BREND_VALUE" || $arHeader["id"] == "PROPERTY_TIP_MODELI_VALUE") continue;
            endforeach;
            ?>
            </tbody>
        </table>
    </div>

    <input type="hidden" id="column_headers" value="<?= htmlspecialcharsbx(implode($arHeaders, ",")) ?>"/>
    <input type="hidden" id="offers_props" value="<?= htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
    <input type="hidden" id="action_var" value="<?= htmlspecialcharsbx($arParams["ACTION_VARIABLE"]) ?>"/>
    <input type="hidden" id="quantity_float" value="<?= ($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="price_vat_show_value"
           value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="auto_calculation" value="<?= ($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y" ?>"/>

    <div class="cart_meta">
        <div class="col-lg-4">
            <div class="promo_block">
                <div class="promo_block_title"><?= Loc::getMessage("STB_COUPON_PROMT") ?></div>
                <input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();"
                       placeholder="<?= Loc::getMessage("SALE_COUPON_PLACEHOLDER") ?>">
                <a href="javascript:void(0)" onclick="enterCoupon();"
                   title="<?= Loc::getMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?= Loc::getMessage('SALE_COUPON_APPLY'); ?>
                </a>
            </div>
        </div>
        <div class="col-lg-5 col-lg-push-3 cart-total">
            <table>
                <tr>
                    <td><?= Loc::getMessage("TSB1_SUMM") ?></td>
                    <td id="PRICE_WITHOUT_DISCOUNT">
                        <span class="sum"><?= ($showTotalPrice ? $arResult["PRICE_WITHOUT_DISCOUNT"] : ''); ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= Loc::getMessage("TSB1_DISCOUNT", array("#DISCOUNT#" => getDiscountByUser())) ?>
                    </td>
                    <td>
                        <span class="sale_value" id="allDiscountValue">-
                            <?php
//                            op($arResult);
//                            op($arResult['PRICE_WITHOUT_DISCOUNT']);
//                            op($arResult['allSum']);
//                            echo $arResult['PRICE_WITHOUT_DISCOUNT'] - $arResult['allSum'];
                            ?>
                        </span>
                    </td>
                </tr>
                <tr class='row_bold'>
                    <td><?= Loc::getMessage("SALE_TOTAL") ?></td>
                    <td id="allSum_FORMATED"><span class="result_sum"><?= str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"]) ?></span></td>
                </tr>
            </table>
            <div class="row">
                <div class="btns_row">
                    <div class="col-lg-5 col-sm-3">
                        <a class="lk_grey_btn" onclick="javascript:history.back(); return false;" href="javascript:void(0)"><?= Loc::getMessage("TSB1_CART_BACK") ?></a>
                    </div>
                    <div class="col-lg-7 col-lg-push-0 col-sm-push-3 col-sm-6">
                        <a href="/personal/order/make/" class="lk_orange_btn"><?= Loc::getMessage("SALE_ORDER") ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?
    /*
		<div id="coupons_block">
		<?
        if ($arParams["HIDE_COUPON"] != "Y") {
            ?>
            <div class="bx_ordercart_coupon">

            </div>
            <?
            if (!empty($arResult['COUPON_LIST'])) {
                foreach ($arResult['COUPON_LIST'] as $oneCoupon) {
                    $couponClass = 'disabled';
                    switch ($oneCoupon['STATUS']) {
                        case DiscountCouponsManager::STATUS_NOT_FOUND:
                        case DiscountCouponsManager::STATUS_FREEZE:
                            $couponClass = 'bad';
                            break;
                        case DiscountCouponsManager::STATUS_APPLYED:
                            $couponClass = 'good';
                            break;
                    }
                    ?>
                    <div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]"
                                                            value="<?= htmlspecialcharsbx($oneCoupon['COUPON']); ?>"
                                                            class="<? echo $couponClass; ?>"><span
                            class="<? echo $couponClass; ?>"
                            data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div
                            class="bx_ordercart_coupon_notes"><?
                        if (isset($oneCoupon['CHECK_CODE_TEXT'])) {
                            echo(is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
                        }
                        ?></div></div><?
                }
                unset($couponClass, $oneCoupon);
            }
        } else {
            ?>&nbsp;<?
        }
        ?>
		</div>
*/?>

    <script>
        BX.ready(function(){
            updateBasket();
        });
    </script>
<?
}else{
    ?>
    <div id="basket_items_list">

    </div>
    <?
}