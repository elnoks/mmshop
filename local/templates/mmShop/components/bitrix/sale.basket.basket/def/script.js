BasketPoolQuantity = function()
{
	this.processing = false;
	this.poolQuantity = {};
	this.updateTimer = null;
	this.currentQuantity = {};
	this.lastStableQuantities = {};

	this.updateQuantity();
};

BasketPoolQuantity.prototype.updateQuantity = function()
{
	var items = BX('basket_items');

	if (basketJSParams['USE_ENHANCED_ECOMMERCE'] === 'Y')
	{
		checkAnalytics(this.lastStableQuantities, items);
	}

	if (!!items && items.rows.length > 0)
	{
		for (var i = 1; items.rows.length > i; i++)
		{
			var itemId = items.rows[i].id;
			this.currentQuantity[itemId] = BX('QUANTITY_' + itemId).value;
		}
	}

	this.lastStableQuantities = BX.clone(this.currentQuantity, true);
};

BasketPoolQuantity.prototype.changeQuantity = function(itemId)
{
	var quantity = BX('QUANTITY_' + itemId).value;
	var isPoolEmpty = this.isPoolEmpty();

	if (this.currentQuantity[itemId] && this.currentQuantity[itemId] != quantity)
	{
		this.poolQuantity[itemId] = this.currentQuantity[itemId] = quantity;
	}

	if (!isPoolEmpty)
	{
		this.enableTimer(true);
	}
	else
	{
		this.trySendPool();
	}
};

BasketPoolQuantity.prototype.trySendPool = function()
{
	if (!this.isPoolEmpty() && !this.isProcessing())
	{
		this.enableTimer(false);
		recalcBasketAjax({});
	}
};

BasketPoolQuantity.prototype.isPoolEmpty = function()
{
	return ( Object.keys(this.poolQuantity).length == 0 );
};

BasketPoolQuantity.prototype.clearPool = function()
{
	this.poolQuantity = {};
};

BasketPoolQuantity.prototype.isProcessing = function()
{
	return (this.processing === true);
};

BasketPoolQuantity.prototype.setProcessing = function(value)
{
	this.processing = (value === true);
};

BasketPoolQuantity.prototype.enableTimer = function(value)
{
	clearTimeout(this.updateTimer);
	if (value === false)
		return;

	this.updateTimer = setTimeout(function(){ basketPoolQuantity.trySendPool(); }, 1500);
};

/**
 * @param basketItemId
 * @param {{BASKET_ID : string, BASKET_DATA : { GRID : { ROWS : {} }}, COLUMNS: {}, PARAMS: {}, DELETE_ORIGINAL : string }} res
 */
function updateBasketTable(basketItemId, res)
{
	var table = BX("basket_items"),
		rows,
		newBasketItemId,
		arItem,
		lastRow,
		newRow,
		arColumns,
		bShowDeleteColumn = false,
		bShowDelayColumn = false,
		bShowPropsColumn = false,
		bShowPriceType = false,
		bUseFloatQuantity,
		origBasketItem,
		i,
		oCellName,
		imageURL,
		cellNameHTML,
		oCellItem,
		cellItemHTML,
		j,
		val,
		oCellQuantity,
		oCellQuantityHTML,
		ratio,
		isUpdateQuantity,
		oldQuantity,
		oCellPrice,
		fullPrice,
		id,
		oCellDiscount,
		oCellCustom,
		customColumnVal,
		createNewItem;

	if (!table || typeof res !== 'object')
	{
		return;
	}

	rows = table.rows;
	lastRow = rows[rows.length - 1];
	bUseFloatQuantity = (res.PARAMS.QUANTITY_FLOAT === 'Y');

	// insert new row instead of original basket item row
	if (basketItemId !== null && !!res.BASKET_DATA)
	{
		origBasketItem = BX(basketItemId);

		newBasketItemId = res.BASKET_ID;
		createNewItem = BX.type.isPlainObject(res.BASKET_DATA.GRID.ROWS[newBasketItemId]);
		if (createNewItem)
		{
			arItem = res.BASKET_DATA.GRID.ROWS[newBasketItemId];
			newRow = document.createElement('tr');

			newRow.setAttribute('id', res.BASKET_ID);
			newRow.setAttribute('data-item-name', arItem['NAME']);
			newRow.setAttribute('data-item-brand', arItem[basketJSParams['BRAND_PROPERTY'] + '_VALUE']);
			newRow.setAttribute('data-item-price', arItem['PRICE']);
			newRow.setAttribute('data-item-currency', arItem['CURRENCY']);

			lastRow.parentNode.insertBefore(newRow, origBasketItem.nextSibling);
		}

		if (res.DELETE_ORIGINAL === 'Y')
		{
			origBasketItem.parentNode.removeChild(origBasketItem);
		}

		if (createNewItem)
		{
			// fill row with fields' values
			arColumns = res.COLUMNS.split(',');

			for (i = 0; i < arColumns.length; i++)
			{
				if (arColumns[i] === 'DELETE')
				{
					bShowDeleteColumn = true;
				}
				else if (arColumns[i] === 'DELAY')
				{
					bShowDelayColumn = true;
				}
				else if (arColumns[i] === 'PROPS')
				{
					bShowPropsColumn = true;
				}
				else if (arColumns[i] === 'TYPE')
				{
					bShowPriceType = true;
				}
			}



			if (bShowDeleteColumn || bShowDelayColumn)
			{
				var oCellControl = newRow.insertCell(-1);
				oCellControl.setAttribute('class', 'control');

				if (bShowDeleteColumn)
					oCellControl.innerHTML = '<a href="' + basketJSParams['DELETE_URL'].replace('#ID#', arItem['ID']) + '">' + basketJSParams['SALE_DELETE'] + '</a><br />';
			}
		}
	}

	// update product params after recalculation
	if (!!res.BASKET_DATA)
	{
		for (id in res.BASKET_DATA.GRID.ROWS)
		{

			if (res.BASKET_DATA.GRID.ROWS.hasOwnProperty(id))
			{

				var item = res.BASKET_DATA.GRID.ROWS[id];

				if (BX('discount_value_' + id))
					BX('discount_value_' + id).innerHTML = item.DISCOUNT_PRICE_PERCENT_FORMATED;

                if (BX('current_price_' + id)) {
                    BX('current_price_' + id).innerHTML = item.PRICE_FORMATED;
                    BX('mob_current_price_' + id).innerHTML = item.PRICE_FORMATED;
                }


				if (BX('old_price_' + id)){
                    BX('old_price_' + id).innerHTML = (item.DISCOUNT_PRICE_PERCENT > 0) ? item.FULL_PRICE_FORMATED : '';
                    BX('mob_old_price_' + id).innerHTML = (item.DISCOUNT_PRICE_PERCENT > 0) ? item.FULL_PRICE_FORMATED : '';
				}

				if (BX('sum_' + id))
					BX('sum_' + id).innerHTML = item.SUM;

				// if the quantity was set by user to 0 or was too much, we need to show corrected quantity value from ajax response
				if (BX('QUANTITY_' + id))
				{
					BX('QUANTITY_INPUT_' + id).value = item.QUANTITY;
					BX('QUANTITY_INPUT_' + id).defaultValue = item.QUANTITY;

					BX('QUANTITY_' + id).value = item.QUANTITY;
				}
			}
		}
	}

	// update coupon info
	if (!!res.BASKET_DATA)
		couponListUpdate(res.BASKET_DATA);

	// update warnings if any
    /*
	if (res.hasOwnProperty('WARNING_MESSAGE'))
	{
		var warningText = '';

		for (i = res['WARNING_MESSAGE'].length - 1; i >= 0; i--)
			warningText += res['WARNING_MESSAGE'][i] + '<br/>';

		BX('warning_message').innerHTML = warningText;
	}
    */
	// update total basket values
	if (!!res.BASKET_DATA)
	{
		if (BX('allWeight_FORMATED'))
			BX('allWeight_FORMATED').innerHTML = res['BASKET_DATA']['allWeight_FORMATED'].replace(/\s/g, '&nbsp;');

		if (BX('allSum_wVAT_FORMATED'))
			BX('allSum_wVAT_FORMATED').innerHTML = res['BASKET_DATA']['allSum_wVAT_FORMATED'].replace(/\s/g, '&nbsp;');

		if (BX('allDiscountValue'))
			BX('allDiscountValue').innerHTML = res['BASKET_DATA']['DISCOUNT_PRICE_FORMATED'].replace(/\s/g, '&nbsp;');

		if (BX('allVATSum_FORMATED'))
			BX('allVATSum_FORMATED').innerHTML = res['BASKET_DATA']['allVATSum_FORMATED'].replace(/\s/g, '&nbsp;');

		if (BX('allSum_FORMATED'))
			BX('allSum_FORMATED').innerHTML = res['BASKET_DATA']['allSum_FORMATED'].replace(/\s/g, '&nbsp;');

		if (BX('PRICE_WITHOUT_DISCOUNT'))
		{
			var showPriceWithoutDiscount = (res['BASKET_DATA']['PRICE_WITHOUT_DISCOUNT'] != res['BASKET_DATA']['allSum_FORMATED']);
			BX('PRICE_WITHOUT_DISCOUNT').innerHTML = showPriceWithoutDiscount ? res['BASKET_DATA']['PRICE_WITHOUT_DISCOUNT'].replace(/\s/g, '&nbsp;') : '';
			BX.style(BX('PRICE_WITHOUT_DISCOUNT').parentNode, 'display', (showPriceWithoutDiscount ? 'table-row' : 'none'));
		}

		BX.onCustomEvent('OnBasketChange');
	}
}
/**
 * @param couponBlock
 * @param {COUPON: string, JS_STATUS: string} oneCoupon - new coupon.
 */
function couponCreate(couponBlock, oneCoupon)
{
	var couponClass = 'disabled';

	if (!BX.type.isElementNode(couponBlock))
		return;
	if (oneCoupon.JS_STATUS === 'BAD')
		couponClass = 'bad';
	else if (oneCoupon.JS_STATUS === 'APPLYED')
		couponClass = 'good';

	couponBlock.appendChild(BX.create(
		'div',
		{
			props: {
				className: 'bx_ordercart_coupon'
			},
			children: [
				BX.create(
					'input',
					{
						props: {
							className: couponClass,
							type: 'text',
							value: oneCoupon.COUPON,
							name: 'OLD_COUPON[]'
						},
						attrs: {
							disabled: true,
							readonly: true
						}
					}
				),
				BX.create(
					'span',
					{
						props: {
							className: couponClass
						},
						attrs: {
							'data-coupon': oneCoupon.COUPON
						}
					}
				),
				BX.create(
					'div',
					{
						props: {
							className: 'bx_ordercart_coupon_notes'
						},
						html: oneCoupon.JS_CHECK_CODE
					}
				)
			]
		}
	));
}

/**
 * @param {COUPON_LIST : []} res
 */
function couponListUpdate(res)
{
	var couponBlock,
		couponClass,
		fieldCoupon,
		couponsCollection,
		couponFound,
		i,
		j,
		key;

	if (!!res && typeof res !== 'object')
	{
		return;
	}

	couponBlock = BX('coupons_block');
	if (!!couponBlock)
	{
		if (!!res.COUPON_LIST && BX.type.isArray(res.COUPON_LIST))
		{
			fieldCoupon = BX('coupon');
			if (!!fieldCoupon)
			{
				fieldCoupon.value = '';
			}
			couponsCollection = BX.findChildren(couponBlock, { tagName: 'input', property: { name: 'OLD_COUPON[]' } }, true);

			if (!!couponsCollection)
			{
				if (BX.type.isElementNode(couponsCollection))
				{
					couponsCollection = [couponsCollection];
				}
				for (i = 0; i < res.COUPON_LIST.length; i++)
				{
					couponFound = false;
					key = -1;
					for (j = 0; j < couponsCollection.length; j++)
					{
						if (couponsCollection[j].value === res.COUPON_LIST[i].COUPON)
						{
							couponFound = true;
							key = j;
							couponsCollection[j].couponUpdate = true;
							break;
						}
					}
					if (couponFound)
					{
						couponClass = 'disabled';
						if (res.COUPON_LIST[i].JS_STATUS === 'BAD')
							couponClass = 'bad';
						else if (res.COUPON_LIST[i].JS_STATUS === 'APPLYED')
							couponClass = 'good';

						BX.adjust(couponsCollection[key], {props: {className: couponClass}});
						BX.adjust(couponsCollection[key].nextSibling, {props: {className: couponClass}});
						BX.adjust(couponsCollection[key].nextSibling.nextSibling, {html: res.COUPON_LIST[i].JS_CHECK_CODE});
					}
					else
					{
						couponCreate(couponBlock, res.COUPON_LIST[i]);
					}
				}
				for (j = 0; j < couponsCollection.length; j++)
				{
					if (typeof (couponsCollection[j].couponUpdate) === 'undefined' || !couponsCollection[j].couponUpdate)
					{
						BX.remove(couponsCollection[j].parentNode);
						couponsCollection[j] = null;
					}
					else
					{
						couponsCollection[j].couponUpdate = null;
					}
				}
			}
			else
			{
				for (i = 0; i < res.COUPON_LIST.length; i++)
				{
					couponCreate(couponBlock, res.COUPON_LIST[i]);
				}
			}
		}
	}
	couponBlock = null;
}

function getColumnName(result, columnCode)
{
	if (BX('col_' + columnCode))
	{
		return BX.util.trim(BX('col_' + columnCode).innerHTML);
	}
	else
	{
		return '';
	}
}

function leftScroll(prop, id, count)
{
	count = parseInt(count, 10);
	var el = BX('prop_' + prop + '_' + id);

	if (el)
	{
		var curVal = parseInt(el.style.marginLeft, 10);
		if (curVal <= -20)
			el.style.marginLeft = curVal + 20 + '%';
	}
}

function rightScroll(prop, id, count)
{
	count = parseInt(count, 10);
	var el = BX('prop_' + prop + '_' + id);

	if (el)
	{
		var curVal = parseInt(el.style.marginLeft, 10);
		if (curVal > (5 - count)*20)
			el.style.marginLeft = curVal - 20 + '%';
	}
}
/*
function checkOut()
{
	if (!!BX('coupon'))
		BX('coupon').disabled = true;
	BX("basket_form").submit();
	return true;
}
*/
function updateBasket()
{
	recalcBasketAjax({});
}

function enterCoupon()
{
	var newCoupon = BX('coupon');
	if (!!newCoupon && !!newCoupon.value)
		recalcBasketAjax({'coupon' : newCoupon.value});
}

// check if quantity is valid
// and update values of both controls (text input field for PC and mobile quantity select) simultaneously
function updateQuantity(controlId, basketId, ratio, bUseFloatQuantity)
{
	var oldVal = BX(controlId).defaultValue,
		newVal = parseFloat(BX(controlId).value) || 0,
		bIsCorrectQuantityForRatio = false,
		autoCalculate = ((BX("auto_calculation") && BX("auto_calculation").value == "Y") || !BX("auto_calculation"));

	if (ratio === 0 || ratio == 1)
	{
		bIsCorrectQuantityForRatio = true;
	}
	else
	{

		var newValInt = newVal * 10000,
			ratioInt = ratio * 10000,
			reminder = newValInt % ratioInt,
			newValRound = parseInt(newVal);

		if (reminder === 0)
		{
			bIsCorrectQuantityForRatio = true;
		}
	}

	var bIsQuantityFloat = false;

	if (parseInt(newVal) != parseFloat(newVal))
	{
		bIsQuantityFloat = true;
	}

	newVal = (bUseFloatQuantity === false && bIsQuantityFloat === false) ? parseInt(newVal) : parseFloat(newVal).toFixed(4);
	newVal = correctQuantity(newVal);

	if (bIsCorrectQuantityForRatio)
	{
		BX(controlId).defaultValue = newVal;

		BX("QUANTITY_INPUT_" + basketId).value = newVal;

		// set hidden real quantity value (will be used in actual calculation)
		BX("QUANTITY_" + basketId).value = newVal;

		if (autoCalculate)
		{
			basketPoolQuantity.changeQuantity(basketId);
		}
	}
	else
	{
		newVal = getCorrectRatioQuantity(newVal, ratio, bUseFloatQuantity);
		newVal = correctQuantity(newVal);

		if (newVal != oldVal)
		{
			BX("QUANTITY_INPUT_" + basketId).value = newVal;
			BX("QUANTITY_" + basketId).value = newVal;

			if (autoCalculate)
			{
				basketPoolQuantity.changeQuantity(basketId);
			}
		}else
		{
			BX(controlId).value = oldVal;
		}
	}
}

// used when quantity is changed by clicking on arrows
function setQuantity(basketId, ratio, sign, bUseFloatQuantity)
{
	var curVal = parseFloat(BX("QUANTITY_INPUT_" + basketId).value),
		newVal;

	newVal = (sign == 'up') ? curVal + ratio : curVal - ratio;

	if (newVal < 0)
		newVal = 0;

	if (bUseFloatQuantity)
	{
		newVal = parseFloat(newVal).toFixed(4);
	}
	newVal = correctQuantity(newVal);

	if (ratio > 0 && newVal < ratio)
	{
		newVal = ratio;
	}

	if (!bUseFloatQuantity && newVal != newVal.toFixed(4))
	{
		newVal = parseFloat(newVal).toFixed(4);
	}

	newVal = getCorrectRatioQuantity(newVal, ratio, bUseFloatQuantity);
	newVal = correctQuantity(newVal);

	BX("QUANTITY_INPUT_" + basketId).value = newVal;
	BX("QUANTITY_INPUT_" + basketId).defaultValue = newVal;

	updateQuantity('QUANTITY_INPUT_' + basketId, basketId, ratio, bUseFloatQuantity);
}

function getCorrectRatioQuantity(quantity, ratio, bUseFloatQuantity)
{
	var newValInt = quantity * 10000,
		ratioInt = ratio * 10000,
		reminder = (quantity / ratio - ((quantity / ratio).toFixed(0))).toFixed(6),
		result = quantity,
		bIsQuantityFloat = false,
		i;
	ratio = parseFloat(ratio);

	if (reminder == 0)
	{
		return result;
	}

	if (ratio !== 0 && ratio != 1)
	{
		for (i = ratio, max = parseFloat(quantity) + parseFloat(ratio); i <= max; i = parseFloat(parseFloat(i) + parseFloat(ratio)).toFixed(4))
		{
			result = i;
		}

	}else if (ratio === 1)
	{
		result = quantity | 0;
	}

	if (parseInt(result, 10) != parseFloat(result))
	{
		bIsQuantityFloat = true;
	}

	result = (bUseFloatQuantity === false && bIsQuantityFloat === false) ? parseInt(result, 10) : parseFloat(result).toFixed(4);
	result = correctQuantity(result);
	return result;
}

function correctQuantity(quantity)
{
	return parseFloat((quantity * 1).toString());
}


/**
 *
 * @param {} params
 */
function recalcBasketAjax(params)
{
	if (basketPoolQuantity.isProcessing())
	{
		return false;
	}

	BX.showWait();

	var property_values = {},
		action_var = BX('action_var').value,
		items = BX('basket_items'),
		delayedItems = BX('delayed_items'),
		postData,
		i;

	postData = {
		'sessid': BX.bitrix_sessid(),
		'site_id': BX.message('SITE_ID'),
		'props': property_values,
		'action_var': action_var,
		'select_props': BX('column_headers').value,
		'offers_props': BX('offers_props').value,
		'quantity_float': BX('quantity_float').value,
		'price_vat_show_value': BX('price_vat_show_value').value,
		'hide_coupon': BX('hide_coupon').value,
		'use_prepayment': BX('use_prepayment').value
	};
	postData[action_var] = 'recalculate';
	if (!!params && typeof params === 'object')
	{
		for (i in params)
		{
			if (params.hasOwnProperty(i))
				postData[i] = params[i];
		}
	}

	if (!!items && items.rows.length > 0)
	{
		for (i = 1; items.rows.length > i; i++)
			postData['QUANTITY_' + items.rows[i].id] = BX('QUANTITY_' + items.rows[i].id).value;
	}

	if (!!delayedItems && delayedItems.rows.length > 0)
	{
		for (i = 1; delayedItems.rows.length > i; i++)
			postData['DELAY_' + delayedItems.rows[i].id] = 'Y';
	}

	basketPoolQuantity.setProcessing(true);
	basketPoolQuantity.clearPool();

	BX.ajax({
		url: '/bitrix/components/bitrix/sale.basket.basket/ajax.php',
		method: 'POST',
		data: postData,
		dataType: 'json',
		onsuccess: function(result)
		{
			BX.closeWait();
			basketPoolQuantity.setProcessing(false);

			if(params.coupon)
			{
				//hello, gifts!
				if(!!result && !!result.BASKET_DATA && !!result.BASKET_DATA.NEED_TO_RELOAD_FOR_GETTING_GIFTS)
				{
					BX.reload();
				}
			}

			if (basketPoolQuantity.isPoolEmpty())
			{
				updateBasketTable(null, result);
				basketPoolQuantity.updateQuantity();
			}
			else
			{
				basketPoolQuantity.enableTimer(true);
			}
		}
	});
}

function deleteCoupon()
{
	var target = this,
		value;

	if (BX.type.isElementNode(target) && target.hasAttribute('data-coupon'))
	{
		value = target.getAttribute('data-coupon');
		if (BX.type.isNotEmptyString(value))
		{
			recalcBasketAjax({'delete_coupon' : value});
		}
	}
}

function deleteProductRow(target)
{
    BX.onCustomEvent('OnBasketChange');
	var targetRow = BX.findParent(target, {tagName: 'TR'}),
		quantityNode,
		delItem;

	if (targetRow)
	{
		quantityNode = BX('QUANTITY_' + targetRow.id);
		if (quantityNode)
		{
			delItem = getCurrentItemAnalyticsInfo(targetRow, quantityNode.value);
		}
	}

	setAnalyticsDataLayer([], [delItem]);

	document.location.href = target.href;

	return false;
}

function checkAnalytics(currentQuantity, newItems)
{
	if (!currentQuantity || !newItems || BX.util.array_values(currentQuantity).length === 0)
		return;

	var itemId, diff,
		current = {}, addItems = [], delItems = [],
		i;

	if (!!newItems && newItems.rows.length)
	{
		for (i = 1; newItems.rows.length > i; i++)
		{
			itemId = newItems.rows[i].id;
			diff = BX('QUANTITY_' + itemId).value - currentQuantity[itemId];

			if (diff != 0)
			{
				current = getCurrentItemAnalyticsInfo(newItems.rows[i], diff);

				if (diff > 0)
				{
					addItems.push(current);
				}
				else
				{
					delItems.push(current);
				}
			}
		}
	}

	if (addItems.length || delItems.length)
	{
		setAnalyticsDataLayer(addItems, delItems);
	}
}

function getCurrentItemAnalyticsInfo(row, diff)
{
	if (!row)
		return;

	var temp, k, variants = [];

	var current = {
		'name': row.getAttribute('data-item-name') || '',
		'id': row.id,
		'price': row.getAttribute('data-item-price') || 0,
		'brand': (row.getAttribute('data-item-brand') || '').split(',  ').join('/'),
		'variant': '',
		'quantity': Math.abs(diff)
	};

	temp = row.querySelectorAll('.bx_active[data-sku-name]');
	for (k = 0; k < temp.length; k++)
	{
		variants.push(temp[k].getAttribute('data-sku-name'));
	}

	current.variant = variants.join('/');

	return current;
}

function setAnalyticsDataLayer(addItems, delItems)
{
	window[basketJSParams['DATA_LAYER_NAME']] = window[basketJSParams['DATA_LAYER_NAME']] || [];

	if (addItems && addItems.length)
	{
		window[basketJSParams['DATA_LAYER_NAME']].push({
			'event': 'addToCart',
			'ecommerce': {
				'currencyCode': getCurrencyCode(),
				'add': {
					'products': addItems
				}
			}
		});
	}

	if (delItems && delItems.length)
	{
		window[basketJSParams['DATA_LAYER_NAME']].push({
			'event': 'removeFromCart',
			'ecommerce': {
				'currencyCode': getCurrencyCode(),
				'remove': {
					'products': delItems
				}
			}
		});
	}
}

function getCurrencyCode()
{
	var root = BX('basket_items'),
		node,
		currency = '';

	if (root)
	{
		node = root.querySelector('[data-item-currency');
		node && (currency = node.getAttribute('data-item-currency'));
	}

	return currency;
}

BX.ready(function() {


	basketPoolQuantity = new BasketPoolQuantity();

	var couponBlock = BX('coupons_block'),
		basketItems = BX('basket_items');

	if (BX.type.isElementNode(couponBlock))
		BX.bindDelegate(couponBlock, 'click', { 'attribute': 'data-coupon' }, deleteCoupon);

	if (BX.type.isNotEmptyString(basketJSParams['EVENT_ONCHANGE_ON_START']) && basketJSParams['EVENT_ONCHANGE_ON_START'] == "Y")
		BX.onCustomEvent('OnBasketChange');
});