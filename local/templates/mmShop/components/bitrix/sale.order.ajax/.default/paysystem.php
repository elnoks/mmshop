<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<script type="text/javascript">
    function changePaySystem(param) {
        if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
        {
            if (param == 'account') {
                if (BX("PAY_CURRENT_ACCOUNT")) {
                    BX("PAY_CURRENT_ACCOUNT").checked = true;
                    BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                    BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

                    // deselect all other
                    var el = document.getElementsByName("PAY_SYSTEM_ID");
                    for (var i = 0; i < el.length; i++)
                        el[i].checked = false;
                }
            }
            else {
                BX("PAY_CURRENT_ACCOUNT").checked = false;
                BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
            }
        }
        else if (BX("account_only") && BX("account_only").value == 'N') {
            if (param == 'account') {
                if (BX("PAY_CURRENT_ACCOUNT")) {
                    BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

                    if (BX("PAY_CURRENT_ACCOUNT").checked) {
                        BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                        BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                    }
                    else {
                        BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                        BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                    }
                }
            }
        }

        submitForm();
    }
</script>
<div class="del-block">
    <?
    uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value


    ?>

    <div class="lk_section">
        <div class="select_mms" style="width: 100%" id="pay_method">
            <?php
            $arrCheckedPaySystem = array();
            foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
                if ($arPaySystem["CHECKED"] == "Y") {
                    ?>
                    <div class="select_mms_active">
                        <div><i class="mmsico_"
                                style="background-image:url(<?= $arPaySystem["PSA_LOGOTIP"]["SRC"] ?>)"></i><?= $arPaySystem["NAME"] ?>
                        </div>
                    </div>
                    <?
                    $arrCheckedPaySystem = $arPaySystem;
                }
            }
            ?>
            <div class="select_mms_vars">
                <?php
                foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
                    ?>
                    <a href="javascript:void(0)"
                       onclick="BX('ID_PAY_SYSTEM_ID').setAttribute('value', BX(this).getAttribute('data-val'));changePaySystem();;"
                       data-val="<?= $arPaySystem["ID"] ?>"
                    >
                        <i class="mmsico_"
                           style="background-image:url(<?= $arPaySystem["PSA_LOGOTIP"]["SRC"] ?>)"></i><?= $arPaySystem["NAME"] ?>
                    </a>
                    <?
                }
                ?>
            </div>
            <input type="hidden" id="ID_PAY_SYSTEM_ID" name="PAY_SYSTEM_ID">
        </div>
    </div>
    <div class="del_screen deliver_method">
        <div class="dd_screen">
            <p>
                <?php
                echo $arrCheckedPaySystem["DESCRIPTION"];
                ?>
            </p>
        </div>
    </div>
    <div class="lk_section">
        <h3><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?></h3>
        <textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" style="max-width:100%;min-height:120px"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea>
    </div>
</div>

