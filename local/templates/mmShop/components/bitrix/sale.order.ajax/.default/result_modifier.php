<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$newResN = $newResY = array();

foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"] as $prop)
    $newResN[$prop["GROUP_NAME"]][] = $prop;

foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"] as $prop)
    $newResY[$prop["GROUP_NAME"]][] = $prop;

$arResult["ORDER_PROP"]["USER_PROPS_N"] = $newResN;
$arResult["ORDER_PROP"]["USER_PROPS_Y"] = $newResY;

if($_POST["is_ajax_post"] == "Y" && empty($_POST["PROFILE_ID"])){
    foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $key => $arUserProfiles) {

        if($_POST["ORDER_PROP_1"] == $arUserProfiles["NAME"]){
            $arResult["ORDER_PROP"]["USER_PROFILES"][$key]["CHECKED"] = "Y";
        }
    }
}
