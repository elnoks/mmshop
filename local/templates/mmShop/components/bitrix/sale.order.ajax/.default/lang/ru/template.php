<?
$MESS['SOA_TEMPL_EMPTY_BUYER'] = "После совершения покупки здесь будет указан основной адрес доставки. Указать основной адрес доставки можно заранее, нажав кнопку “Добавить”.";
$MESS['SALE_PRODUCTS_BASKET_SUMMARY'] = "#COUNT# шт. на сумму #PRICE#";
$MESS['SOA_TEMPL_SIZE_IB'] = "#SIZE# размер";
$MESS['SOA_TEMPL_GETTER'] = "Получатель заказа";
$MESS['SOA_TEMPL_CHANGE_BUTTON'] = "Изменить";
$MESS['SOA_TEMPL_CLIENT_CHOOSE'] = "Выбор получателя";
$MESS['SOA_TEMPL_TO_ORDER'] = "К подтверждению заказа";
$MESS['SOA_TEMPL_CLIENT'] = "Получатель";
$MESS['SOA_TEMPL_BACK'] = "Назад";
$MESS['SOA_TEMPL_NEXT'] = "Далее";
$MESS['SOA_TEMPL_PERSON_TYPE'] = "Тип плательщика";
$MESS['SOA_TEMPL_BUTTON'] = "Подтвердить заказ";
$MESS['SOA_TEMPL_PROP_INFO'] = "Информация для оплаты и доставки заказа";
$MESS['SOA_TEMPL_PROP_CHOOSE'] = "Адрес доставки";
$MESS['SOA_TEMPL_PROP_VARIANT'] = "Вариант доставки";
$MESS['SOA_TEMPL_PROP_PAYMENT'] = "Способ оплаты";
$MESS['SOA_TEMPL_PROP_ADD'] = "Добавить";
$MESS['SOA_POPUP_CREATE'] = "Создать профиль";
$MESS['SOA_TEMPL_PROP_MAIN_ADDRES'] = "Адрес";
$MESS['SOA_TEMPL_PROP_MAIN_EMAIL'] = "Email";
$MESS['SOA_TEMPL_PROP_MAIN_PHONE'] = "Телефон";
$MESS['SOA_TEMPL_EXISTING_PROFILE'] = "Профиль покупателя:";
$MESS['SOA_TEMPL_PROP_NEW_PROFILE'] = "Новый профиль";
$MESS['SOA_TEMPL_PAY_SYSTEM'] = "Оплата";
$MESS['SOA_TEMPL_PAY_ACCOUNT'] = "Оплатить с внутреннего счета";
$MESS['SOA_TEMPL_PAY_ACCOUNT1'] = "На вашем пользовательском счете есть";
$MESS['SOA_TEMPL_PAY_ACCOUNT2'] = "Вы можете использовать для полного или частичного погашения суммы заказа.";
$MESS['SOA_TEMPL_PAY_ACCOUNT3'] = "Средств достаточно для полной оплаты заказа.";
$MESS['SOA_TEMPL_DELIVERY'] = "Доставка";
$MESS['SOA_TEMPL_ORDER_SUC'] = "№ заказа #ORDER_ID#";
$MESS['SOA_TEMPL_ORDER_SUC1'] = "Вы можете следить за выполнением своего заказа в <a href=\"#LINK#\">Персональном разделе сайта</a>. Обратите внимание, что для входа в этот раздел вам необходимо будет ввести логин и пароль пользователя сайта.";
$MESS['SOA_TEMPL_PAY'] = "Оплата заказа";
$MESS['SOA_TEMPL_PAY_LINK'] = "Если окно с платежной информацией не открылось автоматически, нажмите на ссылку <a href=\"#LINK#\" target=\"_blank\">Оплатить заказ</a>.";
$MESS['SOA_TEMPL_PAY_PDF'] = "Для того, чтобы скачать счет в формате pdf, нажмите на ссылку <a href=\"#LINK#\" target=\"_blank\">Скачать счет</a>.";
$MESS['SOA_TEMPL_ERROR_ORDER'] = "Ошибка формирования заказа";
$MESS['SOA_TEMPL_ERROR_ORDER_LOST'] = "Заказ №#ORDER_ID# не найден.";
$MESS['SOA_TEMPL_ERROR_ORDER_LOST1'] = "Пожалуйста обратитесь к администрации интернет-магазина или попробуйте оформить ваш заказ еще раз.";
$MESS['SOA_TEMPL_SUM_NAME'] = "Название";
$MESS['SOA_TEMPL_SUM_PROPS'] = "Свойства";
$MESS['SOA_TEMPL_SUM_PRICE_TYPE'] = "Тип цены";
$MESS['SOA_TEMPL_SUM_DISCOUNT'] = "Персональная ссылка <span class='sale_percent' data-percent='#DISCOUNT#'>(#DISCOUNT#)</span>";
$MESS['SOA_TEMPL_SUM_WEIGHT'] = "Вес";
$MESS['SOA_TEMPL_SUM_QUANTITY'] = "Кол-во";
$MESS['SOA_TEMPL_SUM_PRICE'] = "Цена";
$MESS['SOA_TEMPL_SUM_WEIGHT_SUM'] = "Общий вес:";
$MESS['SOA_TEMPL_SUM_SUMMARY'] = "Стоимость товара";
$MESS['SOA_TEMPL_SUM_VAT'] = "НДС:";
$MESS['SOA_TEMPL_SUM_DELIVERY'] = "Доставка";
$MESS['SOA_TEMPL_SUM_IT'] = "Сумма к оплате";
$MESS['SOA_TEMPL_SUM_PAYED'] = "Оплачено:";
$MESS['SOA_TEMPL_SUM_LEFT_TO_PAY'] = "Осталось оплатить:";
$MESS['SOA_TEMPL_SUM_ADIT_INFO'] = "Дополнительная информация";
$MESS['SOA_TEMPL_SUM_COMMENTS'] = "Комментарий";
$MESS['SOA_TEMPL_SUM_TITLE'] = "Состав заказа";
$MESS['SOA_TEMPL_ORDER_COMPLETE'] = "Заказ сформирован";
$MESS['STOF_PRIVATE_NOTES'] = "Личные сведения, полученные в распоряжение интернет-магазина при регистрации или каким-либо иным образом, не будут без разрешения пользователей передаваться третьим организациям и лицам за исключением ситуаций, когда этого требует закон или судебное решение.";
$MESS['CAPTCHA_REGF_TITLE'] = "Защита от автоматической регистрации";
$MESS['CAPTCHA_REGF_PROMT'] = "Введите слово на картинке";
$MESS['STOF_2REG'] = "Для вернувшихся покупателей";
$MESS['STOF_2NEW'] = "Для новых покупателей";
$MESS['STOF_LOGIN_PROMT'] = "Если вы помните свой логин и пароль, то введите их в соответствующие поля:";
$MESS['STOF_LOGIN'] = "Логин";
$MESS['STOF_PASSWORD'] = "Пароль";
$MESS['STOF_FORGET_PASSWORD'] = "Забыли пароль?";
$MESS['STOF_NEXT_STEP'] = "Продолжить оформление заказа";
$MESS['STOF_NAME'] = "Имя";
$MESS['STOF_LASTNAME'] = "Фамилия";
$MESS['STOF_MY_PASSWORD'] = "Задать логин и пароль";
$MESS['STOF_RE_PASSWORD'] = "Повтор пароля";
$MESS['STOF_SYS_PASSWORD'] = "Сгенерировать логин и пароль";
$MESS['STOF_REQUIED_FIELDS_NOTE'] = "Символом \"звездочка\" (<font color=\"#FF0000\">*</font>) отмечены обязательные для заполнения поля.";
$MESS['STOF_EMAIL_NOTE'] = "После регистрации вы получите информационное письмо.";
$MESS['SALE_DELIV_PRICE'] = "Стоимость";
$MESS['SALE_SADC_TRANSIT'] = 'Срок доставки';
$MESS['SALE_SADC_PACKS'] = 'Необходимо коробок (штук)';
$MESS['SOA_NO_JS'] = "Для оформления заказа необходимо включить JavaScript. По-видимому, JavaScript либо не поддерживается браузером, либо отключен. Измените настройки браузера и затем <a href=\"\">повторите попытку</a>.";
$MESS['SOA_TEMPL_BUYER_INFO'] = "Информация о покупателе";
$MESS['SOA_TEMPL_BUYER_SHOW'] = "Развернуть";
$MESS['SOA_TEMPL_BUYER_HIDE'] = "Свернуть";
$MESS['SOA_TEMPL_SUM_PICTURE'] = "Изображение";
$MESS['SOA_POPUP_SAVE'] = "Сохранить";
$MESS['SOA_ORDER_GIVE'] = "Пункты выдачи заказа";
$MESS['SOA_ORDER_GIVE_TITLE'] = "Пункт выдачи";
$MESS['SOA_TEMPL_RELATED_PROPS'] = "Свойства, связанные с оплатой и доставкой";
$MESS['SOA_TEMPL_PAYSYSTEM_PRICE'] = "За наложенный платеж: #PAYSYSTEM_PRICE#";
$MESS['SALE_TYPE'] = "Тип цены";
$MESS['SALE_PRODUCTS'] = "Товары";
$MESS['SALE_PRODUCTS_SUMMARY'] = "Товары в заказе";
$MESS['SALE_PACKS_COUNT'] = "Необходимо коробок (штук)";
$MESS['SOA_ORDER_DELIVERY_EXTRA_PARAMS'] = "Дополнительные параметры для доставки";
$MESS['SOA_ORDER_DELIVERY_EXTRA_PARAMS_SAVE_ERROR'] = "Ошибка сохранения данных";
$MESS['SOA_OTHER_LOCATION'] = "Другое местоположение";
$MESS['SOA_LOCATION_NOT_FOUND'] = "Местоположение не найдено";
$MESS['SOA_LOCATION_NOT_FOUND_PROMPT'] = "Нажмите #ANCHOR#добавить местоположение#ANCHOR_END#, чтобы мы узнали, куда нам доставить ваш заказ";
$MESS['SOA_NOT_SELECTED_ALT'] = "При необходимости уточнить местоположение";
$MESS['SOA_TEMPL_ORDER_PS_ERROR'] = "Ошибка выбранного способа оплаты. Обратитесь к Администрации сайта, либо выберите другой способ оплаты.";
$MESS["CITY_ZIP_END"] = "#CITY_ZIP_END#, ";
$MESS["LOCATION"] = "г. #LOCATION#";
$MESS["STREET"] = ", ул. #STREET#";
$MESS["CITY_HOUSE_BEGIN"] = ", #CITY_HOUSE_BEGIN#";
$MESS["ORDER_DONE"] = "Ваш заказ успешно оформлен";
$MESS["ORDER_LINK"] = "Для просмотра заказа перейдите по <a class='ord_link' href='/personal/orders/'>ссылке</a>";
$MESS["ORDER_TIME"] = "В ближайшее время с Вами свяжется менеджер для подтверждения заказа. Мы работаем с понедельника по пятницу с 09:00 до 17:00 по МСК.";
$MESS["ORDER_QUESTION"] = "Если у Вас есть какие-то вопросы, свяжитесь с нами по телефону +7 (951) 824-29-25 или <a href='javascript:void(0)' class='ord_link' onclick=\"openCallForm('/', 'get_question')\" data-toggle='modal' data-target='#get_question'>задайте свой вопроc</a>";
?>