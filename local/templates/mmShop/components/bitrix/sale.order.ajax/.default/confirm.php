<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Sale;
use Bitrix\Main\Localization\Loc;
if (!empty($arResult["ORDER"]))
{
    $order = Sale\Order::load($arResult["ORDER"]["ID"]);
    $paymentCollection = $order->getPaymentCollection();
    if (count($paymentCollection) == 1) {
        foreach ($paymentCollection as $payment) {
            $psID = $payment->getPaymentSystemId();
            $payment->delete();

            $paymentAll = $paymentCollection->createItem(
                Bitrix\Sale\PaySystem\Manager::getObjectById($psID)
            );
            $paymentAll->setField("SUM", $order->getPrice() - $order->getDeliveryPrice());

            $paymentDelivery = $paymentCollection->createItem(
                Bitrix\Sale\PaySystem\Manager::getObjectById(10)
            );
            $paymentDelivery->setField("SUM", $order->getDeliveryPrice());
        }
        $order->save();
    }
	?>
    <div class="row">
        <div class="col-md-12 text-center profile-created">
            <h1><?= Loc::getMessage("ORDER_DONE"); ?></h1>
            <p style="font-size: 16px;"><strong><?= Loc::getMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?></strong></p>
            <p><?= Loc::getMessage("ORDER_LINK"); ?></p>
        </div>
    </div>
    <div class="delimer"></div>
    <div class="row">
        <div class="col-sm-2 hidden-xs"></div>
        <div class="col-lg-3 col-xs-12 nopad">
            <p><?= Loc::getMessage("ORDER_TIME"); ?></p>
        </div>
        <div class="col-sm-2 hidden-xs"><div class="delimerVert"></div></div>
        <div class="col-lg-3 col-xs-12 nopad">
            <p><?= Loc::getMessage("ORDER_QUESTION"); ?></p>
        </div>
        <div class="col-sm-2 hidden-xs"></div>
    </div>
<?
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
?>
