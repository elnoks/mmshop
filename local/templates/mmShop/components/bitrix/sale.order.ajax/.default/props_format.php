<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (!function_exists("PrintPropsFormEasy")) {
    function PrintPropsFormEasy($arSource = array(), $showEasyArr = array(), $locationTemplate = "")
    {
        if ($locationTemplate == "FINISH") {
            $arr = array_shift($arSource);
            foreach ($arr as $arProperties) {
                if (in_array($arProperties["CODE"], $showEasyArr)) {
                    echo $arProperties["VALUE"]." ";
                }
            }
        }
        elseif ($locationTemplate == "CITY") {
            $arr = array();
            $newProfList = "";
            $toDeliver = next($arSource);

            foreach ($showEasyArr as $nedle)
                foreach ($toDeliver as $item)
                    if ($item["CODE"] == $nedle)
                        $arr[] = $item;

            foreach ($arr as $item) {
                if (!empty($item["VALUE"])) {
                    if ($item["CODE"] == "LOCATION")
                        $val = CSaleLocation::GetByID($item["VALUE"], "ru")["CITY_NAME"];
                    else
                        $val = $item["VALUE"];
                    $newProfList .= Loc::getMessage($item["CODE"], array("#" . $item["CODE"] . "#" => $val));
                }
            }
            echo $newProfList;
        }elseif ($locationTemplate == "HIDE_ALL") {
            $date = array_shift($arSource);
            foreach ($date as $arProperties) {
                if ($arProperties["TYPE"] == "DATE") {
                    ?>
                    <div class="lk_section_50 input_pr_10">
                        <h3><?= $arProperties["NAME"] ?></h3>
                        <div class="propDate">
                            <?
                            global $APPLICATION;
                            $APPLICATION->IncludeComponent('bitrix:main.calendar', '', array(
                                'SHOW_INPUT' => 'Y',
                                'INPUT_NAME' => "ORDER_PROP_" . $arProperties["ID"],
                                'INPUT_VALUE' => $arProperties["VALUE"],
                                'SHOW_TIME' => 'Y'
                            ), null, array('HIDE_ICONS' => 'N'));
                            ?>
                        </div>
                    </div>
                    <?
                } elseif ($arProperties["TYPE"] == "SELECT") {
                    ?>
                    <div class="lk_section_50">
                        <h3><?= $arProperties["NAME"] ?></h3>
                        <div class="select_mms" style="width: 100%; float: none;" id="select_mms">
                            <?php
                            foreach ($arProperties["VARIANTS"] as $arVariants) {
                                if ($arVariants["SELECTED"] == "Y") {
                                    ?>
                                    <div class="select_mms_active" style="padding-left: 8px;"><?= $arVariants["NAME"] ?></div>
                                    <?
                                }
                            }
                            ?>
                            <div class="select_mms_vars" id="cb_method">
                                <?php
                                foreach ($arProperties["VARIANTS"] as $arVariants) {
                                    ?>
                                    <a href="javascript:void(0)"
                                       style="padding-left: 8px;"
                                       data-value="<?= $arVariants["VALUE"] ?>"
                                       onclick="closeSelect(this); BX('<?= $arProperties["FIELD_NAME"] ?>').value = this.getAttribute('data-value');">
                                        <?= $arVariants["NAME"] ?>
                                    </a>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>"
                               id="<?= $arProperties["FIELD_NAME"] ?>">
                    </div>
                    <?
                }
            }
        } else {
            $personal = array_shift($arSource);
            $toDeliver = array_shift($arSource);
            $arSource = array_merge($personal, $toDeliver);
            foreach ($arSource as $arProperties) {
                ?>
                <input type="hidden" size="<?= $arProperties["VALUE"] ?>"
                       value="<?= $arProperties["VALUE"] ?>"
                       name="<?= $arProperties["FIELD_NAME"] ?>"
                       id="<?= $arProperties["FIELD_NAME"] ?>">
                <?
            }
            $first = $arr = array();
            $newProfList = "";

            foreach ($showEasyArr as $nedle)
                foreach ($toDeliver as $item)
                    if ($item["CODE"] == $nedle)
                        $arr[] = $item;

            foreach ($showEasyArr as $nedle)
                foreach ($personal as $item)
                    if ($item["CODE"] == $nedle)
                        $first[] = $arr[] = $item;

            foreach ($arr as $item) {
                if (!empty($item["VALUE"])) {
                    if ($item["CODE"] == "LOCATION")
                        $val = CSaleLocation::GetByID($item["VALUE"], "ru")["CITY_NAME"];
                    else
                        $val = $item["VALUE"];
                    $newProfList .= Loc::getMessage($item["CODE"], array("#" . $item["CODE"] . "#" => $val));
                }
            }
            ?>
            <div class="dd_screen">
                <div class="lk_section">
                    <a class="lk-edit"
                       href="javascript:void(0)"
                       onclick="actionProfile('currentProfileUser', 'edit', 'cityProp')"
                    ></a>
                    <h3><?= Loc::getMessage("SOA_TEMPL_PROP_MAIN_ADDRES") ?></h3>
                    <span id="sityFieldFrom"><?= $newProfList ?></span>
                </div>
                <?php
                foreach ($first as $item) {
                    ?>
                    <div class="lk_section">
                        <h3><?= Loc::getMessage("SOA_TEMPL_PROP_MAIN_" . $item["CODE"]) ?></h3>
                        <?= $item["VALUE"] ?>
                    </div>
                    <?
                }
                ?>
            </div>
            <?
        }
    }
}
if (!function_exists("PrintPropsForm")) {
    function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $secator = "")
    {
        if (!empty($arSource)) {
            if ($secator == "PROFILE")
                $personal = array_shift($arSource);
            elseif ($secator == "CITY")
                $toDeliver = next($arSource);
            else {
                $personal = array_shift($arSource);
                $toDeliver = array_shift($arSource);
            }

            foreach ($personal as $arProperties) {
                if ($arProperties["CODE"] == "FIO") {
                    ?>
                    <div class="lk_section">
                        <div data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>">
                            <label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
                            <input type="text" size="<?= $arProperties["SIZE1"] ?>"
                                   value="<?= $arProperties["VALUE"] ?>"
                                   name="<?= $arProperties["FIELD_NAME"] ?>"
                                   id="<?= $arProperties["FIELD_NAME"] ?>"
                                   data-required="<?= $arProperties["REQUIRED"] ?>"
                            >
                        </div>
                    </div>
                    <?
                } else {
                    if ($arProperties["CODE"] == "EMAIL") {
                        ?>
                        <div class="lk_section">
                        <div class="row">
                        <?
                    }
                    ?>
                    <div class="col-lg-6" data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>">
                        <label for="<?= $arProperties["FIELD_NAME"] ?>"><?= $arProperties["NAME"] ?></label>
                        <input type="text" size="<?= $arProperties["SIZE1"] ?>"
                               value="<?= $arProperties["VALUE"] ?>"
                               name="<?= $arProperties["FIELD_NAME"] ?>"
                               id="<?= $arProperties["FIELD_NAME"] ?>"
                               data-required="<?= $arProperties["REQUIRED"] ?>"
                        >
                    </div>
                    <?
                    if ($arProperties["CODE"] == "PHONE") {
                        ?>
                        </div>
                        </div>
                        <?
                    }
                }
            }
            ?>
            <div class="sm-input-wrap">
                <?
                foreach ($toDeliver as $arProperties) {
                    if ($arProperties["CODE"] == "LOCATION") {

                        ?>
                        <span data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>">
                            <label for="<?= $arProperties["FIELD_NAME"] ?>">
                                <?= $arProperties["NAME"] ?>
                            </label>
                            <?
                            $value = 0;
                            if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0) {
                                foreach ($arProperties["VARIANTS"] as $arVariant) {
                                    if ($arVariant["SELECTED"] == "Y") {
                                        $value = $arVariant["ID"];
                                        break;
                                    }
                                }
                            }

                            if (CSaleLocation::isLocationProMigrated()) {
                                $locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
                                $locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
                            }

                            if ($locationTemplateP == 'steps'):?>
                                <input type="hidden"
                                       id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?= intval($arProperties["ID"]) ?>]"
                                       name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?= intval($arProperties["ID"]) ?>]"
                                       value="<?= ($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0') ?>"/>
                            <?endif;

                            CSaleLocation::proxySaleAjaxLocationsComponent(array(
                                "AJAX_CALL" => "N",
                                "COUNTRY_INPUT_NAME" => "COUNTRY",
                                "REGION_INPUT_NAME" => "REGION",
                                "CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
                                "CITY_OUT_LOCATION" => "Y",
                                "LOCATION_VALUE" => $value,
                                "ORDER_PROPS_ID" => $arProperties["ID"],
                                "ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
                                "SIZE1" => $arProperties["SIZE1"],
                            ),
                                array(
                                    "ID" => $value,
                                    "CODE" => "",
                                    "SHOW_DEFAULT_LOCATIONS" => "Y",

                                    // function called on each location change caused by user or by program
                                    // it may be replaced with global component dispatch mechanism coming soon
                                    "JS_CALLBACK" => "submitFormProxy",

                                    // function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
                                    // it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
                                    "JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

                                    // an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
                                    // it may be replaced with global component dispatch mechanism coming soon
                                    "JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

                                    "DISABLE_KEYBOARD_INPUT" => "N",
                                    "PRECACHE_LAST_LEVEL" => "Y",
                                    "PRESELECT_TREE_TRUNK" => "Y",
                                    "SUPPRESS_ERRORS" => "Y"
                                ),
                                $locationTemplateP,
                                true,
                                'location-block-wrapper'
                            );

                            if (CSaleLocation::isLocationProEnabled()) {
                                ?>

                                <?
                                $propertyAttributes = array(
                                    'type' => $arProperties["TYPE"],
                                    'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
                                );

                                if (intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
                                    $propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

                                if (intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
                                    $propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

                                if ($arProperties['IS_ZIP'] == 'Y')
                                    $propertyAttributes['isZip'] = true;
                                ?>

                                <script>

                                    <?// add property info to have client-side control on it?>
                                    (window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
                                        'id' => intval($arProperties["ID"]),
                                        'attributes' => $propertyAttributes
                                    ))?>);

                                </script>
                                <?
                            }
                            ?>
                        </span>
                        <?
                    } elseif ($arProperties["CODE"] == "STREET") {
                        ?>
                        <span data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>">
                            <label for="<?= $arProperties["FIELD_NAME"] ?>">
                                <?= $arProperties["NAME"] ?>
                            </label>
                            <input type="text" size="<?= $arProperties["SIZE1"] ?>"
                                   value="<?= $arProperties["VALUE"] ?>"
                                   name="<?= $arProperties["FIELD_NAME"] ?>"
                                   id="<?= $arProperties["FIELD_NAME"] ?>"
                                   data-required="<?= $arProperties["REQUIRED"] ?>"
                                   onkeyup="onSetProperty(this)"
                                <? $arProperties["REQUIRED"] == "Y" ? "requared" : "" ?>
                            >
                </span>
                        <?
                    } else {
                        $pos = strpos($arProperties["CODE"], "BEGIN");
                        if ($pos !== false)
                            echo "<div class='flex-for-input'>";
                        ?>
                        <label
                                for="<?= $arProperties["FIELD_NAME"] ?>"
                                data-property-id-row="<?= intval(intval($arProperties["ID"])) ?>"
                        >
                            <?= $arProperties["NAME"] ?>
                            <input class="mt10" type="text" size="<?= $arProperties["SIZE1"] ?>"
                                   value="<?= $arProperties["VALUE"] ?>"
                                   name="<?= $arProperties["FIELD_NAME"] ?>"
                                   id="<?= $arProperties["FIELD_NAME"] ?>">
                        </label>
                        <?
                        $pos = strpos($arProperties["CODE"], "END");
                        if ($pos !== false)
                            echo "</div>";
                    }
                }
                ?>
            </div>
            <?
        }
    }
}
?>