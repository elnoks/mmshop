<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$bDefaultColumns = $arResult["GRID"]["DEFAULT_COLUMNS"];
$colspan = ($bDefaultColumns) ? count($arResult["GRID"]["HEADERS"]) : count($arResult["GRID"]["HEADERS"]) - 1;
$bPropsColumn = false;
$bUseDiscount = false;
$bPriceType = false;
$bShowNameWithPicture = ($bDefaultColumns) ? true : false; // flat to show name and picture column in one column

include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
?>

<div class="col-lg-12">
    <div class="buyer_info">
        <div class="info_row">
            <div class="info_title"><?= GetMessage("SOA_TEMPL_GETTER") ?></div>
            <div class="info_content">
                <?php
                PrintPropsFormEasy($arResult["ORDER_PROP"]["USER_PROPS_Y"], array("FIO", "PHONE"), "FINISH");
                ?>
            </div>
        </div>
        <div class="info_row">
            <div class="info_title"><?= GetMessage("SOA_TEMPL_PROP_CHOOSE") ?></div>
            <div class="info_content" id="cityField">
                <?php
                PrintPropsFormEasy($arResult["ORDER_PROP"]["USER_PROPS_Y"], array("CITY_ZIP_END", "LOCATION", "STREET", "CITY_HOUSE_BEGIN"), "CITY");
                ?>
            </div>
        </div>
        <div class="info_row">
            <div class="info_title"><?= GetMessage("SOA_TEMPL_PROP_VARIANT") ?></div>
            <div class="info_content"> <?php
                foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
                    if ($arDelivery["CHECKED"] == "Y") {
                        echo $arDelivery["NAME"];
                    }
                }
                ?>
            </div>
        </div>
        <div class="info_row">
            <div class="info_title"><?= GetMessage("SOA_TEMPL_PROP_PAYMENT") ?></div>
            <div class="info_content">
                <?php
                foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
                    if ($arPaySystem["CHECKED"] == "Y") {
                        echo $arPaySystem["NAME"];
                    }
                }
                ?>
            </div>
        </div>
        <a class="info_change" id="backButton" onclick="showFirst();"
           href="javascript:void(0);"><?= GetMessage("SOA_TEMPL_CHANGE_BUTTON") ?></a>
    </div>
</div>

<div class="cart_meta">
    <div class="col-lg-7 pr30">
        <div class="promo_block items_at_cart_t">
            <div class="items_at_cart_total">
                <?= GetMessage("SALE_PRODUCTS_BASKET_SUMMARY", array('#COUNT#' => count($arResult["BASKET_ITEMS"]), '#PRICE#' => $arResult["ORDER_PRICE_FORMATED"])); ?>            </div>
            <div class="box-for-favorite mCustomScrollbar">
                <?php
                foreach ($arResult["BASKET_ITEMS"] as $item) {
                    if (strlen($arData["data"]["PREVIEW_PICTURE_SRC"]) > 0):
                        $url = $arData["data"]["PREVIEW_PICTURE_SRC"];
                    elseif (strlen($arData["data"]["DETAIL_PICTURE_SRC"]) > 0):
                        $url = $arData["data"]["DETAIL_PICTURE_SRC"];
                    else:
                        $url = $templateFolder."/images/no_photo.png";
                    endif;
                    ?>
                    <div class="cab-favorite-item flex-display">
                        <div class="fav-image-wrap">
                            <a class="cab-fav-link"><img src="<?= $url ?>" alt="<?= $item["NAME"] ?>"></a>
                        </div>
                        <div class="fav-item-info">
                            <p>
                                <a class="cab-fav-link"><?= $item["NAME"] ?></a>
                                <span class="fav-price">
                                <?
                                if ($item["DISCOUNT_PRICE"])
                                    echo "<strike>" . $item["SUM_BASE_FORMATED"] . "</strike>";
                                echo $item["PRICE_FORMATED"];
                                ?>
                             </span>
                            </p>
                            <p class="cab-fav-details">
                                <span><?=$item["QUANTITY"]." ".$item["MEASURE_NAME"]?></span>
                                <span><?= getFromHighloadbyXmlId(COLOR_IB, $item["PROPERTY_TSVET_VALUE"]) ?></span>
                                <span><?= GetMessage("SOA_TEMPL_SIZE_IB", array('#SIZE#' => getFromHighloadbyXmlId(SIZE_IB, $item["PROPERTY_RAZMERY_VALUE"]))) ?></span>
                            </p>
                        </div>
                    </div>
                    <?
                }
                ?>
            </div>
        </div>
    </div>


    <div class="col-lg-5 cart-total">
        <table>
            <tr>
                <td>
                    <?= GetMessage("SOA_TEMPL_SUM_SUMMARY") ?>
                </td>
                <td>
                    <span class="sum"><?= $arResult["PRICE_WITHOUT_DISCOUNT"] ?></span>
                </td>
            </tr>
            <?php
            if ($arResult["DISCOUNT_PRICE"] > 0) {
                ?>
                <tr>
                    <td>
                        <?= GetMessage("SOA_TEMPL_SUM_DISCOUNT", array("#DISCOUNT#" => getDiscountByUser())) ?>
                    </td>
                    <td>
                        <span class="sale_value">-<?=$arResult["DISCOUNT_PRICE"]?></span>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td>
                    <?= GetMessage("SOA_TEMPL_SUM_DELIVERY") ?>
                </td>
                <td>
                    <span class=""><?= $arResult["DELIVERY_PRICE_FORMATED"] ?></span>
                </td>
            </tr>
            <tr class='row_bold'>
                <td>
                    <?= GetMessage("SOA_TEMPL_SUM_IT") ?>
                </td>
                <td>
                    <span class="result_sum"><?= $arResult["ORDER_TOTAL_PRICE_FORMATED"] ?></span>
                </td>
            </tr>
        </table>
        <div class="row">
            <div class="btns_row">
                <div class="col-lg-5">
                    <a class="lk_grey_btn" id="backButton" onclick="showFirst()"
                       href="javascript:void(0);"><?= GetMessage("SOA_TEMPL_BACK") ?></a>
                </div>
                <div class="col-lg-7">
                    <a class='lk_orange_btn' href="javascript:void(0);" style="display:none"
                       onclick="submitForm('Y'); return false;"
                       id="ORDER_CONFIRM_BUTTON"><?= GetMessage("SOA_TEMPL_BUTTON") ?></a>
                </div>
            </div>
        </div>
    </div>
</div>