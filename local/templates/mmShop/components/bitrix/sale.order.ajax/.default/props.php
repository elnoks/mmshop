<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
?>

<div class="del-block">
    <?
    if (is_array($arResult["ORDER_PROP"]["USER_PROFILES"]) && !empty($arResult["ORDER_PROP"]["USER_PROFILES"]) && $USER->IsAuthorized()) {
        if ($_POST["condition"] != "edit" && ($_POST["PROFILE_ID"] != 0 || empty($_POST["PROFILE_ID"] ))) {
            ?>
            <h3 class="mb15"><?= GetMessage("SOA_TEMPL_CLIENT_CHOOSE") ?></h3>
            <a href="javascript:void(0);"
               id="editProfile_0"
               onclick="actionProfile('editProfile_0', 'add', '')"
               data-val="0"
               class="add_recip"><?= GetMessage("SOA_TEMPL_PROP_ADD") ?>
            </a>

            <div class="lk_section">
                <div class="select_mms">
                    <?php

                    foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                        if ($arUserProfiles["CHECKED"] == "Y") {
                            ?>
                            <div class="select_mms_active" id="currentProfileUser"
                                 data-val="<?= $arUserProfiles["ID"] ?>">
                                <i class="flaticon-user"></i><?= $arUserProfiles["NAME"] ?>
                            </div>
                            <?
                        }
                    }
                    ?>
                    <div class="select_mms_vars" style="overflow: hidden; display: none;">
                        <?
                        foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                            ?>
                            <a href="javascript:void(0)"
                               id="editProfile_<?= $arUserProfiles["ID"] ?>"
                               onclick="actionProfile('editProfile_<?= $arUserProfiles["ID"] ?>', 'change', '')"
                               data-val="<?= $arUserProfiles["ID"] ?>"
                            >
                                <i class="flaticon-user"></i><?= $arUserProfiles["NAME"] ?>
                            </a>
                            <?
                        }

                        ?>
                    </div>
                </div>
                <?php
                foreach ($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) {
                    if ($arUserProfiles["CHECKED"] == "Y") {
                        ?>
                        <a class="lk-edit"
                           href="javascript:void(0)"
                           onclick="actionProfile('currentProfileUser', 'edit', 'profProp')"
                        ></a>
                        <?
                    }
                }
                ?>
            </div>
            <?php
        }
        ?>
        <?php

        if ($_POST["condition"] == "edit" ||$_POST["condition"] == "add"){
            if ($_POST["whatEdit"] == "profProp")
                PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], "PROFILE");
            elseif($_POST["whatEdit"] == "cityProp")
                PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], "CITY");
            else
                PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
            ?>
            <div class="lk_section">
                <div class="btns_row oz_reciev">
                    <div class="row">
                        <div class="col-lg-5">
                            <a class="lk_grey_btn"
                               id="editProfileBackButton"
                               href="javascript:void(0);"
                               onclick="actionProfile('editProfileBackButton' , 'back', '')"
                               data-val="<?= $_POST["oldProfile"] > 0 ? $_POST["oldProfile"] : $_POST["PROFILE_ID"] ?>"
                            ><?= Loc::getMessage("SOA_TEMPL_BACK"); ?></a>
                        </div>
                        <?/*
                        <div class="col-lg-7">
                            <a class="lk_orange_btn"
                               href="javascript:void(0);"
                               onclick="actionProfile('editProfileBackButton' , 'save', '')"
                            ><?= Loc::getMessage("SOA_POPUP_SAVE"); ?></a>
                        </div>
                        */?>
                    </div>
                </div>
            </div>
            <?
        } elseif(!empty($_POST["PROFILE_ID"]) && !empty($_POST) ){
            PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
        }else{
            $personal = array("EMAIL", "PHONE", "CITY_ZIP_END", "LOCATION", "STREET", "CITY_HOUSE_BEGIN");
            PrintPropsFormEasy($arResult["ORDER_PROP"]["USER_PROPS_Y"], $personal, $arParams["TEMPLATE_LOCATION"]);
        }
    } else {
        PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"]);
    }
    ?>
</div>

<? if (!CSaleLocation::isLocationProEnabled()): ?>
    <div style="display:none;">
        <? $APPLICATION->IncludeComponent(
            "bitrix:sale.ajax.locations",
            $arParams["TEMPLATE_LOCATION"],
            array(
                "AJAX_CALL" => "N",
                "COUNTRY_INPUT_NAME" => "COUNTRY_tmp",
                "REGION_INPUT_NAME" => "REGION_tmp",
                "CITY_INPUT_NAME" => "tmp",
                "CITY_OUT_LOCATION" => "Y",
                "LOCATION_VALUE" => "",
                "ONCITYCHANGE" => "submitForm()",
            ),
            null,
            array('HIDE_ICONS' => 'Y')
        ); ?>
    </div>
<? endif ?>
