<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Catalog;

include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
?>

<script type="text/javascript">
    function GetBuyerStore() {
        BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
        //BX('ORDER_DESCRIPTION').value = '<?=GetMessage("SOA_ORDER_GIVE_TITLE")?>: '+BX('POPUP_STORE_NAME').value;
        BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
        BX.show(BX('select_store'));
    }

    function insertParamsToForm(deliveryId, paramsFormName) {
        var orderForm = BX("ORDER_FORM"),
            paramsForm = BX(paramsFormName);
        wrapDivId = deliveryId + "_extra_params";

        var wrapDiv = BX(wrapDivId);
        window.BX.SaleDeliveryExtraParams = {};

        if (wrapDiv)
            wrapDiv.parentNode.removeChild(wrapDiv);

        wrapDiv = BX.create('div', {props: {id: wrapDivId}});

        for (var i = paramsForm.elements.length - 1; i >= 0; i--) {
            var input = BX.create('input', {
                    props: {
                        type: 'hidden',
                        name: 'DELIVERY_EXTRA[' + deliveryId + '][' + paramsForm.elements[i].name + ']',
                        value: paramsForm.elements[i].value
                    }
                }
            );

            window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

            wrapDiv.appendChild(input);
        }

        orderForm.appendChild(wrapDiv);

        BX.onCustomEvent('onSaleDeliveryGetExtraParams', [window.BX.SaleDeliveryExtraParams]);
    }

    if (typeof submitForm === 'function')
        BX.addCustomEvent('onDeliveryExtraServiceValueChange', function () {
            submitForm();
        });
</script>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>"/>
<div class="del-block">
    <?
    if (!empty($arResult["DELIVERY"])) {
        $width = ($arParams["SHOW_STORES_IMAGES"] == "Y") ? 850 : 700;
        $arrCheckedDelivery = array();
        ?>
        <div class="lk_section">
            <div class="select_mms" style="width: 100%;">
                <?php
                foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
                    if ($arDelivery["CHECKED"] == "Y") {
                        ?>
                        <div class="select_mms_active">
                            <div><i class="mmsico_"
                                    style="background-image:url(<?= $arDelivery["LOGOTIP"]["SRC"] ?>)"></i><?= $arDelivery["NAME"] ?>
                            </div>
                        </div>
                        <?
                        $arrCheckedDelivery = $arDelivery;
                    }
                }
                ?>
                <div class="select_mms_vars">
                    <?php
                    foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery) {
                        ?>
                        <a href="javascript:void(0)"
                           onclick="BX('ID_DELIVERY_ID_').setAttribute('value', BX(this).getAttribute('data-val'));submitForm();"
                           data-val="<?= $arDelivery["ID"] ?>"
                        >
                            <i class="mmsico_"
                               style="background-image:url(<?= $arDelivery["LOGOTIP"]["SRC"] ?>)"></i><?= $arDelivery["NAME"] ?>
                        </a>
                        <?
                    }
                    ?>
                </div>
            </div>
            <input type="hidden" name="DELIVERY_ID" id="ID_DELIVERY_ID_">
        </div>
        <?
    }
    ?>
    <div class="del_screen deliver_method">
        <div class="dd_screen">
            <p>
                <?php
                echo $arrCheckedDelivery["DESCRIPTION"];
                ?>
            </p>
            <?php
            if (isset($arrCheckedDelivery["STORE"])) {
                $arStore = Catalog\StoreTable::getRow([
                    'select' => ['DESCRIPTION'],
                    'filter' => [
                        'ID' => $arrCheckedDelivery["STORE"][0],
                    ]
                ]);
                if ($arStore) {
                    ?>
                    <div class="dd_screen">
                        <div class="lk_section">
                            <div style='border: 1px solid #e8e8e8'>
                                <?php
                                echo $arStore["DESCRIPTION"]
                                ?>
                            </div>
                        </div>
                    </div>
                    <?
                }
                ?>
                <?php
                if (!empty($arrCheckedDelivery['EXTRA_SERVICES'])) {
                    ?>
                        <?
                        foreach ($arrCheckedDelivery['EXTRA_SERVICES'] as $extraServiceId => $extraService) {
                            if (!$extraService->canUserEditValue()) continue;
                            if ($extraService->getName() == "DATE_DELIVERY")
                                PrintPropsFormEasy($arResult["ORDER_PROP"]["USER_PROPS_N"], array("DATE_DELIVERY", "CALL_KUR"), "HIDE_ALL");
                        }
                        ?>
                    <?
                }
            }
            ?>
        </div>
    </div>
</div>