<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

?>
<? if (isset($arResult["REQUEST"]["ORIGINAL_QUERY"])) {
    ?>
    <div class="search-language-guess">
    <? echo Loc::getMessage("CT_BSP_KEYBOARD_WARNING", array("#query#" => '<a href="' . $arResult["ORIGINAL_QUERY_URL"] . '">' . $arResult["REQUEST"]["ORIGINAL_QUERY"] . '</a>')) ?>
    </div><?
} ?>

<? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false) {

} elseif ($arResult["ERROR_CODE"] != 0) {
    ?>
    <p><?= Loc::getMessage("SEARCH_ERROR") ?></p>
    <? ShowError($arResult["ERROR_TEXT"]); ?>
    <p><?= Loc::getMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>
    <p><?= Loc::getMessage("SEARCH_SINTAX") ?><br/><b><?= Loc::getMessage("SEARCH_LOGIC") ?></b></p>
    <table border="0" cellpadding="5">
        <tr>
            <td align="center" valign="top"><?= Loc::getMessage("SEARCH_OPERATOR") ?></td>
            <td valign="top"><?= Loc::getMessage("SEARCH_SYNONIM") ?></td>
            <td><?= Loc::getMessage("SEARCH_DESCRIPTION") ?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?= Loc::getMessage("SEARCH_AND") ?></td>
            <td valign="top">and, &amp;, +</td>
            <td><?= Loc::getMessage("SEARCH_AND_ALT") ?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?= Loc::getMessage("SEARCH_OR") ?></td>
            <td valign="top">or, |</td>
            <td><?= Loc::getMessage("SEARCH_OR_ALT") ?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?= Loc::getMessage("SEARCH_NOT") ?></td>
            <td valign="top">not, ~</td>
            <td><?= Loc::getMessage("SEARCH_NOT_ALT") ?></td>
        </tr>
        <tr>
            <td align="center" valign="top">( )</td>
            <td valign="top">&nbsp;</td>
            <td><?= Loc::getMessage("SEARCH_BRACKETS_ALT") ?></td>
        </tr>
    </table>
<? } elseif (count($arResult["SEARCH"]) > 0) {
    ?>
    <?
    $arSearchID = array();
    foreach ($arResult["SEARCH"] as $arItem) {
        $mxResult = CCatalogSku::GetProductInfo($arItem["ITEM_ID"]);

        if (is_array($mxResult)) {
            $arSearchID[] = $mxResult['ID'];
        } else {
            $arSearchID[] = $arItem['ITEM_ID'];
        }
    }
    $arSearchID = array_unique($arSearchID);

    $pageElementCount = 30;
    $step = array(30, 60, 120);
    if (array_key_exists("showBy", $_REQUEST)) {
        if (intVal($_REQUEST["showBy"]) && in_array(intVal($_REQUEST["showBy"]), $step)) {
            $pageElementCount = intVal($_REQUEST["showBy"]);
            $_SESSION["showBy"] = $pageElementCount;
        } elseif ($_SESSION["showBy"]) {
            $pageElementCount = intVal($_SESSION["showBy"]);
        }
    }

    ?>
    <div class="col-lg-12">
        <?php
        echo "<h1>" . Loc::getMessage("SEARCH_REQUEST", array("#QUESTION#" => $_REQUEST["q"])) . "</h1>";
        echo "<p class='search-count'>" . Loc::getMessage("SEARCH_FIND", array("#COUNT#" => count($arSearchID))) . "</p>";

        $priceId = 1;
        $dbPriceType = CCatalogGroup::GetList(
            array(),
            array("NAME" => "retail")
        );
        if ($arPriceType = $dbPriceType->Fetch())
            $priceId = $arPriceType["ID"];

        $sortField = 'ID';
        $sortOrder = 'asc';

        if (
            isset($_GET["sort"]) && isset($_GET["method"]) &&
            (
                $_GET["sort"] == "catalog_PRICE_" . $priceId ||
                $_GET["sort"] == "property_NOVINKA" ||
                $_GET["sort"] == "property_SPETSPREDLOZHENIE"
            )
        ) {
            $sortField = $_GET["sort"];
            $sortOrder = $_GET["method"];
        }
        $this->SetViewTarget("showBy");
        ?>
        <span><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY"); ?></span>
        <ul>
            <?
            foreach ($step as $i) {
                ?>
                <li <? if ($i == $pageElementCount): ?>class="active"<? endif; ?>>
                    <a rel="nofollow"
                       href="<?= $APPLICATION->GetCurPageParam('showBy=' . $i, array('showBy', 'mode')) ?>"><?= $i ?></a>
                </li>
                <?
            }
            ?>
        </ul>
        <?php
        $this->EndViewTarget("showBy");
        ?>

        <div class="cat_sub_line">
            <div class="items_per_page">
                <?php
                if (count($arSearchID) > $step[0]) $APPLICATION->ShowViewContent("showBy");
                ?>
            </div>
            <div class="mobile_filter_trigger"></div>
            <div class="sort_filters">
                <span><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY"); ?></span>
                <ul>
                    <li>
                        <a href="<?= $APPLICATION->GetCurPageParam("", array("showBy", "sort", "method")); ?>"><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY_USER"); ?></a>
                    </li>
                    <li class="<?
                    if ($sortField == "property_NOVINKA") echo "active"; ?>">
                        <a href="<?= $arResult["SECTION_PAGE_URL"] ?>?sort=property_NOVINKA&method=asc"><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY_NEW"); ?></a>
                    </li>
                    <li class="<?
                    if ($sortField == "catalog_PRICE_" . $priceId && $sortOrder == "desc") echo "active"; ?>">
                        <a href="<?= $arResult["SECTION_PAGE_URL"] ?>?sort=catalog_PRICE_<?= $priceId ?>&method=desc"><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY_COST"); ?></a>
                    </li>
                    <li class="<?
                    if ($sortField == "catalog_PRICE_" . $priceId && $sortOrder == "asc") echo "active"; ?>">
                        <a href="<?= $arResult["SECTION_PAGE_URL"] ?>?sort=catalog_PRICE_<?= $priceId ?>&method=asc"><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY_CHEAP"); ?></a>
                    </li>
                    <li class="<?
                    if ($sortField == "property_NOVINKA") echo "active"; ?>">
                        <a href="<?= $arResult["SECTION_PAGE_URL"] ?>?sort=property_SPETSPREDLOZHENIE<?= $priceId ?>&method=asc"><?= Loc::getMessage("CT_BCS_CATALOG_MESS_SHOW_BY_SALE"); ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?
    global $arSearch;
    $arSearch = array("ID" => $arSearchID);

    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        ".default",
        array(
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/cart/",
            "BROWSER_TITLE" => "-",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COMPATIBLE_MODE" => "N",
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "CUSTOM_FILTER" => "",
            "DETAIL_URL" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISCOUNT_PERCENT_POSITION" => "top-left",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "ELEMENT_SORT_FIELD" => $sortField,
            "ELEMENT_SORT_FIELD2" => $sortOrder,
            "ELEMENT_SORT_ORDER" => "desc",
            "ELEMENT_SORT_ORDER2" => "desc",
            "ENLARGE_PRODUCT" => "STRICT",
            "USE_FILTER" => "Y",
            "FILTER_NAME" => "arSearch",
            "HIDE_NOT_AVAILABLE" => "Y",
            "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
            "IBLOCK_ID" => "4",
            "IBLOCK_TYPE" => "1c_catalog",
            "IBLOCK_TYPE_ID" => "catalog",
            "INCLUDE_SUBSECTIONS" => "N",
            "LABEL_PROP" => array(
                0 => "SPETSPREDLOZHENIE",
                1 => "NOVINKA",
            ),
            "LABEL_PROP_MOBILE" => array(),
            "LABEL_PROP_POSITION" => "top-left",
            "LAZY_LOAD" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "LOAD_ON_SCROLL" => "N",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "Добавит в корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_LAZY_LOAD" => "Показать ещё",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_CART_PROPERTIES" => array(),
            "OFFERS_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "OFFERS_PROPERTY_CODE" => array(
                0 => "TSVET",
                1 => "RAZMERY",
                2 => "",
            ),
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "desc",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
            "OFFER_TREE_PROPS" => array(
                0 => "TSVET",
                1 => "RAZMERY",
            ),
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "round",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => $pageElementCount,
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                0 => "retail",
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPERTIES" => array(),
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "5",
            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "PROPERTY_CODE" => array(
                0 => "BREND",
                1 => "TIP_MODELI",
                2 => "",
            ),
            "PROPERTY_CODE_MOBILE" => array(),
            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
            "RCM_TYPE" => "personal",
            "SECTION_CODE" => "",
            "SECTION_ID" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SEF_MODE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_ALL_WO_SECTION" => "Y",
            "SHOW_CLOSE_POPUP" => "Y",
            "SHOW_DISCOUNT_PERCENT" => "Y",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "Y",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "N",
            "SLIDER_INTERVAL" => "0",
            "SLIDER_PROGRESS" => "N",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "COMPONENT_TEMPLATE" => ".default"
        ),
        false
    ); ?>
    <?
} else {
    ?>
    <div class="col-lg-12">
        <?
        echo "<h1>" . Loc::getMessage("SEARCH_REQUEST", array("#QUESTION#" => $_REQUEST["q"])) . "</h1>";
        echo "<p class='search-count'>" . Loc::getMessage("SEARCH_FIND", array("#COUNT#" => count($arSearchID))) . "</p>";
        ?>
    </div>
    <div class="row">
        <div class="col-md-12 text-center find-nothing">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/include/nothing-find.php",
                    "EDIT_TEMPLATE" => ""
                ),
                false
            ); ?>
            <div class="search-form">
                <form action="" method="get">
                    <i class="ico_find"></i>
                    <input type="text"  name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>"/>
                    <input type="hidden" value="<?= Loc::getMessage("SEARCH_GO") ?>"/>
                </form>
            </div>
        </div>
    </div>

    <? $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "search_menu",
        array(
            "ROOT_MENU_TYPE" => "top",
            "MAX_LEVEL" => "2",
            "CHILD_MENU_TYPE" => "top",
            "USE_EXT" => "Y",
            "DELAY" => "N",
            "ALLOW_MULTI_SELECT" => "Y",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS" => array(),
            "COMPONENT_TEMPLATE" => "search_menu",
            "MENU_THEME" => "site",
            "LINK_NEW" => "/2",
            "LINK_SALE" => "/3"
        ),
        false
    ); ?>

    <?$APPLICATION->IncludeComponent(
        "bitrix:catalog.section",
        ".default",
        array(
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "-",
            "ADD_PROPERTIES_TO_BASKET" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "BACKGROUND_IMAGE" => "-",
            "BASKET_URL" => "/personal/cart/",
            "BLOCK_NAME" => "Рекомендуем",
            "BROWSER_TITLE" => "-",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COMPATIBLE_MODE" => "N",
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
            "DETAIL_URL" => "",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "DISCOUNT_PERCENT_POSITION" => "top-left",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_COMPARE" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER" => "desc",
            "ELEMENT_SORT_ORDER2" => "desc",
            "ENLARGE_PRODUCT" => "STRICT",
            "FILTER_NAME" => "",
            "HIDE_NOT_AVAILABLE" => "Y",
            "HIDE_NOT_AVAILABLE_OFFERS" => "Y",
            "IBLOCK_ID" => "4",
            "IBLOCK_TYPE" => "1c_catalog",
            "IBLOCK_TYPE_ID" => "catalog",
            "INCLUDE_SUBSECTIONS" => "N",
            "LABEL_PROP" => array(
                0 => "SPETSPREDLOZHENIE",
                1 => "NOVINKA",
            ),
            "LABEL_PROP_MOBILE" => array(
            ),
            "LABEL_PROP_POSITION" => "top-left",
            "LAZY_LOAD" => "N",
            "LINE_ELEMENT_COUNT" => "3",
            "LOAD_ON_SCROLL" => "N",
            "MESSAGE_404" => "",
            "MESS_BTN_ADD_TO_BASKET" => "Добавит в корзину",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_LAZY_LOAD" => "Показать ещё",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "META_DESCRIPTION" => "-",
            "META_KEYWORDS" => "-",
            "OFFERS_CART_PROPERTIES" => array(
            ),
            "OFFERS_FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "OFFERS_PROPERTY_CODE" => array(
                0 => "TSVET",
                1 => "RAZMERY",
                2 => "",
            ),
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "desc",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
            "OFFER_TREE_PROPS" => array(
                0 => "TSVET",
                1 => "RAZMERY",
            ),
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "round",
            "PAGER_TITLE" => "Товары",
            "PAGE_ELEMENT_COUNT" => "4",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                0 => "retail",
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPERTIES" => array(
            ),
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "5",
            "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false}]",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "PROPERTY_CODE" => array(
                0 => "BREND",
                1 => "TIP_MODELI",
                2 => "",
            ),
            "PROPERTY_CODE_MOBILE" => array(
            ),
            "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
            "RCM_TYPE" => "personal",
            "SECTION_CODE" => "",
            "SECTION_ID" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SEF_MODE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SHOW_ALL_WO_SECTION" => "Y",
            "SHOW_CLOSE_POPUP" => "Y",
            "SHOW_DISCOUNT_PERCENT" => "Y",
            "SHOW_FROM_SECTION" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_OLD_PRICE" => "Y",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "N",
            "SLIDER_INTERVAL" => "0",
            "SLIDER_PROGRESS" => "N",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "COMPONENT_TEMPLATE" => ".default"
        ),
        false
    );?>
<?
}
?>