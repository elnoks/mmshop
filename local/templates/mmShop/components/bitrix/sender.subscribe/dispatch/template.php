<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$buttonId = $this->randString();
?>
<span id="sender-subscribe">
    <?
    $frame = $this->createFrame("sender-subscribe", false)->begin();
    ?>
    <div class="f_header"><?= GetMessage("subscr_form_title") ?></div>
    <? if (isset($arResult['MESSAGE'])):
        ?>

        <div id="<?= ($arResult['MESSAGE']['TYPE'] == 'ERROR' ? 'modal_failed' : 'modal_success') ?>">

            <div class="modal-header" style="height: 20px"> </div>

            <div class="modal_img_container">
                <img src="<?= ($this->GetFolder() . '/images/' . ($arResult['MESSAGE']['TYPE'] == 'ERROR' ? 'failed.png' : 'success.png')) ?>" alt="">
            </div>
            <div class="modal_subheader"><?= GetMessage('subscr_form_response_' . $arResult['MESSAGE']['TYPE']) ?></div>
            <p class="modal_text" style="color: #777777"><?= htmlspecialcharsbx($arResult['MESSAGE']['TEXT']) ?></p>
        </div>
        <script>
            BX.ready(function () {
                var oPopup = BX.PopupWindowManager.create("<?=($arResult['MESSAGE']['TYPE'] == 'ERROR' ? 'modal_failed' : 'modal_success')?>", window.body, {
                    autoHide: true,
                    className: 'modal-dialog',
                    offsetTop: -100,
                    offsetLeft: 0,
                    lightShadow: true,
                    closeIcon: true,
                    closeByEsc: true,
                    closeIcon: {
                        right: "-38px",
                        top: "0",
                        backgroundImage: "url(<?=$this->GetFolder() . '/images/white_cross.png'?>)",
                        backgroundRepeat : "no-repeat",
                    },
                    overlay: {
                        backgroundColor: 'rgba(57,60,67,0.82)', opacity: '80'
                    }
                });
                oPopup.setContent(BX("<?=($arResult['MESSAGE']['TYPE'] == 'ERROR' ? 'modal_failed' : 'modal_success')?>"));
                oPopup.show();
            });
		</script>
    <? endif; ?>

    <script>
		(function () {
            var btn = BX('bx_subscribe_btn_<?=$buttonId?>');
            var form = BX('bx_subscribe_subform_<?=$buttonId?>');

            if (!btn) {
                return;
            }

            BX.bind(form, 'submit', function () {
                btn.disabled = true;
                setTimeout(function () {
                    btn.disabled = false;
                }, 2000);

                return true;
            });
        })();
	</script>
	<form id="bx_subscribe_subform_<?= $buttonId ?>" class="subscribe_form" role="form" method="post"
          action="<?= $arResult["FORM_ACTION"] ?>">
		<?= bitrix_sessid_post() ?>
        <input type="hidden" name="sender_subscription" value="add">
        <input type="text" name="SENDER_SUBSCRIBE_EMAIL" value="<?= $arResult["EMAIL"] ?>"
               title="<?= GetMessage("subscr_form_email_title") ?>"
               placeholder="<?= htmlspecialcharsbx(GetMessage('subscr_form_email_title')) ?>" required>
        <input id="bx_subscribe_btn_<?= $buttonId ?>" type="submit" value="<?= GetMessage("subscr_form_button") ?>">

        <? if ($arParams['USER_CONSENT'] == 'Y'): ?>
            <div class="bx_subscribe_checkbox_container bx-sender-subscribe-agreement">
			<? $APPLICATION->IncludeComponent(
                "bitrix:main.userconsent.request",
                "",
                array(
                    "ID" => $arParams["USER_CONSENT_ID"],
                    "IS_CHECKED" => $arParams["USER_CONSENT_IS_CHECKED"],
                    "AUTO_SAVE" => "Y",
                    "IS_LOADED" => $arParams["USER_CONSENT_IS_LOADED"],
                    "ORIGIN_ID" => "sender/sub",
                    "ORIGINATOR_ID" => "",
                    "REPLACE" => array(
                        "button_caption" => GetMessage("subscr_form_button"),
                        "fields" => array(GetMessage("subscr_form_email_title"))
                    ),
                )
            ); ?>
		    </div>
        <? endif; ?>
	</form>
</span>