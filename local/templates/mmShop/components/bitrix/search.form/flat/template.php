<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true); ?>

<div class="head_control_item search_key">
    <a href="javascript:void(0);" id="search_link">
        <div class="head_control_item_ico head_control_item_ico_4" id="search_link_click"></div>
        <div class="head_control_item_label">
            <?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>
        </div>
    </a>
    <div class="access_menu">
        <form action="<?= $arResult["FORM_ACTION"] ?>">
            <input type="text" name="q" size="15" maxlength="50" placeholder="<?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>"
                   name="search">
            <input name="s" type="submit" value="<?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>" class="btn_orange">
        </form>
    </div>
</div>
<script>
    BX.bind(BX('search_link_click'), 'click', function() {
        if (BX.hasClass("search_link", 'bx-opener'))
            BX.removeClass(BX('search_link'), 'bx-opener');
        else
            BX.addClass(BX('search_link'), 'bx-opener');
    })
    BX.bind(BX('overlay'), 'click', function() {
        BX.removeClass(BX('search_link'), 'bx-opener');
    })
</script>