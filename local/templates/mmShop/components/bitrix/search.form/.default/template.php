<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="mobile_search_cont">
<form action="<?=$arResult["FORM_ACTION"]?>">
    <input type="text" name="q"  class='mobile_search' value="" size="15" maxlength="50" placeholder="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>"/>
    <i></i>
    <input type="submit" value="" placeholder="0">
    <input type="hidden" value="send">
</form>
</div>