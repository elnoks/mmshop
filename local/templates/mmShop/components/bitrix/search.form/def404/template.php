<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true); ?>
<div class="search-form">
    <form action="<?= $arResult["FORM_ACTION"] ?>">
        <div class="inputForm">
            <i class="ico_find"></i>
            <input class="find404" type="text" name="q" size="15" maxlength="50" placeholder="<?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>"
                   name="search">
            <input name="s" type="hidden" value="<?= GetMessage("BSF_T_SEARCH_BUTTON"); ?>" class="btn_orange">
        </div>
    </form>
</div>
