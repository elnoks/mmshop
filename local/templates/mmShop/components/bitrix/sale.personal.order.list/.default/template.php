<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL'])) {
    foreach ($arResult['ERRORS']['FATAL'] as $error) {
        ShowError($error);
    }
    $component = $this->__component;
    if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED])) {
        $APPLICATION->AuthForm('', false, false, 'N', false);
    }

} else {
    $price = 0;
    foreach ($arResult['ORDERS'] as $order)
        $price = $price + $order["ORDER"]["PRICE"];

    $discount = "0%";
    if ($price > 10000 && $price < 15000)
        $discount = "3%";
    elseif($price >= 15000 && $price < 20000)
        $discount = "4%";
    elseif($price > 20000 && $price < 30000)
        $discount = "5%";
    elseif($price > 30000 && $price < 40000)
        $discount = "6%";
    elseif($price > 40000 && $price < 50000)
        $discount = "7%";
    elseif($price > 50000 && $price < 75000)
        $discount = "8%";
    elseif($price > 75000 && $price < 100000)
        $discount = "9%";
    elseif($price > 100000)
        $discount = "10%";

    $this->SetViewTarget("globalDiscount");
        echo $discount;
    $this->EndViewTarget("globalDiscount");

    $this->SetViewTarget("globalBuy");
        echo CurrencyFormat($price, "RUB");
    $this->EndViewTarget("globalBuy");
}
?>
