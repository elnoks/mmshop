<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
    die();
}
?>
<div class="auth-dialog head_control_item">
    <h4><?=GetMessage("AUTH_GET_CHECK_STRING")?></h4>
        <?
        if(!empty($arParams["~AUTH_RESULT"])):
            $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
            ?>
            <div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div>
        <?endif?>

        <p class="bx-authform-content-container"><?=GetMessage("AUTH_FORGOT_PASSWORD_1")?></p>
        <form class="login_form" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
            <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
            <?endif?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">
            <div class="inputForm">
                <label for='USER_LOGIN'><?echo GetMessage("AUTH_LOGIN_EMAIL")?></label>
                <i class="ico_man"></i>
                <input type="text" name="USER_LOGIN" id="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" />
            </div>

            <i class="ico_email"></i>
            <input type="hidden" name="USER_EMAIL" />
            <?if ($arResult["USE_CAPTCHA"]):?>
                <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="100%" height="40" alt="CAPTCHA" />
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                    </div>
                </div>
            <?endif?>
            <input type="submit" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>" />
            <div class="registration_p">
                <a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a>
            </div>
        </form>
</div>

<script type="text/javascript">
    document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
    document.bform.USER_LOGIN.focus();
</script>
