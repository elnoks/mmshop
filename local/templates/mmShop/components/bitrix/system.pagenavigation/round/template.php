<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

?>


<ul class='pagination'>
    <?
    if ($arResult["bDescPageNumbering"] === true): ?>

        <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
            <li class="page_first"><a href='<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1'></a></li>
            <? if ($arResult["bSavePage"]): ?>
                <li class="page_prev"><a
                            href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
                </li>
                <li>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">1</a>
                </li>
            <? else: ?>
                <? if (($arResult["NavPageNomer"] + 1) == $arResult["NavPageCount"]): ?>
                    <li class="page_prev"><a
                                href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
                    </li>
                <? else: ?>
                    <li class="page_prev">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                            
                        </a>
                    </li>
                <? endif ?>
                <li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
            <? endif ?>
        <? else: ?>
            <li class="page_first"><a href="javascript:void(0)"></a></li>
            <li class="page_prev"><a href="javascript:void(0)"></a></li>
            <li class="current"><a href="javascript:void(0)">1</a></li>
        <? endif ?>

        <?
        $arResult["nStartPage"]--;
        while ($arResult["nStartPage"] >= $arResult["nEndPage"] + 1):
            ?>
            <? $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1; ?>

            <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <li class="current"><<a href="javascript:void(0)"><?= $NavRecordGroupPrint ?></a></li>
        <? else:?>
            <li>
                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $NavRecordGroupPrint ?></a>
            </li>
        <? endif ?>

            <? $arResult["nStartPage"]-- ?>
        <? endwhile ?>

        <? if ($arResult["NavPageNomer"] > 1): ?>
            <? if ($arResult["NavPageCount"] > 1): ?>
                <li>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a>
                </li>
            <? endif ?>
            <li class="page_next"><a
                        href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
            </li>
            <li class="page_last"><a href='<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?=$arResult["nEndPage"]?>'></a></li>
        <? else: ?>
            <? if ($arResult["NavPageCount"] > 1): ?>
                <li class="current"><a href="javascript:void(0)"><?= $arResult["NavPageCount"] ?></a></li>
            <? endif ?>
            <li class="page_next"><a href="javascript:void(0)"></a></li>
            <li class="page_last"><a href="javascript:void(0)"></a></li>
        <? endif ?>

    <? else: ?>

        <? if ($arResult["NavPageNomer"] > 1): ?>
            <li class="page_first"><a href='<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1'></a></li>
            <? if ($arResult["bSavePage"]): ?>
                <li class="page_prev"><a
                            href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
                </li>
                <li>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
                </li>
            <? else: ?>
                <? if ($arResult["NavPageNomer"] > 2): ?>
                    <li class="page_prev"><a
                                href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
                    </li>
                <? else: ?>
                    <li class="page_prev"><a
                                href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
                    </li>
                <? endif ?>
                <li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a></li>
            <? endif ?>
        <? else: ?>
            <li class="page_first"><a href="javascript:void(0)"></a></li>
            <li class="page_prev"><a href="javascript:void(0)"></a></li>
            <li class="current"><a href="javascript:void(0)">1</a></li>
        <? endif ?>

        <?
        $arResult["nStartPage"]++;
        while ($arResult["nStartPage"] <= $arResult["nEndPage"] - 1):
            ?>
            <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
            <li class="current"><a href="javascript:void(0)"><?= $arResult["nStartPage"] ?></a></li>
        <? else:?>
            <li>
                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
            </li>
        <? endif ?>
            <? $arResult["nStartPage"]++ ?>
        <? endwhile ?>

        <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
            <? if ($arResult["NavPageCount"] > 1): ?>
                <li>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
                </li>
            <? endif ?>
            <li class="page_next"><a
                        href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
            </li>
            <li class="page_last"><a href='<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?=$arResult["nEndPage"]?>'></a></li>
        <? else: ?>
            <? if ($arResult["NavPageCount"] > 1): ?>
                <li class="current"><a href="javascript:void(0)"><?= $arResult["NavPageCount"] ?></a></li>
            <? endif ?>
            <li class="page_next"><a href="javascript:void(0)"></a></li>
            <li class="page_last"><a href="javascript:void(0)"></a></li>
        <? endif ?>

    <? endif ?>

</ul>
