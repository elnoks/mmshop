<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
    return;
?>
<ul>
    <li>
        <a href="<?= $arParams["LINK_NEW"] ?>"><?= GetMessage("NEW") ?> <i class="label label-orange">NEW</i></a>
    </li>
    <? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>     <!-- first level-->
        <? $existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false; ?>
        <li <? if (is_array($arColumns) && count($arColumns) > 0){ ?>class="dropdown"<? } ?>>
            <a href="<?= count($arColumns) > 0 ? "javascript:void(0)" : $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?></a>
            <? if (is_array($arColumns) && count($arColumns) > 0): ?>
                <? foreach ($arColumns as $key => $arRow): ?>
                    <ul>
                        <? foreach ($arRow as $itemIdLevel_2 => $arLevel_3): ?>  <!-- second level-->
                            <li <? if (is_array($arLevel_3) && count($arLevel_3) > 0){ ?>class="dropdown"<? } ?>>
                                <a href="<?= count($arLevel_3) > 0 ? "javascript:void(0)" : $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>">
                                    <?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] ?>
                                </a>
                                <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?>
                                    <ul class="sub_menu">
                                        <? foreach ($arLevel_3 as $itemIdLevel_3): ?>    <!-- third level-->
                                            <li>
                                                <a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"] ?>">
                                                    <?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"] ?>
                                                </a>
                                            </li>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif ?>
                            </li>
                        <? endforeach; ?>
                    </ul>
                <? endforeach; ?>
            <? endif ?>
        </li>
    <? endforeach; ?>
    <li>
        <a href="<?= $arParams["LINK_SALE"] ?>"><?= GetMessage("SALE") ?> <i class="label label-red label-percent">%</i></a>
    </li>
    <li>
        <div class="mobile_control_menu">

            <?php
            $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.line",
                "def",
                array(
                    "MOBILE_MODE" => "Y",
                    "COMPONENT_TEMPLATE" => "fav",
                    "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                    "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                    "SHOW_NUM_PRODUCTS" => "Y",
                    "SHOW_TOTAL_PRICE" => "N",
                    "SHOW_EMPTY_VALUES" => "N",
                    "SHOW_PERSONAL_LINK" => "N",
                    "PATH_TO_PERSONAL" => SITE_DIR . "personal/",
                    "SHOW_AUTHOR" => "N",
                    "PATH_TO_AUTHORIZE" => "",
                    "SHOW_REGISTRATION" => "N",
                    "PATH_TO_REGISTER" => SITE_DIR . "auth/?register=yes",
                    "PATH_TO_PROFILE" => SITE_DIR . "personal/",
                    "SHOW_PRODUCTS" => "Y",
                    "SHOW_DELAY" => "Y",
                    "SHOW_NOTAVAIL" => "N",
                    "SHOW_IMAGE" => "Y",
                    "SHOW_PRICE" => "Y",
                    "SHOW_SUMMARY" => "Y",
                    "POSITION_FIXED" => "N",
                    "HIDE_ON_BASKET_PAGES" => "N",
                    "POSITION_HORIZONTAL" => "right",
                    "POSITION_VERTICAL" => "top"
                ),
                false
            ); ?>

            <? if ($USER->IsAuthorized()) {
                $name = trim($USER->GetFullName());
                if (!$name)
                    $name = trim($USER->GetLogin());
                if (strlen($name) > 15)
                    $name = substr($name, 0, 13) . '...';
                ?>
                <a href='/profile/' class="mobile_login"><i></i><?= $name ?></a>
                <a href='/personal/orders/' class="mobile_orders"><i></i>
                    <?= GetMessage("ORDERS") ?></a>
                <a href='?logout=yes' class="mobile_logout"><i></i><?= GetMessage("EXIT") ?></a>
                <?
            } else {
                ?>
                <a href='/auth/?login=yes' class="mobile_login"><i></i><?= GetMessage("ENTER"); ?></a>
                <?
            }
            ?>

        </div>
    </li>
</ul>
