<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
    echo $arResult['ERROR'];
    return false;
}
?>
<div class="manufact_slider">
    <ul>
        <?
        foreach ($arResult['rows'] as $row){
            if (!empty($row["UF_FILE"])){
                ?>
                <li>
                    <a style="color: #666" href="<?=$row["UF_LINK"]?>">
                        <div class="ms_i_cont">
                            <?=$row["UF_FILE"]?>
                        </div>
                        <div class="ms_i_title lk_grey_btn"><?=$row["UF_NAME"]?></div>
                    </a>
                </li>
                <?
            }
        }
        ?>
    </ul>
</div>