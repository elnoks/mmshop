<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}
?>
<div class="row brand-description">
    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-9">
    <?
    foreach ($arResult['rows'] as $row){
        ?>
        <p>
            <?=htmlspecialcharsBack($row["UF_DESCRIPTION"])?>
        </p>
    </div>
    <div class="col-lg-offset-4 col-md-offset-4 hidden-xs col-sm-3 col-lg-2 col-md-2 col-sm-6 brand-logo-row">
        <?=$row["UF_FILE"]?>
    </div>
    <?
    }
    ?>
</div>