<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];
$TSB1_YOUR_CART = $arParams["SHOW_DELAY"] == 'Y' ? "TSB1_YOUR_CART_FAV" : "TSB1_YOUR_CART";

require(realpath(dirname(__FILE__)) . '/top_template.php');

$arFilter = array(
    "TYPE" => "1c_catalog",
    "SITE_ID" => SITE_ID,
);

$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
$dbIBlock = new CIBlockResult($dbIBlock);
if ($arIBlock = $dbIBlock->GetNext())
    $id = $arIBlock["ID"];


if($arParams["SHOW_DELAY"] == "Y")
    $num = count($arResult["CATEGORIES"]["DELAY"]);
else
    $num = $arResult['NUM_PRODUCTS'];


//$arResult['NUM_PRODUCTS'] > 3
?>
<div class="access_menu" id="<?= $cartId ?>list">
    <h4><?= Loc::getMessage($TSB1_YOUR_CART); ?></h4>
    <?
    if ($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY']))) {
        if ($arParams["POSITION_FIXED"] == "Y"):?>
            <span id="<?= $cartId ?>status" style="display:none;"></span>
        <? endif ?>
        <?
        if ($arParams["SHOW_DELAY"] == "Y")
            unset($arResult["CATEGORIES"]["READY"]);

        foreach ($arResult["CATEGORIES"] as $category => $items) {
            if (empty($items))
                continue;
            ?>
            <div id="<?=$arParams["SHOW_DELAY"] == "Y" ? "fav":"cart"?>" class="favorite_container <?=$num > 3 ? "mCustomScrollbar":"defaultClass"?>">
                <ul id="<?= $cartId ?>products">
                    <?
                    foreach ($items as $v) {
                        ?>
                        <li>
                            <div class="f_cell_left">
                                <?
                                $mxResult = CCatalogSku::GetProductInfo($v["PRODUCT_ID"]);
                                if (is_array($mxResult)) {
                                    $elid = $mxResult["ID"];
                                    $id = $mxResult["OFFER_IBLOCK_ID"];
                                }else{
                                    $morePics = array();
                                    $res = CIBlockElement::GetProperty($id, $v["PRODUCT_ID"], "sort", "asc", array("CODE" => "MORE_PHOTO"));
                                    if ($ob = $res->GetNext()) {
                                        $morePics[] = $ob['VALUE'];
                                    }
                                }
                                TrimArr($morePics);
                                if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]) {
                                    $img = $v["PICTURE_SRC"];
                                } elseif (empty($v["PICTURE_SRC"]) && $elid > 0) {
                                    $res = CIBlockElement::GetByID($elid);
                                    if ($ar_res = $res->GetNext()) {
                                        if ($ar_res["DETAIL_PICTURE"] > 0)
                                            $img = CFile::GetPath($ar_res["DETAIL_PICTURE"]);
                                        elseif ($ar_res["PREVIEW_PICTURE"] > 0)
                                            $img = CFile::GetPath($ar_res["PREVIEW_PICTURE"]);
                                        else
                                            $img = $templateFolder . "/images/no_photo.png";
                                    }
                                } elseif(!empty($morePics)) {
                                    $img = CFile::GetPath($morePics[0]);
                                }else{
                                    $img = $templateFolder . "/images/no_photo.png";
                                }

                                if ($v["DETAIL_PAGE_URL"]) {
                                    ?>
                                    <a href="<?= $v["DETAIL_PAGE_URL"] ?>">
                                        <img src="<?= $img ?>" alt="<?= $v["NAME"] ?>">
                                    </a>
                                    <?
                                } else {
                                    ?>
                                    <img src="<?= $img ?>" alt="<?= $v["NAME"] ?>"/>
                                    <?
                                } ?>
                            </div>
                            <div class="f_cell_right">
                                <div class="f_title">
                                    <? if ($v["DETAIL_PAGE_URL"]): ?>
                                        <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><?= $v["NAME"] ?></a>
                                    <? else: ?>
                                        <?= $v["NAME"] ?>
                                    <? endif ?>
                                </div>
                                <?php
                                $iterator = array("f_color" => "TSVET", "f_size" => "RAZMERY");
                                foreach ($iterator as $class => $propName) {
                                    $res = CIBlockElement::GetProperty($id, $v["PRODUCT_ID"], "sort", "asc", array("CODE" => $propName));
                                    if ($ob = $res->GetNext()) {
                                        $displayValue = CIBlockFormatProperties::GetDisplayValue($v["PRODUCT_ID"], $ob, false);
                                        if (!empty($displayValue["DISPLAY_VALUE"]))
                                            echo "<div class='" . $class . "'>" . $displayValue["NAME"] . ": " . $displayValue["DISPLAY_VALUE"]."</div>";
                                    }
                                }

                                $mxResult = CCatalogSku::GetProductInfo($v["PRODUCT_ID"]);
                                if (is_array($mxResult))
                                    $favorEl = $mxResult['ID'];
                                else
                                    $favorEl = $v["PRODUCT_ID"];


                                ?>
                                <div class="f_color">
                                    <a href="javascript:void(0)" class="f_remove"
                                       data-fav="<?= $favorEl ?>"
                                       id="<?= $v["ID"]."del"?>"
                                       onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)"
                                       title="<?= Loc::getMessage("TSB1_DELETE") ?>"></a>
                                </div>
                                <div class="f_price">
                                    <?
                                    if ($v["FULL_PRICE"] != $v["PRICE_FMT"]) { ?>
                                        <div class="f_old_price"><?= $v["FULL_PRICE"] ?></div><?= $v["PRICE_FMT"] ?>
                                    <? } else{
                                        echo $v["FULL_PRICE"];
                                    }?>
                                </div>
                                <?php
                                if ($arParams["SHOW_DELAY"] == 'Y') {
                                    $mxResult = CCatalogSku::GetProductInfo($v["PRODUCT_ID"]);
                                    if (is_array($mxResult))
                                        $elid = $mxResult['ID'];
                                    ?>
                                    <a href="javascript:void(0)"
                                       class="f_action btn_orange"
                                       data-elIdFavdel = "<?=$elid?>"
                                       onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>);openCallForm('/', 'favToCart', '<?= $v["PRODUCT_ID"] ?>', true)">
                                        <?= Loc::getMessage("TSB1_CART_FAV_TO_CART") ?>
                                    </a>
                                    <?
                                }
                                ?>
                            </div>
                        </li>
                        <?
                        unset($elid);
                    }
                    ?>
                </ul>
            </div>
            <?
        }
        ?>

        <? if (($arResult['NUM_PRODUCTS'] > 0 || $arParams['SHOW_EMPTY_VALUES'] == 'Y') && $arParams["SHOW_DELAY"] != 'Y') { ?>
            <div class="cart_statistic">
                <div class="items_count">
                    <?
                    $ends = BasketNumberWordEndings($arResult['NUM_PRODUCTS'], "ru");
                    echo Loc::getMessage("TSB1_PRODUCT") . $ends . ': ' . $arResult['NUM_PRODUCTS'];
                    ?>
                </div>
                <div class="items_sum"><?= Loc::getMessage('TSB1_TOTAL_PRICE') ?>: <?= $arResult['TOTAL_PRICE'] ?></div>
            </div>
            <div class="order_link">
                <a href="<?= $arParams["PATH_TO_ORDER"] ?>"
                   class="f_action btn_orange"><?= Loc::getMessage("TSB1_2ORDER") ?></a>
            </div>
            <?
        }
    } else {
        ?>
        <div class="ifHide">
            <ul id="<?= $cartId ?>products"></ul>
            <div class="favorite_container emptyCart">
                <div class="f_title"><?= Loc::getMessage("TSB1_EMPTY") ?></div>
            </div>

            <div class="order_link">
                <a href="/catalog/" class="f_action btn_orange"><?= Loc::getMessage("TSB1_CATALOG") ?></a>
            </div>
        </div>
        <?
    }

    if ($arParams["SHOW_DELAY"] == 'Y' && empty($arResult['CATEGORIES']['DELAY'])) {
        ?>
        <ul id="<?= $cartId ?>products"></ul>
        <div class="favorite_container emptyCart">
            <div class="f_title"><?= Loc::getMessage("TSB1_EMPTY_FAV") ?></div>
        </div>

        <div class="order_link">
            <a href="/catalog/" class="f_action btn_orange TSB1_EMPTY_FAV"><?= Loc::getMessage("TSB1_CATALOG") ?></a>
        </div>
        <?
    }
    ?>
</div>