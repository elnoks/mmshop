<?
$MESS ['TSB1_YOUR_CART'] = "Ваша корзина";
$MESS ['TSB1_CART'] = "Корзина";
$MESS ['TSB1_TOTAL_PRICE'] = "Сумма";
$MESS ['TSB1_PERSONAL'] = "Персональный раздел";
$MESS ['TSB1_LOGIN'] = "Войти";
$MESS ['TSB1_LOGOUT'] = "Выйти";
$MESS ['TSB1_REGISTER'] = "Регистрация";

$MESS ['TSB1_READY'] = "Готовые к покупке товары";
$MESS ['TSB1_DELAY'] = "Отложенные товары";
$MESS ['TSB1_NOTAVAIL'] = "Недоступные товары";
$MESS ['TSB1_SUBSCRIBE'] = "Подписанные товары";

$MESS ['TSB1_SUM'] = "Сумма";
$MESS ['TSB1_DELETE'] = "Удалить";

$MESS ['TSB1_2ORDER'] = "Оформить заказ";

$MESS ['TSB1_EXPAND'] = "Раскрыть";
$MESS ['TSB1_COLLAPSE'] = "Скрыть";

$MESS ['TSB1_PRODUCT'] = "Товар";
$MESS ['TSB1_EMPTY'] = "Ваша корзина пуста";
$MESS ['TSB1_CATALOG'] = "Перейти в каталог";
$MESS ['TSB1_EMPTY_FAV'] = "У вас нет избранных товаров";
$MESS ['TSB1_YOUR_CART_FAV'] = "Избранные";
$MESS ['TSB1_YOUR_CART_FAV_MOBILE'] = "Избранное";
$MESS ['TSB1_CART_FAV'] = "Избранные";
$MESS ['TSB1_CART_FAV_TO_CART'] = "Добавить в корзину";