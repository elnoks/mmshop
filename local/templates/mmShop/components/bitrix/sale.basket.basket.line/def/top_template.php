<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
$delay = false;
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
if ($arParams["SHOW_DELAY"] == 'Y') {
    $delay = true;
    $countProd = count($arResult["CATEGORIES"]["DELAY"]);
    $TSB1_CART = "TSB1_CART_FAV";
    $style = "head_control_item_ico_2";
    $idel = "favorite_link";
}else{
    $countProd = $arResult['NUM_PRODUCTS'];
    $TSB1_CART = "TSB1_CART";
    $style = "head_control_item_ico_3";
    $idel = "cart_link";
}
if (!$compositeStub) {
    if ($arParams["MOBILE_MODE"] == "Y"){
        ?>
            <a href='/personal/favorites/' class="mobile_favorite"><i></i>
                <div class="label-orange"><?= $arResult['NUM_PRODUCTS'] ?></div>
                <?= GetMessage("TSB1_YOUR_CART_FAV_MOBILE") ?></a>
        <?
    }else {
        if (!$delay) {
            ?>
            <a href="/personal/cart/" id="cart_link_mobile">
                <div class="head_control_item_ico head_control_item_ico_3">
                    <i class="label label-orange"><?= $arResult['NUM_PRODUCTS'] ?></i>
                </div>
                <div class="head_control_item_label">
                    <?= Loc::getMessage("TSB1_CART"); ?>
                </div>
            </a>
            <?php
        }
        ?>
        <a href="javascript:void(0)" id="<?= $idel ?>" onclick="<?= $cartId ?>.toggleOpenCloseCart()">
            <div class="head_control_item_ico <?= $style ?>">
                <i class="label label-orange"><?= $countProd ?></i>
            </div>
            <div class="head_control_item_label">
                <?= Loc::getMessage($TSB1_CART); ?>
            </div>
        </a>
        <?
    }
}
?>