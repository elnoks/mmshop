'use strict';

function BitrixSmallCart(){}

BitrixSmallCart.prototype = {

	activate: function ()
	{
		this.cartElement = BX(this.cartId);
		this.setCartBodyClosure = this.closure('setCartBody');
		BX.addCustomEvent(window, 'OnBasketChange', this.closure('refreshCart', {}));
	},

	closure: function (fname, data)
	{

		var obj = this;
		return data
			? function(){obj[fname](data)}
			: function(arg1){obj[fname](arg1)};
	},

	toggleOpenCloseCart: function ()
	{
        var field = BX.findParent(BX(this.cartId), {class: 'head_controls'}, true, true);
        var childs = BX.findChild(BX(field), {class: 'access_menu'}, true, true);

		if (childs.length > 0) {
            childs.forEach(function (element) {
                if (!element.getAttribute('id')) {
                    element.setAttribute("style", "display:none");
                }
            });
        }

        if (this.arParams.SHOW_DELAY == "Y") {
            var fav = BX.findParent(BX("cart_link"), {id: this.cartId}, true, true);
            if(BX.hasClass(BX(this.cartId), "bx-closed"))
            {
                if(BX.hasClass(fav, "bx-opener")){
                    BX.toggleClass(fav, ["bx-closed", "bx-opener"]);
                }
                BX.toggleClass(this.cartElement,  ["bx-closed", "bx-opener"]);
                BX.removeClass('overlay', 'hidden');
            }else{
                BX.toggleClass(this.cartElement,  ["bx-closed", "bx-opener"]);
                BX.addClass('overlay', 'hidden');
            }
        }else {
            var fav = BX.findParent(BX("favorite_link"), {id: this.cartId}, true, true);
            if(BX.hasClass(BX(this.cartId), "bx-closed"))
            {
            	if(BX.hasClass(fav, "bx-opener")){
                    BX.toggleClass(fav, ["bx-closed", "bx-opener"]);
				}
                BX.removeClass('overlay', 'hidden');
                BX.toggleClass(this.cartElement,  ["bx-closed", "bx-opener"]);
            }else{
                BX.toggleClass(this.cartElement,  ["bx-closed", "bx-opener"]);
                BX.addClass('overlay', 'hidden');
			}
        }
        BX(this.cartId + 'list').removeAttribute("style");
	},

	refreshCart: function (data)
	{
		if (this.itemRemoved)
		{
			this.itemRemoved = false;
			return;
		}

		data.sessid = BX.bitrix_sessid();
		data.siteId = this.siteId;
		data.templateName = this.templateName;
		data.arParams = this.arParams;
		BX.ajax({
			url: this.ajaxPath,
			method: 'POST',
			dataType: 'html',
			data: data,
			onsuccess: this.setCartBodyClosure
		});
	},

	setCartBody: function (result)
	{
		if (this.cartElement)
			this.cartElement.innerHTML = result.replace(/#CURRENT_URL#/g, this.currentUrl);
	},

	removeItemFromCart: function (id)
	{
        var product_id = BX(id + 'del').getAttribute('data-fav');
		this.refreshCart ({sbblRemoveItemFromCart: id});
        if(BX(product_id + 'favoriteEl')) {
            BX.adjust(BX(product_id + 'favoriteEl') , {style: {'display': 'none'}});
        }
        if(BX(product_id + 'favorite')) {
            BX.removeClass(product_id + 'favorite', 'active');
        }else{

		}
		this.itemRemoved = true;
		BX.onCustomEvent('OnBasketChange');
	}
};
BX.addCustomEvent('onAjaxSuccess', function() {
    $(".mCustomScrollbar").mCustomScrollbar();
});