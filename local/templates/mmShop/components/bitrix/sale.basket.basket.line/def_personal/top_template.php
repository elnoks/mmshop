<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if ($arParams["SHOW_DELAY"] == 'Y') {
    $countProd = count($arResult["CATEGORIES"]["DELAY"]);
}

$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');

echo "<h2>".Loc::getMessage("TSB1_YOUR_CART_FAV")."</h2>";
echo "<p>".Loc::getMessage("TSB1_YOUR_CART_FAV_TEXT")."</p>";
if (!$compositeStub && $countProd > 0) {
 echo "<h3>".Loc::getMessage("TSB1_YOUR_COUNT_FAV", array("#NUM#" => $countProd))."</h3>";
}
?>

