<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;

$this->IncludeLangFile('template.php');

$cartId = $arParams['cartId'];
$TSB1_YOUR_CART = $arParams["SHOW_DELAY"] == 'Y' ? "TSB1_YOUR_CART_FAV" : "TSB1_YOUR_CART";

require(realpath(dirname(__FILE__)) . '/top_template.php');

$arFilter = array(
    "TYPE" => "1c_catalog",
    "SITE_ID" => SITE_ID,
);

$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
$dbIBlock = new CIBlockResult($dbIBlock);
if ($arIBlock = $dbIBlock->GetNext())
    $id = $arIBlock["ID"];
?>
<span id="<?= $cartId ?>list">
    <?
    if ($arParams["SHOW_PRODUCTS"] == "Y" && ($arResult['NUM_PRODUCTS'] > 0 || !empty($arResult['CATEGORIES']['DELAY']))) {
        if ($arParams["POSITION_FIXED"] == "Y"):?>
            <span id="<?= $cartId ?>status" style="display:none;"></span>
        <? endif ?>
        <?
        if ($arParams["SHOW_DELAY"] == "Y")
            unset($arResult["CATEGORIES"]["READY"]);

        foreach ($arResult["CATEGORIES"] as $category => $items) {
            if (empty($items))
                continue;
            ?>
            <div class="box-for-favorites" id="<?= $cartId ?>products">
                <?
                foreach ($items as $key => $v) {
                    if ($key == 7) break;
                    ?>
                    <div class="cab-favorite-item flex-display">
                        <div class="fav-image-wrap">
                            <?
                            $mxResult = CCatalogSku::GetProductInfo($v["PRODUCT_ID"]);
                            if (is_array($mxResult)) {
                                $elid = $mxResult["ID"];
                                $id = $mxResult["OFFER_IBLOCK_ID"];
                            } else {
                                $morePics = array();
                                $res = CIBlockElement::GetProperty($id, $v["PRODUCT_ID"], "sort", "asc", array("CODE" => "MORE_PHOTO"));
                                if ($ob = $res->GetNext()) {
                                    $morePics[] = $ob['VALUE'];
                                }
                            }
                            TrimArr($morePics);
                            if ($arParams["SHOW_IMAGE"] == "Y" && $v["PICTURE_SRC"]) {
                                $img = $v["PICTURE_SRC"];
                            } elseif (empty($v["PICTURE_SRC"]) && $elid > 0) {
                                $res = CIBlockElement::GetByID($elid);
                                if ($ar_res = $res->GetNext()) {
                                    if ($ar_res["DETAIL_PICTURE"] > 0)
                                        $img = CFile::GetPath($ar_res["DETAIL_PICTURE"]);
                                    elseif ($ar_res["PREVIEW_PICTURE"] > 0)
                                        $img = CFile::GetPath($ar_res["PREVIEW_PICTURE"]);
                                    else
                                        $img = $templateFolder . "/images/no_photo.png";
                                }
                            } elseif (!empty($morePics)) {
                                $img = CFile::GetPath($morePics[0]);
                            } else {
                                $img = $templateFolder . "/images/no_photo.png";
                            }

                            if ($v["DETAIL_PAGE_URL"]) {
                                ?>
                                <a href="<?= $v["DETAIL_PAGE_URL"] ?>"><img src="<?= $img ?>" alt="<?= $v["NAME"] ?>"></a>
                                <?
                            } else {
                                ?>
                                <a href="javascript:void(0)"><img src="<?= $img ?>" alt="<?= $v["NAME"] ?>"/></a>
                                <?
                            } ?>
                        </div>
                        <div class="fav-item-info">
                            <p>
                                <?
                                if ($v["DETAIL_PAGE_URL"]){ ?>
                                    <a class="cab-fav-link" href="<?= $v["DETAIL_PAGE_URL"] ?>"><?= $v["NAME"] ?></a>
                                <? }else{ ?>
                                    <a class="cab-fav-link" href="javascript:void(0)"><?= $v["NAME"] ?></a>
                                <? } ?>

                                <span class="fav-price">
                                    <? if ($v["FULL_PRICE"] != $v["PRICE_FMT"]) { ?>
                                    <strike><?= $v["FULL_PRICE"] ?></strike><?= $v["PRICE_FMT"] ?>
                                    <? }else{
                                        echo $v["FULL_PRICE"];
                                    } ?>
                                </span>

                                <em>
                                    <a href="javascript:void(0)" class="f_remove"
                                       data-fav="<?= $v["PRODUCT_ID"] ?>"
                                       id="<?= $v["ID"] . "del" ?>"
                                       onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)"
                                       title="<?= Loc::getMessage("TSB1_DELETE") ?>">
                                        <img src="<?=$this->__folder?>/images/icon-del-sm.png">
                                    </a>
                                </em>
                            </p>
                            <p class="cab-fav-details">
                                <span>
                                    <?php
                                    echo Loc::getMessage("TSB1_EMPTY_FAV_MR", array("#INC#" => $v["QUANTITY"]))
                                    ?>
                                </span>
                                <?php
                                $iterator = array("f_color" => "TSVET", "f_size" => "RAZMERY");
                                foreach ($iterator as $class => $propName) {
                                    $res = CIBlockElement::GetProperty($id, $v["PRODUCT_ID"], "sort", "asc", array("CODE" => $propName));
                                    if ($ob = $res->GetNext()) {
                                        $displayValue = CIBlockFormatProperties::GetDisplayValue($v["PRODUCT_ID"], $ob, false);
                                        if (!empty($displayValue["DISPLAY_VALUE"]))
                                            if($class == "f_color"){
                                                echo "<span>" .$displayValue["DISPLAY_VALUE"]."</span>";
                                            }else{
                                                echo "<span>" .$displayValue["DISPLAY_VALUE"].Loc::getMessage("f_size"). "</span>";
                                            }
                                    }
                                }
                                ?>
                            </p>
                        </div>
                    </div>
                    <?
                    unset($elid);
                }
                ?>
            </div>

            <?
        }
    }

    if ($arParams["SHOW_DELAY"] == 'Y' && empty($arResult['CATEGORIES']['DELAY'])) {
        ?>
        <ul id="<?= $cartId ?>products"></ul>
        <div class="favorite_container emptyCart">
            <div class="f_title"><?= Loc::getMessage("TSB1_EMPTY_FAV") ?></div>
        </div>
        <div class="order_link">
            <a href="/catalog/" class="button btn-orange"><?= Loc::getMessage("TSB1_CATALOG") ?></a>
        </div>
        <?
    }else{
        ?>
        <p>
            <a href="/personal/favorites/" class="button btn-orange"><?= Loc::getMessage("MORE_FAVORITE") ?></a>
        </p>
        <?
    }
    ?>
</span>
