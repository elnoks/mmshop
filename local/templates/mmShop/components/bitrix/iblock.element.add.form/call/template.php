<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(false);

if (strlen($arResult["MESSAGE"]) > 0){
?>
	<?=$arResult["SUCCESS_MESSAGE"]?>
<?
}
else
{
?>
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close oln" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true"></span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$arResult["FORM_NAME"]?></h4>
    </div>
    <div class="modal-body">
        <form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
                <?if (is_array($arResult["PROPERTY_LIST"]) && !empty($arResult["PROPERTY_LIST"])):?>
                    <?foreach ($arResult["PROPERTY_LIST"] as $propertyID):
                                foreach ($arResult["ERRORS"] as $erItem)
                                {
                                    $pos = strripos($erItem, $arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]);
                                    if ($pos === false) {
                                        $erClass = "";
                                    } else {
                                        $erClass = "empty_field"; break;
                                    }
                                }

                                if (intval($propertyID) > 0)
                                {
                                    if (
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "T"
                                        &&
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] == "1"
                                    )
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "S";
                                    elseif (
                                        (
                                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "S"
                                            ||
                                            $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] == "N"
                                        )
                                        &&
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"] > "1"
                                    )
                                        $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "T";
                                }
                                elseif (($propertyID == "TAGS") && CModule::IncludeModule('search'))
                                    $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"] = "TAGS";

                                if ($arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE"] == "Y")
                                {
                                    $inputNum = ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0) ? count($arResult["ELEMENT_PROPERTIES"][$propertyID]) : 0;
                                    $inputNum += $arResult["PROPERTY_LIST_FULL"][$propertyID]["MULTIPLE_CNT"];
                                }
                                else
                                {
                                    $inputNum = 1;
                                }

                                if($arResult["PROPERTY_LIST_FULL"][$propertyID]["GetPublicEditHTML"])
                                    $INPUT_TYPE = "USER_TYPE";
                                else
                                    $INPUT_TYPE = $arResult["PROPERTY_LIST_FULL"][$propertyID]["PROPERTY_TYPE"];

                                switch ($INPUT_TYPE):
                                    case "S":
                                    case "N":
                                    for ($i = 0; $i<$inputNum; $i++)
                                    {
                                        if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                        {
                                            $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                        }
                                        elseif ($i == 0)
                                        {
                                            $value = intval($propertyID) <= 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                        }
                                        else
                                        {
                                            $value = "";
                                        }
                                        if ($propertyID == "NAME"){
                                            ?>
                                            <input type="hidden" name="PROPERTY[<?=$propertyID?>][<?=$i?>]" size="25" value="<?=$value.$this->randString(25);?>" />
                                            <?
                                        }else{
                                            ?>
                                                <label for="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["NAME"]?></label>
                                                <input type="text"
                                                       class="<?=$erClass?> <?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]?>"
                                                       name="PROPERTY[<?=$propertyID?>][<?=$i?>]"
                                                       size="25"
                                                       value="<?=$value?>"
                                                       placeholder="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["HINT"]?>"
                                                />
                                            <?
                                        }
                                    }
                                    break;
                                    case "T":
                                        for ($i = 0; $i<$inputNum; $i++)
                                        {

                                            if ($arParams["ID"] > 0 || count($arResult["ERRORS"]) > 0)
                                            {
                                                $value = intval($propertyID) > 0 ? $arResult["ELEMENT_PROPERTIES"][$propertyID][$i]["VALUE"] : $arResult["ELEMENT"][$propertyID];
                                            }
                                            elseif ($i == 0)
                                            {
                                                $value = intval($propertyID) > 0 ? "" : $arResult["PROPERTY_LIST_FULL"][$propertyID]["DEFAULT_VALUE"];
                                            }
                                            else
                                            {
                                                $value = "";
                                            }
                                            ?>
                                            <textarea class="<?=$erClass?> <?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["CODE"]?>" cols="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["COL_COUNT"]?>" rows="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["ROW_COUNT"]?>" placeholder="<?=$arResult["PROPERTY_LIST_FULL"][$propertyID]["HINT"]?>" name="PROPERTY[<?=$propertyID?>][<?=$i?>]"><?=$value?></textarea>
                                            <?
                                        }
                                    break;
                                endswitch;
                                ?>
                    <?endforeach;?>
                    <?if($arParams["USE_CAPTCHA"] == "Y" && $arParams["ID"] <= 0):?>
                            <?=GetMessage("IBLOCK_FORM_CAPTCHA_TITLE")?>

                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />

                            <?=GetMessage("IBLOCK_FORM_CAPTCHA_PROMPT")?><span class="starrequired">*</span>:
                            <input type="text" name="captcha_word" maxlength="50" value="">
                    <?endif?>
                <?endif?>
            <button type="submit" class="btn_orange"><?=$arParams["BUTTON_TEXT"]?></button>
            <input type="hidden" name="iblock_submit" value="call-me" />
        </form>
    </div>
</div>
<script>
    BX.ready(function(){
        $('input, textarea').focus(function(){
            $(this).data('placeholder',$(this).attr('placeholder'))
            $(this).attr('placeholder','');
        });
            $('input,textarea').blur(function(){
            $(this).attr('placeholder',$(this).data('placeholder'));
        });

        $(".phone_input").mask("+7(999) 999-99-99");
        $(".time_input").mask("99:99");
        BX.addCustomEvent('onAjaxSuccess', function () {
            $(".phone_input").mask("+7(999) 999-99-99");
            $(".time_input").mask("99:99");
        });

    });
</script>
<?
}
?>