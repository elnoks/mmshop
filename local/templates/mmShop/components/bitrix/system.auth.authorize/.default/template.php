<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
ShowMessage($arParams["~AUTH_RESULT"]);
ShowMessage($arResult['ERROR_MESSAGE']);
?>
<div class="head_control_item profile_key">
    <?if ($USER->IsAuthorized()){
        $name = trim($USER->GetFullName());
        if (! $name)
            $name = trim($USER->GetLogin());
        if (strlen($name) > 15)
            $name = substr($name, 0, 8).'...';
        ?>
        <a href="javascript:void(0);" id="profile_link">
            <div class="head_control_item_ico head_control_item_ico_1 logged_in" id="profile_link_click"></div>
            <div class="head_control_item_label">
                <?=$name?>
            </div>
        </a>
        <div class="access_menu login_form_container">
            <ul>
                <li>
                    <a href="/personal/"><?= GetMessage('AUTH_PROFILE') ?></a>
                </li>
                <li>
                    <a href="/personal/favorites/"><?= GetMessage('AUTH_FAV') ?></a>
                </li>
                <li>
                    <a href="/personal/order/"><?= GetMessage('AUTH_LAST_ORDERS') ?></a>
                </li>
                <li>
                    <a href="?logout=yes"><?= GetMessage('AUTH_OUT') ?></a>
                </li>
            </ul>
        </div>

    <?php
    }else{
    ?>
        <a href="javascript:void(0);" id="profile_link">
            <div class="head_control_item_ico head_control_item_ico_1" id="profile_link_click"></div>
            <div class="head_control_item_label">
                <?=GetMessage("AUTH_AUTHORIZE");?>
            </div>
        </a>
        <div class="access_menu login_form_container">
            <h4><?=GetMessage("AUTH_AUTHORIZE");?></h4>
            <form class='login_form' name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
                <input type="hidden" name="AUTH_FORM" value="Y" />
                <input type="hidden" name="TYPE" value="AUTH" />
                <?if (strlen($arResult["BACKURL"]) > 0){?>
                    <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                <?}?>
                <?foreach ($arResult["POST"] as $key => $value){?>
                    <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                <?}?>
                <div class="inputForm">
                    <label for='USER_LOGIN'><?=GetMessage("AUTH_LOGIN")?></label>
                    <i class="ico_man"></i>
                    <input type='text' name="USER_LOGIN" placeholder='<?=GetMessage("AUTH_LOGIN_MESS")?>' id='USER_LOGIN' value="<?=$arResult["LAST_LOGIN"]?>"/>
                </div>
                <div class="inputForm">
                    <label for='USER_PASSWORD'>Пароль</label>
                    <i class="ico_pw"></i>
                    <input type="password" name="USER_PASSWORD" id='USER_PASSWORD' placeholder='<?=GetMessage("AUTH_PASSWORD")?>' maxlength="255" autocomplete="off" />
                </div>
                <?if ($arParams["NOT_SHOW_LINKS"] != "Y"):?>
                    <noindex>
                        <div class='forgot_p'>
                            <a href="<?=$arParams["FORGOT_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                        </div>
                    </noindex>
                <?endif?>

                <input type="submit" name="Login" value="<?=GetMessage("AUTH_AUTHORIZE")?>" />

                <?if($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"):?>
                    <noindex>
                        <div class='registration_p'>
                            <a href="<?=$arParams["REGISTER_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_REGISTER")?></a>
                        </div>
                    </noindex>
                <?endif?>

                <?if($arResult["CAPTCHA_CODE"]){?>
                    <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>

                    <div class="bx-authform-formgroup-container dbg_captha">
                        <div class="bx-authform-label-container">
                            <? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>
                        </div>
                        <div class="bx-captcha"><img
                                    src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180"
                                    height="40" alt="CAPTCHA"/></div>
                        <div class="bx-authform-input-container">
                            <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                        </div>
                    </div>
                <?}?>
            </form>
        </div>
        <script type="text/javascript">
        <?if (strlen($arResult["LAST_LOGIN"])>0):?>
        try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
        <?else:?>
        try{document.form_auth.USER_LOGIN.focus();}catch(e){}
        <?endif?>
        </script>
<?
    }
?>
</div>
<script>
    BX.bind(BX('profile_link_click'), 'click', function() {
        if (BX.hasClass("profile_link", 'bx-opener'))
            BX.removeClass(BX('profile_link'), 'bx-opener');
        else
            BX.addClass(BX('profile_link'), 'bx-opener');
    })
    BX.bind(BX('overlay'), 'click', function() {
        BX.removeClass(BX('profile_link'), 'bx-opener');
    })
</script>