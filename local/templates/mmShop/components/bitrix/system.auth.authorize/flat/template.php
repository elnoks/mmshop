<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="auth-dialog head_control_item">
    <h4><?= GetMessage("AUTH_PLEASE_AUTH") ?></h4>
    <?
    if ($arResult['ERROR_MESSAGE'] <> ''):
        $text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
        ?>
        <div class="alert alert-danger"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
    <? endif ?>
    <form class="login_form" name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>"
          class='login_form'>
        <?
        if (!empty($arParams["~AUTH_RESULT"])):
            $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
            ?>
            <div class="alert alert-danger"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
        <? endif ?>
        <input type="hidden" name="AUTH_FORM" value="Y"/>
        <input type="hidden" name="TYPE" value="AUTH"/>
        <? if (strlen($arResult["BACKURL"]) > 0) { ?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
        <? } ?>
        <? foreach ($arResult["POST"] as $key => $value) { ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
        <? } ?>
        <div class="inputForm">
            <label for='USER_LOGIN'><?= GetMessage("AUTH_LOGIN") ?></label>
            <i class="ico_man"></i>
            <input type="text" name="USER_LOGIN" maxlength="255"
                   placeholder='<?= GetMessage("AUTH_LOGIN_PLACEHOLDER") ?>' value="<?= $arResult["LAST_LOGIN"] ?>"/>
        </div>
        <div class="inputForm">
            <label for='USER_PASSWORD'>Пароль</label>
            <i class="ico_pw"></i>
            <input type="password" name="USER_PASSWORD" id='USER_PASSWORD'
                   placeholder='<?= GetMessage("AUTH_PASSWORD") ?>' maxlength="255" autocomplete="off"/>
        </div>
        <? if ($arResult["CAPTCHA_CODE"]): ?>
            <input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>

            <div class="bx-authform-formgroup-container dbg_captha">
                <div class="bx-authform-label-container">
                    <? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>
                </div>
                <div class="bx-captcha"><img
                            src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180"
                            height="40" alt="CAPTCHA"/></div>
                <div class="bx-authform-input-container">
                    <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                </div>
            </div>
        <? endif; ?>

        <? if ($arParams["NOT_SHOW_LINKS"] != "Y"): ?>
            <noindex>
                <div class='forgot_p'>
                    <a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>"
                       rel="nofollow"><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>
                </div>
            </noindex>
        <? endif ?>
        <input type="submit" name="Login" value="<?= GetMessage("AUTH_AUTHORIZE") ?>"/>
        <? if ($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"): ?>
            <noindex>
                <div class='registration_p'>
                    <a href="<?= $arResult["AUTH_REGISTER_URL"] ?>"
                       rel="nofollow"><?= GetMessage("AUTH_REGISTER") ?></a>
                </div>
            </noindex>
        <? endif ?>
    </form>
</div>

<script type="text/javascript">
    <?if (strlen($arResult["LAST_LOGIN"]) > 0):?>
    try {
        document.form_auth.USER_PASSWORD.focus();
    } catch (e) {
    }
    <?else:?>
    try {
        document.form_auth.USER_LOGIN.focus();
    } catch (e) {
    }
    <?endif?>
</script>

