<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;

$fields = array(
    "USER_NAME" => array(
        "TYPE" => "text",
    ),
    "USER_LAST_NAME" => array(
        "TYPE" => "text",
    ),
    "PERSONAL_PHONE" => array(
        "TYPE" => "text",
        "CLASS" => "input-bg-tel",
    ),
    "USER_EMAIL" => array(
        "TYPE" => "text",
        "CLASS" => "input-bg-email",
        "FUNCTION" => "onkeyup=\"BX('userLogin').setAttribute('value', this.value);\"",
    ),
    "USER_PASSWORD" => array(
        "TYPE" => "password",
        "CLASS" => "input-bg-lock"
    ),
    "USER_CONFIRM_PASSWORD" => array(
        "TYPE" => "password",
        "CLASS" => "input-bg-lock"
    ),
    "USER_EXTENSION" => array(
        "LINK" => "USER_EMAIL"
    )
);

if(!empty($arParams["~AUTH_RESULT"])) {
    $text = str_replace(array("<br>", "<br />"), "<>", $arParams["~AUTH_RESULT"]["MESSAGE"]);
    $arErrors = explode("<>", $text);
    TrimArr($arErrors);

    foreach($fields as $key => $item){
        foreach($arErrors as $v) {
            $pos = strripos($v, Loc::getMessage($key));
            if ($pos !== false) {
                if(isset($fields[$key]["LINK"])){
                    $fields[$fields[$key]["LINK"]]["ERROR"]["VALUE"] = "Y";
                    $fields[$fields[$key]["LINK"]]["ERROR"]["TEXT"] = $v;
                }else {
                    $fields[$key]["ERROR"]["VALUE"] = "Y";
                    $fields[$key]["ERROR"]["TEXT"] = $v;
                }
            }
        }
    }
}
$arResult["BACKURL"] = $arResult["BACKURL"]."?registered=yes";
$arResult["REQUEST"] = $fields;