<?
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["USER_NAME"] = "Имя";
$MESS["USER_LAST_NAME"] = "Фамилия";
$MESS["PERSONAL_PHONE"] = "Телефон";
$MESS["USER_EMAIL"] = "E-Mail";
$MESS["USER_LOGIN"] = "Логин";
$MESS["USER_EMAIL_PLACEHOLDER"] = "Ваш E-Mail";
$MESS["PERSONAL_PHONE_PLACEHOLDER"] = "Ваш телефон";
$MESS["USER_NAME_PLACEHOLDER"] = "Ваше имя";
$MESS["USER_LOGIN_PLACEHOLDER"] = "Ваш логин";
$MESS["USER_PASSWORD"] = "Пароль";
$MESS["USER_LAST_NAME_PLACEHOLDER"] = "Ваша фамилия";
$MESS["CAPTCHA_REGF_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REQ"] = "Обязательные поля.";
$MESS["AUTH_AUTH"] = "Авторизация";

$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "На указанный в форме e-mail придет запрос на подтверждение регистрации.";
$MESS["AUTH_EMAIL_SENT"] = "На указанный в форме e-mail было выслано письмо с информацией о подтверждении регистрации.";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_SECURE_PRIVACY"] = 'Согласен(а) на <a class="grey-link" target="_blank" href="/privacy/">обработку персональных данных</a>';
$MESS["USER_CONFIRM_PASSWORD"] = 'Подтверждение пароля';
$MESS["USER_EXTENSION"] = 'уже существует';
?>