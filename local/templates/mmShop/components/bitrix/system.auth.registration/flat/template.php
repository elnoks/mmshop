<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

$APPLICATION->SetTitle(Loc::getMessage("AUTH_REGISTER"));
?>
<div class="content_front">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1><?=Loc::getMessage("AUTH_REGISTER")?></h1>
            </div>
        </div>
        <noindex>
            <div class="row login-wrap reg-cover">
                <form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform" enctype="multipart/form-data">
                    <div class="row">
                        <?if($arResult["BACKURL"] <> ''):?>
                            <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                        <?endif?>
                        <input type="hidden" name="AUTH_FORM" value="Y" />
                        <input type="hidden" name="TYPE" value="REGISTRATION" />
                        <?php
                        foreach ($arResult["REQUEST"] as $key => $item){
                            if(isset($item["LINK"])) continue;
                            ?>
                            <div class="col-sm-6" id="<?=$key?>_reg">
                                <label class="<?=$item["CLASS"]?>" for="<?=$key?>"><?=Loc::getMessage($key)?></label>
                                    <input <?=$item['FUNCTION']?>
                                             class="input-bg <?=$item["ERROR"]["VALUE"] == "Y" ? "alert-input":"";?>"
                                             name="<?=$key?>"
                                             placeholder='<?=Loc::getMessage($key."_PLACEHOLDER")?>'
                                             type="<?=$item["TYPE"]?>"
                                             value="<?=$arResult[$key]?>"
                                    />
                                </label>
                                <p class="btn-alert"><?=$item["ERROR"]["TEXT"]?></p>
                            </div>
                            <?
                        }
                        ?>
                        <input type="hidden" name="USER_LOGIN" id="userLogin" maxlength="255" value="<?=$arResult["USER_EMAIL"]?>" />

                        <?
                        if ($arResult["USE_CAPTCHA"] == "Y"){?>

                            <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="100%" height="40" alt="CAPTCHA" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
                                </div>
                            </div>
                        <?
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <input type="checkbox" checked class="checkbox" id="checkbox" />
                            <label class="personal-agree" for="checkbox"><?=Loc::getMessage("AUTH_SECURE_PRIVACY")?></label>
                        </div>
                        <div class="col-sm-4 cab-buttons-wrap">
                            <button type="submit" id="submitForm" class="button btn-orange"><?=Loc::getMessage("AUTH_REGISTER")?></button>
                        </div>
                    </div>
                    <input type="hidden" name="Register" value="<?=Loc::getMessage("AUTH_REGISTER")?>" />
                </form>
            </div>
        </noindex>
        <script type="text/javascript">
            document.bform.USER_NAME.focus();
            $('input').keyup(function(){
                console.log(this);
                $(this).removeClass("alert-input");
                $(this).next().html('');

            });
            $('#checkbox').change(function(){
                if(this.checked != true)
                {
                    BX.addClass('submitForm', 'unclick');
                    return false;
                }else{
                    BX.removeClass('submitForm', 'unclick');
                    return true;
                }
            });
        </script>
    </div>
</div>
