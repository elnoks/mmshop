<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<div class="hidden-sm hidden-md hidden-lg mobile_bc">';

$itemSize = count($arResult);

$mobilebr = $arResult[count($arResult) - 2];

$title = htmlspecialcharsex($mobilebr["TITLE"]);

$strReturn .= "<a href=".$mobilebr["LINK"]." title=".$title."><i class='fa fa-chevron-left' aria-hidden='true'></i>&nbsp&nbsp".$title."</a>";


$strReturn .= '</div>';

$strReturn .= '<div class="breadcrumbs"><div class="col-lg-12"><ul class="hidden-xs">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<li><a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a></li>';
	}
	else
	{
		$strReturn .= '<li>'.$title.'</li>';
	}
}

$strReturn .= '</ul></div></div>';

return $strReturn;